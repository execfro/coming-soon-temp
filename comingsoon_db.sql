-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 18, 2019 at 12:46 AM
-- Server version: 5.6.38
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `comingsoon_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `cs_commentmeta`
--

CREATE TABLE `cs_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cs_comments`
--

CREATE TABLE `cs_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cs_comments`
--

INSERT INTO `cs_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2019-04-03 21:24:50', '2019-04-03 21:24:50', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cs_gf_draft_submissions`
--

CREATE TABLE `cs_gf_draft_submissions` (
  `uuid` char(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `ip` varchar(39) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source_url` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `submission` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cs_gf_entry`
--

CREATE TABLE `cs_gf_entry` (
  `id` int(10) UNSIGNED NOT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime DEFAULT NULL,
  `is_starred` tinyint(1) NOT NULL DEFAULT '0',
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `ip` varchar(39) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_agent` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `currency` varchar(5) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `payment_status` varchar(15) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `payment_amount` decimal(19,2) DEFAULT NULL,
  `payment_method` varchar(30) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `transaction_id` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `is_fulfilled` tinyint(1) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `transaction_type` tinyint(1) DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cs_gf_entry_meta`
--

CREATE TABLE `cs_gf_entry_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `entry_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  `item_index` varchar(60) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cs_gf_entry_notes`
--

CREATE TABLE `cs_gf_entry_notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `entry_id` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(250) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_520_ci,
  `note_type` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `sub_type` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cs_gf_form`
--

CREATE TABLE `cs_gf_form` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_trash` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cs_gf_form`
--

INSERT INTO `cs_gf_form` (`id`, `title`, `date_created`, `date_updated`, `is_active`, `is_trash`) VALUES
(1, 'Contact Form', '2019-04-03 22:10:15', NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cs_gf_form_meta`
--

CREATE TABLE `cs_gf_form_meta` (
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `display_meta` longtext COLLATE utf8mb4_unicode_520_ci,
  `entries_grid_meta` longtext COLLATE utf8mb4_unicode_520_ci,
  `confirmations` longtext COLLATE utf8mb4_unicode_520_ci,
  `notifications` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cs_gf_form_meta`
--

INSERT INTO `cs_gf_form_meta` (`form_id`, `display_meta`, `entries_grid_meta`, `confirmations`, `notifications`) VALUES
(1, '{\"title\":\"Contact Form\",\"description\":\"\",\"labelPlacement\":\"top_label\",\"descriptionPlacement\":\"below\",\"button\":{\"type\":\"text\",\"text\":\"Submit\",\"imageUrl\":\"\"},\"fields\":[{\"type\":\"name\",\"id\":1,\"label\":\"Name\",\"adminLabel\":\"\",\"isRequired\":false,\"size\":\"medium\",\"errorMessage\":\"\",\"visibility\":\"visible\",\"nameFormat\":\"advanced\",\"inputs\":[{\"id\":\"1.2\",\"label\":\"Prefix\",\"name\":\"\",\"choices\":[{\"text\":\"Mr.\",\"value\":\"Mr.\",\"isSelected\":false,\"price\":\"\"},{\"text\":\"Mrs.\",\"value\":\"Mrs.\",\"isSelected\":false,\"price\":\"\"},{\"text\":\"Miss\",\"value\":\"Miss\",\"isSelected\":false,\"price\":\"\"},{\"text\":\"Ms.\",\"value\":\"Ms.\",\"isSelected\":false,\"price\":\"\"},{\"text\":\"Dr.\",\"value\":\"Dr.\",\"isSelected\":false,\"price\":\"\"},{\"text\":\"Prof.\",\"value\":\"Prof.\",\"isSelected\":false,\"price\":\"\"},{\"text\":\"Rev.\",\"value\":\"Rev.\",\"isSelected\":false,\"price\":\"\"}],\"isHidden\":true,\"inputType\":\"radio\"},{\"id\":\"1.3\",\"label\":\"First\",\"name\":\"\",\"placeholder\":\"First Name\"},{\"id\":\"1.4\",\"label\":\"Middle\",\"name\":\"\",\"isHidden\":true},{\"id\":\"1.6\",\"label\":\"Last\",\"name\":\"\",\"placeholder\":\"Last Name\"},{\"id\":\"1.8\",\"label\":\"Suffix\",\"name\":\"\",\"isHidden\":true}],\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputMaskIsCustom\":false,\"maxLength\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"\",\"cssClass\":\"\",\"inputName\":\"\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"pageNumber\":1,\"fields\":\"\",\"displayOnly\":\"\",\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"useRichTextEditor\":false},{\"type\":\"phone\",\"id\":3,\"label\":\"Phone\",\"adminLabel\":\"\",\"isRequired\":false,\"size\":\"medium\",\"errorMessage\":\"\",\"visibility\":\"visible\",\"inputs\":null,\"phoneFormat\":\"standard\",\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputMaskIsCustom\":false,\"maxLength\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"Phone Number\",\"cssClass\":\"gf_left_half\",\"inputName\":\"\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"form_id\":\"\",\"productField\":\"\",\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"useRichTextEditor\":false,\"pageNumber\":1,\"fields\":\"\",\"displayOnly\":\"\"},{\"type\":\"email\",\"id\":2,\"label\":\"Email\",\"adminLabel\":\"\",\"isRequired\":false,\"size\":\"medium\",\"errorMessage\":\"\",\"visibility\":\"visible\",\"inputs\":null,\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputMaskIsCustom\":false,\"maxLength\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"Email Address\",\"cssClass\":\"gf_right_half\",\"inputName\":\"\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"emailConfirmEnabled\":\"\",\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"useRichTextEditor\":false,\"pageNumber\":1,\"fields\":\"\",\"displayOnly\":\"\"},{\"type\":\"textarea\",\"id\":4,\"label\":\"Message\",\"adminLabel\":\"\",\"isRequired\":false,\"size\":\"medium\",\"errorMessage\":\"\",\"visibility\":\"visible\",\"inputs\":null,\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputMaskIsCustom\":false,\"maxLength\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"Please enter your message here:\",\"cssClass\":\"\",\"inputName\":\"\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"form_id\":\"\",\"useRichTextEditor\":false,\"pageNumber\":1,\"fields\":\"\",\"displayOnly\":\"\",\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false}],\"version\":\"2.4.6\",\"id\":1,\"useCurrentUserAsAuthor\":true,\"postContentTemplateEnabled\":false,\"postTitleTemplateEnabled\":false,\"postTitleTemplate\":\"\",\"postContentTemplate\":\"\",\"lastPageButton\":null,\"pagination\":null,\"firstPageCssClass\":null,\"nextFieldId\":6,\"notifications\":{\"5ca52f472b2ba\":{\"isActive\":true,\"id\":\"5ca52f472b2ba\",\"name\":\"Admin Notification\",\"service\":\"wordpress\",\"event\":\"form_submission\",\"to\":\"developer@bigrigmedia.com\",\"toType\":\"email\",\"cc\":\"\",\"bcc\":\"\",\"subject\":\"New submission from **Website Title** | Contact Form\",\"message\":\"{all_fields}\",\"from\":\"{Email:2}\",\"fromName\":\"{Name (Prefix):1.2}\",\"replyTo\":\"{Email:2}\",\"routing\":null,\"conditionalLogic\":null,\"disableAutoformat\":false,\"enableAttachments\":false}},\"confirmations\":{\"5ca52f472b737\":{\"id\":\"5ca52f472b737\",\"name\":\"Default Confirmation\",\"isDefault\":true,\"type\":\"message\",\"message\":\"Thanks for contacting us! We will get in touch with you shortly.\",\"url\":\"\",\"pageId\":\"\",\"queryString\":\"\"}}}', NULL, '{\"5ca52f472b737\":{\"id\":\"5ca52f472b737\",\"name\":\"Default Confirmation\",\"isDefault\":true,\"type\":\"message\",\"message\":\"Thanks for contacting us! We will get in touch with you shortly.\",\"url\":\"\",\"pageId\":\"\",\"queryString\":\"\"}}', '{\"5ca52f472b2ba\":{\"isActive\":true,\"id\":\"5ca52f472b2ba\",\"name\":\"Admin Notification\",\"service\":\"wordpress\",\"event\":\"form_submission\",\"to\":\"developer@bigrigmedia.com\",\"toType\":\"email\",\"cc\":\"\",\"bcc\":\"\",\"subject\":\"New submission from **Website Title** | Contact Form\",\"message\":\"{all_fields}\",\"from\":\"{Email:2}\",\"fromName\":\"{Name (Prefix):1.2}\",\"replyTo\":\"{Email:2}\",\"routing\":null,\"conditionalLogic\":null,\"disableAutoformat\":false,\"enableAttachments\":false}}');

-- --------------------------------------------------------

--
-- Table structure for table `cs_gf_form_revisions`
--

CREATE TABLE `cs_gf_form_revisions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `display_meta` longtext COLLATE utf8mb4_unicode_520_ci,
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cs_gf_form_view`
--

CREATE TABLE `cs_gf_form_view` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `ip` char(15) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `count` mediumint(8) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cs_gf_form_view`
--

INSERT INTO `cs_gf_form_view` (`id`, `form_id`, `date_created`, `ip`, `count`) VALUES
(1, 1, '2019-04-03 22:11:10', '', 23),
(2, 1, '2019-04-05 17:14:10', '', 2),
(3, 1, '2019-04-08 18:02:44', '', 1),
(4, 1, '2019-04-19 18:04:53', '', 5),
(5, 1, '2019-05-03 18:42:54', '', 1),
(6, 1, '2019-05-06 22:45:58', '', 59),
(7, 1, '2019-05-07 22:53:19', '', 28);

-- --------------------------------------------------------

--
-- Table structure for table `cs_links`
--

CREATE TABLE `cs_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cs_options`
--

CREATE TABLE `cs_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cs_options`
--

INSERT INTO `cs_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://coming-soon.test/wp', 'yes'),
(2, 'home', 'http://coming-soon.test/', 'yes'),
(3, 'blogname', 'Coming Soon', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'developer@bigrigmedia.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:88:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=2&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:6:{i:0;s:29:\"gravityforms/gravityforms.php\";i:1;s:34:\"advanced-custom-fields-pro/acf.php\";i:2;s:35:\"all-slider-control/basic-slider.php\";i:3;s:33:\"classic-editor/classic-editor.php\";i:5;s:23:\"soliloquy/soliloquy.php\";i:6;s:29:\"wp-mail-smtp/wp_mail_smtp.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'sage/resources', 'yes'),
(41, 'stylesheet', 'sage/resources', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '44719', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:2:{s:33:\"classic-editor/classic-editor.php\";a:2:{i:0;s:14:\"Classic_Editor\";i:1;s:9:\"uninstall\";}s:23:\"soliloquy/soliloquy.php\";s:24:\"soliloquy_uninstall_hook\";}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '2', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'initial_db_version', '44719', 'yes'),
(94, 'cs_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:15:\"sidebar-primary\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:14:\"sidebar-footer\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(102, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'cron', 'a:7:{i:1558135490;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1558171490;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1558214698;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1558214699;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1558214734;a:1:{s:17:\"gravityforms_cron\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1558219128;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(117, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1558132727;s:7:\"checked\";a:1:{s:14:\"sage/resources\";s:5:\"9.0.9\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(129, 'can_compress_scripts', '1', 'no'),
(136, 'theme_mods_twentynineteen', 'a:1:{s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1554326703;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(137, 'current_theme', 'Sage Starter Theme', 'yes'),
(138, 'theme_mods_sage/resources', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(139, 'theme_switched', '', 'yes'),
(142, 'recently_activated', 'a:1:{s:27:\"coming-soon/coming-soon.php\";i:1558132676;}', 'yes'),
(143, 'gf_db_version', '2.4.6', 'yes'),
(144, 'rg_form_version', '2.4.6', 'yes'),
(145, 'gform_enable_background_updates', '1', 'yes'),
(146, 'gform_pending_installation', '', 'yes'),
(147, 'widget_gform_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(148, 'gravityformsaddon_gravityformswebapi_version', '1.0', 'yes'),
(149, 'gform_version_info', 'a:6:{s:12:\"is_valid_key\";s:1:\"0\";s:15:\"expiration_time\";i:0;s:7:\"version\";s:5:\"1.7.9\";s:3:\"url\";s:0:\"\";s:9:\"offerings\";a:0:{}s:9:\"timestamp\";i:1558132611;}', 'yes'),
(150, 'acf_version', '5.7.1', 'yes'),
(169, 'WPLANG', '', 'yes'),
(170, 'new_admin_email', 'developer@bigrigmedia.com', 'yes'),
(187, 'options_logo_field', '', 'no'),
(188, '_options_logo_field', 'field_5ca5278b43767', 'no'),
(189, 'options_phone_number', '', 'no'),
(190, '_options_phone_number', 'field_5c8be52ffb2d6', 'no'),
(191, 'options_address_street_name', '', 'no'),
(192, '_options_address_street_name', 'field_5c8bf58f1a56f', 'no'),
(193, 'options_address_city_name', '', 'no'),
(194, '_options_address_city_name', 'field_5c8bf5a51a570', 'no'),
(195, 'options_address_state', '', 'no'),
(196, '_options_address_state', 'field_5c8bf5b51a571', 'no'),
(197, 'options_address_zipcode', '', 'no'),
(198, '_options_address_zipcode', 'field_5c8bf5be1a572', 'no'),
(199, 'options_newsletter_form', '', 'no'),
(200, '_options_newsletter_form', 'field_5c8bdecf3edab', 'no'),
(201, 'options_facebook_url', '', 'no'),
(202, '_options_facebook_url', 'field_5c8bec12e62b7', 'no'),
(203, 'options_google_url', '', 'no'),
(204, '_options_google_url', 'field_5c8bece4fd248', 'no'),
(205, 'options_yelp_url', '', 'no'),
(206, '_options_yelp_url', 'field_5c8bed63780c5', 'no'),
(207, 'options_instagram_url', '', 'no'),
(208, '_options_instagram_url', 'field_5c8bed54fd249', 'no'),
(233, 'options_map_iframe', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d13282.346952090202!2d-116.3159858802246!3d33.66786845!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sus!4v1554329333089!5m2!1sen!2sus\" width=\"100%\" height=\"250\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', 'no'),
(234, '_options_map_iframe', 'field_5ca52be54aa57', 'no'),
(243, 'options_email_field', '', 'no'),
(244, '_options_email_field', 'field_5ca52ea416411', 'no'),
(252, 'rg_gforms_enable_akismet', '1', 'yes'),
(253, 'rg_gforms_currency', '', 'yes'),
(254, 'gform_enable_toolbar_menu', '1', 'yes'),
(257, 'category_children', 'a:0:{}', 'yes'),
(268, 'wp_mail_smtp_initial_version', '1.4.2', 'no'),
(269, 'wp_mail_smtp_version', '1.4.2', 'no'),
(270, 'wp_mail_smtp', 'a:5:{s:4:\"mail\";a:6:{s:10:\"from_email\";s:36:\"postmaster@mg.onepointmediagroup.com\";s:9:\"from_name\";s:11:\"Coming Soon\";s:6:\"mailer\";s:7:\"mailgun\";s:11:\"return_path\";b:0;s:16:\"from_email_force\";b:0;s:15:\"from_name_force\";b:1;}s:4:\"smtp\";a:7:{s:7:\"autotls\";s:3:\"yes\";s:4:\"host\";s:0:\"\";s:10:\"encryption\";s:4:\"none\";s:4:\"port\";s:0:\"\";s:4:\"user\";s:0:\"\";s:4:\"pass\";s:0:\"\";s:4:\"auth\";b:0;}s:5:\"gmail\";a:2:{s:9:\"client_id\";s:0:\"\";s:13:\"client_secret\";s:0:\"\";}s:7:\"mailgun\";a:3:{s:7:\"api_key\";s:36:\"key-cebbb771ca4a17103f30974f7350be09\";s:6:\"domain\";s:25:\"mg.onepointmediagroup.com\";s:6:\"region\";s:2:\"US\";}s:8:\"sendgrid\";a:1:{s:7:\"api_key\";s:0:\"\";}}', 'no'),
(271, '_amn_smtp_last_checked', '1558051200', 'yes'),
(272, 'wp_mail_smtp_debug', 'a:0:{}', 'no'),
(381, 'options_contact_image', '', 'no'),
(382, '_options_contact_image', 'field_5ca52be54aa57', 'no'),
(391, 'soliloquy_upgrade', '1', 'yes'),
(392, 'soliloquy_upgrade_cpts', '1', 'yes'),
(393, 'soliloquy_review', 'a:2:{s:4:\"time\";i:1558132620;s:9:\"dismissed\";b:1;}', 'yes'),
(408, 'soliloquy', 'a:5:{s:3:\"key\";s:0:\"\";s:4:\"type\";s:0:\"\";s:10:\"is_expired\";b:0;s:11:\"is_disabled\";b:0;s:10:\"is_invalid\";b:0;}', 'yes'),
(409, 'soliloquy-publishing-default', 'active', 'yes'),
(410, 'soliloquy_slide_view', 'grid', 'yes'),
(411, 'widget_soliloquy', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(412, '_amn_sol_last_checked', '1558051200', 'yes'),
(427, 'seed_csp4_initial_version', '5.0.25', 'no'),
(428, 'seed_csp4_version', '5.0.25', 'no'),
(429, 'seed_csp4_settings_content', 'a:9:{s:6:\"status\";s:1:\"0\";s:4:\"logo\";s:86:\"http://coming-soon.test/app/uploads/2019/05/One-Point-Media-Group-logoA-01-262x300.png\";s:8:\"headline\";s:49:\"Get Ready... Something Really Cool Is Coming Soon\";s:11:\"description\";s:0:\"\";s:13:\"footer_credit\";s:1:\"0\";s:7:\"favicon\";s:0:\"\";s:9:\"seo_title\";s:0:\"\";s:15:\"seo_description\";s:0:\"\";s:12:\"ga_analytics\";s:0:\"\";}', 'yes'),
(430, 'seed_csp4_settings_design', 'a:12:{s:8:\"bg_color\";s:7:\"#fafafa\";s:8:\"bg_image\";s:0:\"\";s:8:\"bg_cover\";a:1:{i:0;s:1:\"1\";}s:9:\"bg_repeat\";s:9:\"no-repeat\";s:11:\"bg_position\";s:8:\"left top\";s:13:\"bg_attahcment\";s:5:\"fixed\";s:9:\"max_width\";s:0:\"\";s:10:\"text_color\";s:7:\"#666666\";s:10:\"link_color\";s:7:\"#27AE60\";s:14:\"headline_color\";s:7:\"#444444\";s:9:\"text_font\";s:6:\"_arial\";s:10:\"custom_css\";s:0:\"\";}', 'yes'),
(431, 'seed_csp4_settings_advanced', 'a:2:{s:14:\"header_scripts\";s:0:\"\";s:14:\"footer_scripts\";s:0:\"\";}', 'yes'),
(570, 'options_$contact_iframe', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d92993.17926365437!2d-95.2751511948348!3d29.02232813059581!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x864073c919157f55%3A0x3020949b529783f1!2sBlue+Water+Bar+%26+Grill!5e0!3m2!1sen!2sus!4v1557268257043!5m2!1sen!2sus\" width=\"100%\" height=\"100%\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', 'no'),
(571, '_options_$contact_iframe', 'field_5cd206dbdbba3', 'no'),
(576, 'options_contact_iframe', '', 'no'),
(577, '_options_contact_iframe', 'field_5cd206dbdbba3', 'no'),
(625, '_transient_timeout_acf_plugin_updates', '1558219012', 'no'),
(626, '_transient_acf_plugin_updates', 'a:3:{s:7:\"plugins\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";a:8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:5:\"5.8.0\";s:3:\"url\";s:37:\"https://www.advancedcustomfields.com/\";s:6:\"tested\";s:3:\"5.2\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:1:{s:7:\"default\";s:66:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg\";}}}s:10:\"expiration\";i:86400;s:6:\"status\";i:1;}', 'no'),
(627, '_site_transient_timeout_theme_roots', '1558134413', 'no'),
(628, '_site_transient_theme_roots', 'a:2:{s:6:\"backup\";s:7:\"/themes\";s:14:\"sage/resources\";s:7:\"/themes\";}', 'no'),
(629, '_site_transient_timeout_browser_5f32e89c1d16ee823264142a04b68f30', '1558737416', 'no'),
(630, '_site_transient_browser_5f32e89c1d16ee823264142a04b68f30', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"74.0.3729.157\";s:8:\"platform\";s:9:\"Macintosh\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(631, '_site_transient_timeout_php_check_c10b24bcda05543594ded94839f19c88', '1558737417', 'no'),
(632, '_site_transient_php_check_c10b24bcda05543594ded94839f19c88', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(635, '_site_transient_timeout_community-events-1aecf33ab8525ff212ebdffbb438372e', '1558175818', 'no'),
(636, '_site_transient_community-events-1aecf33ab8525ff212ebdffbb438372e', 'a:2:{s:8:\"location\";a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}s:6:\"events\";a:5:{i:0;a:7:{s:4:\"type\";s:6:\"meetup\";s:5:\"title\";s:37:\"#IEWP Monthly Meetup (General Meetup)\";s:3:\"url\";s:55:\"https://www.meetup.com/inlandempirewp/events/261242770/\";s:6:\"meetup\";s:36:\"Inland Empire WordPress Meetup Group\";s:10:\"meetup_url\";s:38:\"https://www.meetup.com/inlandempirewp/\";s:4:\"date\";s:19:\"2019-06-04 19:00:00\";s:8:\"location\";a:4:{s:8:\"location\";s:18:\"Riverside, CA, USA\";s:7:\"country\";s:2:\"us\";s:8:\"latitude\";d:33.97884400000000226782503887079656124114990234375;s:9:\"longitude\";d:-117.3730800000000016325429896824061870574951171875;}}i:1;a:7:{s:4:\"type\";s:6:\"meetup\";s:5:\"title\";s:86:\"Get Your Friends to say ‘Wow!’ – Using Images in WordPress with Christina  Hills\";s:3:\"url\";s:66:\"https://www.meetup.com/Carlsbad-WordPress-Meetup/events/255468707/\";s:6:\"meetup\";s:25:\"Carlsbad WordPress Meetup\";s:10:\"meetup_url\";s:49:\"https://www.meetup.com/Carlsbad-WordPress-Meetup/\";s:4:\"date\";s:19:\"2019-06-04 19:30:00\";s:8:\"location\";a:4:{s:8:\"location\";s:17:\"Carlsbad, CA, USA\";s:7:\"country\";s:2:\"us\";s:8:\"latitude\";d:33.16197199999999867259248276241123676300048828125;s:9:\"longitude\";d:-117.34926600000000007639755494892597198486328125;}}i:2;a:7:{s:4:\"type\";s:6:\"meetup\";s:5:\"title\";s:32:\"WordPress Workshop Night - #IEWP\";s:3:\"url\";s:55:\"https://www.meetup.com/inlandempirewp/events/261433300/\";s:6:\"meetup\";s:36:\"Inland Empire WordPress Meetup Group\";s:10:\"meetup_url\";s:38:\"https://www.meetup.com/inlandempirewp/\";s:4:\"date\";s:19:\"2019-06-13 19:00:00\";s:8:\"location\";a:4:{s:8:\"location\";s:18:\"Riverside, CA, USA\";s:7:\"country\";s:2:\"us\";s:8:\"latitude\";d:33.97884400000000226782503887079656124114990234375;s:9:\"longitude\";d:-117.373076999999994995960150845348834991455078125;}}i:3;a:7:{s:4:\"type\";s:6:\"meetup\";s:5:\"title\";s:37:\"#IEWP Monthly Meetup (General Meetup)\";s:3:\"url\";s:58:\"https://www.meetup.com/inlandempirewp/events/jpmnspyzkbdb/\";s:6:\"meetup\";s:36:\"Inland Empire WordPress Meetup Group\";s:10:\"meetup_url\";s:38:\"https://www.meetup.com/inlandempirewp/\";s:4:\"date\";s:19:\"2019-07-02 19:00:00\";s:8:\"location\";a:4:{s:8:\"location\";s:18:\"Riverside, CA, USA\";s:7:\"country\";s:2:\"us\";s:8:\"latitude\";d:33.97884400000000226782503887079656124114990234375;s:9:\"longitude\";d:-117.3730800000000016325429896824061870574951171875;}}i:4;a:7:{s:4:\"type\";s:6:\"meetup\";s:5:\"title\";s:35:\"Typography Design with Amber Hewitt\";s:3:\"url\";s:66:\"https://www.meetup.com/Carlsbad-WordPress-Meetup/events/256087208/\";s:6:\"meetup\";s:25:\"Carlsbad WordPress Meetup\";s:10:\"meetup_url\";s:49:\"https://www.meetup.com/Carlsbad-WordPress-Meetup/\";s:4:\"date\";s:19:\"2019-07-09 19:30:00\";s:8:\"location\";a:4:{s:8:\"location\";s:17:\"Carlsbad, CA, USA\";s:7:\"country\";s:2:\"us\";s:8:\"latitude\";d:33.16197199999999867259248276241123676300048828125;s:9:\"longitude\";d:-117.34926600000000007639755494892597198486328125;}}}}', 'no'),
(641, '_transient_timeout_plugin_slugs', '1558219076', 'no'),
(642, '_transient_plugin_slugs', 'a:9:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";i:1;s:35:\"all-slider-control/basic-slider.php\";i:2;s:33:\"classic-editor/classic-editor.php\";i:3;s:27:\"coming-soon/coming-soon.php\";i:4;s:29:\"gravityforms/gravityforms.php\";i:5;s:21:\"imsanity/imsanity.php\";i:6;s:23:\"soliloquy/soliloquy.php\";i:7;s:17:\"stream/stream.php\";i:8;s:29:\"wp-mail-smtp/wp_mail_smtp.php\";}', 'no'),
(643, '_site_transient_update_plugins', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1558132726;s:8:\"response\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";O:8:\"stdClass\":8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:5:\"5.8.0\";s:3:\"url\";s:37:\"https://www.advancedcustomfields.com/\";s:6:\"tested\";s:3:\"5.2\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:1:{s:7:\"default\";s:66:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg\";}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:4:{s:33:\"classic-editor/classic-editor.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/classic-editor\";s:4:\"slug\";s:14:\"classic-editor\";s:6:\"plugin\";s:33:\"classic-editor/classic-editor.php\";s:11:\"new_version\";s:3:\"1.5\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/classic-editor/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/classic-editor.1.5.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-256x256.png?rev=1998671\";s:2:\"1x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-128x128.png?rev=1998671\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/classic-editor/assets/banner-1544x500.png?rev=1998671\";s:2:\"1x\";s:69:\"https://ps.w.org/classic-editor/assets/banner-772x250.png?rev=1998676\";}s:11:\"banners_rtl\";a:0:{}}s:21:\"imsanity/imsanity.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:22:\"w.org/plugins/imsanity\";s:4:\"slug\";s:8:\"imsanity\";s:6:\"plugin\";s:21:\"imsanity/imsanity.php\";s:11:\"new_version\";s:5:\"2.4.2\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/imsanity/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/imsanity.2.4.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:61:\"https://ps.w.org/imsanity/assets/icon-256x256.png?rev=1094749\";s:2:\"1x\";s:61:\"https://ps.w.org/imsanity/assets/icon-128x128.png?rev=1170755\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:62:\"https://ps.w.org/imsanity/assets/banner-772x250.png?rev=900541\";}s:11:\"banners_rtl\";a:0:{}}s:17:\"stream/stream.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:20:\"w.org/plugins/stream\";s:4:\"slug\";s:6:\"stream\";s:6:\"plugin\";s:17:\"stream/stream.php\";s:11:\"new_version\";s:5:\"3.2.3\";s:3:\"url\";s:37:\"https://wordpress.org/plugins/stream/\";s:7:\"package\";s:55:\"https://downloads.wordpress.org/plugin/stream.3.2.3.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:59:\"https://ps.w.org/stream/assets/icon-256x256.png?rev=1427219\";s:2:\"1x\";s:51:\"https://ps.w.org/stream/assets/icon.svg?rev=1427219\";s:3:\"svg\";s:51:\"https://ps.w.org/stream/assets/icon.svg?rev=1427219\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:62:\"https://ps.w.org/stream/assets/banner-1544x500.png?rev=1427219\";s:2:\"1x\";s:61:\"https://ps.w.org/stream/assets/banner-772x250.png?rev=1427219\";}s:11:\"banners_rtl\";a:0:{}}s:29:\"wp-mail-smtp/wp_mail_smtp.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:26:\"w.org/plugins/wp-mail-smtp\";s:4:\"slug\";s:12:\"wp-mail-smtp\";s:6:\"plugin\";s:29:\"wp-mail-smtp/wp_mail_smtp.php\";s:11:\"new_version\";s:5:\"1.4.2\";s:3:\"url\";s:43:\"https://wordpress.org/plugins/wp-mail-smtp/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wp-mail-smtp.1.4.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/wp-mail-smtp/assets/icon-256x256.png?rev=1755440\";s:2:\"1x\";s:65:\"https://ps.w.org/wp-mail-smtp/assets/icon-128x128.png?rev=1755440\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/wp-mail-smtp/assets/banner-1544x500.png?rev=1982773\";s:2:\"1x\";s:67:\"https://ps.w.org/wp-mail-smtp/assets/banner-772x250.png?rev=1982773\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(646, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.2.zip\";s:10:\"no_content\";s:68:\"https://downloads.wordpress.org/release/wordpress-5.2-no-content.zip\";s:11:\"new_bundled\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:3:\"5.2\";s:7:\"version\";s:3:\"5.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1558132727;s:15:\"version_checked\";s:3:\"5.2\";s:12:\"translations\";a:0:{}}', 'no'),
(648, 'recovery_keys', 'a:0:{}', 'yes'),
(649, '_site_transient_timeout_available_translations', '1558143531', 'no');
INSERT INTO `cs_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(650, '_site_transient_available_translations', 'a:117:{s:2:\"af\";a:8:{s:8:\"language\";s:2:\"af\";s:7:\"version\";s:5:\"5.0.4\";s:7:\"updated\";s:19:\"2019-05-16 12:52:45\";s:12:\"english_name\";s:9:\"Afrikaans\";s:11:\"native_name\";s:9:\"Afrikaans\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.0.4/af.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"af\";i:2;s:3:\"afr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Gaan voort\";}}s:2:\"ar\";a:8:{s:8:\"language\";s:2:\"ar\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-04 07:33:39\";s:12:\"english_name\";s:6:\"Arabic\";s:11:\"native_name\";s:14:\"العربية\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/5.2/ar.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:2;s:3:\"ara\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:3:\"ary\";a:8:{s:8:\"language\";s:3:\"ary\";s:7:\"version\";s:5:\"4.7.7\";s:7:\"updated\";s:19:\"2017-01-26 15:42:35\";s:12:\"english_name\";s:15:\"Moroccan Arabic\";s:11:\"native_name\";s:31:\"العربية المغربية\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.7/ary.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:3;s:3:\"ary\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:2:\"as\";a:8:{s:8:\"language\";s:2:\"as\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-22 18:59:07\";s:12:\"english_name\";s:8:\"Assamese\";s:11:\"native_name\";s:21:\"অসমীয়া\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/as.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"as\";i:2;s:3:\"asm\";i:3;s:3:\"asm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:3:\"azb\";a:8:{s:8:\"language\";s:3:\"azb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-12 20:34:31\";s:12:\"english_name\";s:17:\"South Azerbaijani\";s:11:\"native_name\";s:29:\"گؤنئی آذربایجان\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/azb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:3;s:3:\"azb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"az\";a:8:{s:8:\"language\";s:2:\"az\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-06 00:09:27\";s:12:\"english_name\";s:11:\"Azerbaijani\";s:11:\"native_name\";s:16:\"Azərbaycan dili\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/az.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:2;s:3:\"aze\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Davam\";}}s:3:\"bel\";a:8:{s:8:\"language\";s:3:\"bel\";s:7:\"version\";s:6:\"4.9.10\";s:7:\"updated\";s:19:\"2019-05-14 14:59:20\";s:12:\"english_name\";s:10:\"Belarusian\";s:11:\"native_name\";s:29:\"Беларуская мова\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/translation/core/4.9.10/bel.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"be\";i:2;s:3:\"bel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Працягнуць\";}}s:5:\"bg_BG\";a:8:{s:8:\"language\";s:5:\"bg_BG\";s:7:\"version\";s:5:\"5.0.4\";s:7:\"updated\";s:19:\"2018-12-11 16:43:39\";s:12:\"english_name\";s:9:\"Bulgarian\";s:11:\"native_name\";s:18:\"Български\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.0.4/bg_BG.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bg\";i:2;s:3:\"bul\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Напред\";}}s:5:\"bn_BD\";a:8:{s:8:\"language\";s:5:\"bn_BD\";s:7:\"version\";s:5:\"4.8.6\";s:7:\"updated\";s:19:\"2017-10-01 12:57:10\";s:12:\"english_name\";s:20:\"Bengali (Bangladesh)\";s:11:\"native_name\";s:15:\"বাংলা\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.8.6/bn_BD.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"bn\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:23:\"এগিয়ে চল.\";}}s:2:\"bo\";a:8:{s:8:\"language\";s:2:\"bo\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-17 06:00:41\";s:12:\"english_name\";s:7:\"Tibetan\";s:11:\"native_name\";s:21:\"བོད་ཡིག\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/5.2/bo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bo\";i:2;s:3:\"tib\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"མུ་མཐུད།\";}}s:5:\"bs_BA\";a:8:{s:8:\"language\";s:5:\"bs_BA\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-04 20:20:28\";s:12:\"english_name\";s:7:\"Bosnian\";s:11:\"native_name\";s:8:\"Bosanski\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/bs_BA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bs\";i:2;s:3:\"bos\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:2:\"ca\";a:8:{s:8:\"language\";s:2:\"ca\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-17 04:45:01\";s:12:\"english_name\";s:7:\"Catalan\";s:11:\"native_name\";s:7:\"Català\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/5.2/ca.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ca\";i:2;s:3:\"cat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:3:\"ceb\";a:8:{s:8:\"language\";s:3:\"ceb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-02 17:25:51\";s:12:\"english_name\";s:7:\"Cebuano\";s:11:\"native_name\";s:7:\"Cebuano\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/ceb.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"ceb\";i:3;s:3:\"ceb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Padayun\";}}s:5:\"cs_CZ\";a:8:{s:8:\"language\";s:5:\"cs_CZ\";s:7:\"version\";s:5:\"5.1.1\";s:7:\"updated\";s:19:\"2019-05-05 14:40:10\";s:12:\"english_name\";s:5:\"Czech\";s:11:\"native_name\";s:9:\"Čeština\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.1.1/cs_CZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cs\";i:2;s:3:\"ces\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:11:\"Pokračovat\";}}s:2:\"cy\";a:8:{s:8:\"language\";s:2:\"cy\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-08 07:53:38\";s:12:\"english_name\";s:5:\"Welsh\";s:11:\"native_name\";s:7:\"Cymraeg\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/5.2/cy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cy\";i:2;s:3:\"cym\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Parhau\";}}s:5:\"da_DK\";a:8:{s:8:\"language\";s:5:\"da_DK\";s:7:\"version\";s:5:\"5.1.1\";s:7:\"updated\";s:19:\"2019-03-25 20:13:18\";s:12:\"english_name\";s:6:\"Danish\";s:11:\"native_name\";s:5:\"Dansk\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.1.1/da_DK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"da\";i:2;s:3:\"dan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Fortsæt\";}}s:5:\"de_CH\";a:8:{s:8:\"language\";s:5:\"de_CH\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-09 10:39:56\";s:12:\"english_name\";s:20:\"German (Switzerland)\";s:11:\"native_name\";s:17:\"Deutsch (Schweiz)\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/de_CH.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:5:\"de_AT\";a:8:{s:8:\"language\";s:5:\"de_AT\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-07 21:15:55\";s:12:\"english_name\";s:16:\"German (Austria)\";s:11:\"native_name\";s:21:\"Deutsch (Österreich)\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/de_AT.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:14:\"de_CH_informal\";a:8:{s:8:\"language\";s:14:\"de_CH_informal\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-09 10:42:35\";s:12:\"english_name\";s:30:\"German (Switzerland, Informal)\";s:11:\"native_name\";s:21:\"Deutsch (Schweiz, Du)\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/core/5.2/de_CH_informal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:5:\"de_DE\";a:8:{s:8:\"language\";s:5:\"de_DE\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-17 08:04:59\";s:12:\"english_name\";s:6:\"German\";s:11:\"native_name\";s:7:\"Deutsch\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/de_DE.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:12:\"de_DE_formal\";a:8:{s:8:\"language\";s:12:\"de_DE_formal\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-17 08:05:13\";s:12:\"english_name\";s:15:\"German (Formal)\";s:11:\"native_name\";s:13:\"Deutsch (Sie)\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/translation/core/5.2/de_DE_formal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:3:\"dzo\";a:8:{s:8:\"language\";s:3:\"dzo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-06-29 08:59:03\";s:12:\"english_name\";s:8:\"Dzongkha\";s:11:\"native_name\";s:18:\"རྫོང་ཁ\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/dzo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"dz\";i:2;s:3:\"dzo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:2:\"el\";a:8:{s:8:\"language\";s:2:\"el\";s:7:\"version\";s:5:\"5.1.1\";s:7:\"updated\";s:19:\"2019-05-04 20:40:40\";s:12:\"english_name\";s:5:\"Greek\";s:11:\"native_name\";s:16:\"Ελληνικά\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.1.1/el.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"el\";i:2;s:3:\"ell\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Συνέχεια\";}}s:5:\"en_GB\";a:8:{s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-17 15:11:07\";s:12:\"english_name\";s:12:\"English (UK)\";s:11:\"native_name\";s:12:\"English (UK)\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/en_GB.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_ZA\";a:8:{s:8:\"language\";s:5:\"en_ZA\";s:7:\"version\";s:5:\"5.1.1\";s:7:\"updated\";s:19:\"2019-03-08 03:57:25\";s:12:\"english_name\";s:22:\"English (South Africa)\";s:11:\"native_name\";s:22:\"English (South Africa)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.1.1/en_ZA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_CA\";a:8:{s:8:\"language\";s:5:\"en_CA\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-17 15:16:29\";s:12:\"english_name\";s:16:\"English (Canada)\";s:11:\"native_name\";s:16:\"English (Canada)\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/en_CA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_AU\";a:8:{s:8:\"language\";s:5:\"en_AU\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-17 17:00:47\";s:12:\"english_name\";s:19:\"English (Australia)\";s:11:\"native_name\";s:19:\"English (Australia)\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/en_AU.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_NZ\";a:8:{s:8:\"language\";s:5:\"en_NZ\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-17 07:34:05\";s:12:\"english_name\";s:21:\"English (New Zealand)\";s:11:\"native_name\";s:21:\"English (New Zealand)\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/en_NZ.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"eo\";a:8:{s:8:\"language\";s:2:\"eo\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-05 12:47:00\";s:12:\"english_name\";s:9:\"Esperanto\";s:11:\"native_name\";s:9:\"Esperanto\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/5.2/eo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eo\";i:2;s:3:\"epo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Daŭrigi\";}}s:5:\"es_AR\";a:8:{s:8:\"language\";s:5:\"es_AR\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-14 18:14:12\";s:12:\"english_name\";s:19:\"Spanish (Argentina)\";s:11:\"native_name\";s:21:\"Español de Argentina\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/es_AR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_VE\";a:8:{s:8:\"language\";s:5:\"es_VE\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-11 15:51:57\";s:12:\"english_name\";s:19:\"Spanish (Venezuela)\";s:11:\"native_name\";s:21:\"Español de Venezuela\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/es_VE.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_ES\";a:8:{s:8:\"language\";s:5:\"es_ES\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-17 16:38:22\";s:12:\"english_name\";s:15:\"Spanish (Spain)\";s:11:\"native_name\";s:8:\"Español\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/es_ES.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_GT\";a:8:{s:8:\"language\";s:5:\"es_GT\";s:7:\"version\";s:3:\"5.1\";s:7:\"updated\";s:19:\"2019-03-02 06:35:01\";s:12:\"english_name\";s:19:\"Spanish (Guatemala)\";s:11:\"native_name\";s:21:\"Español de Guatemala\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.1/es_GT.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CL\";a:8:{s:8:\"language\";s:5:\"es_CL\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-03 20:24:32\";s:12:\"english_name\";s:15:\"Spanish (Chile)\";s:11:\"native_name\";s:17:\"Español de Chile\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/es_CL.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CR\";a:8:{s:8:\"language\";s:5:\"es_CR\";s:7:\"version\";s:3:\"5.0\";s:7:\"updated\";s:19:\"2018-12-06 21:26:01\";s:12:\"english_name\";s:20:\"Spanish (Costa Rica)\";s:11:\"native_name\";s:22:\"Español de Costa Rica\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.0/es_CR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_MX\";a:8:{s:8:\"language\";s:5:\"es_MX\";s:7:\"version\";s:3:\"5.0\";s:7:\"updated\";s:19:\"2018-12-07 18:38:30\";s:12:\"english_name\";s:16:\"Spanish (Mexico)\";s:11:\"native_name\";s:19:\"Español de México\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.0/es_MX.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CO\";a:8:{s:8:\"language\";s:5:\"es_CO\";s:7:\"version\";s:5:\"4.9.9\";s:7:\"updated\";s:19:\"2018-12-14 13:48:04\";s:12:\"english_name\";s:18:\"Spanish (Colombia)\";s:11:\"native_name\";s:20:\"Español de Colombia\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.9/es_CO.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_PE\";a:8:{s:8:\"language\";s:5:\"es_PE\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-09 09:36:22\";s:12:\"english_name\";s:14:\"Spanish (Peru)\";s:11:\"native_name\";s:17:\"Español de Perú\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/es_PE.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"et\";a:8:{s:8:\"language\";s:2:\"et\";s:7:\"version\";s:9:\"5.0-beta3\";s:7:\"updated\";s:19:\"2018-11-28 16:04:33\";s:12:\"english_name\";s:8:\"Estonian\";s:11:\"native_name\";s:5:\"Eesti\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/5.0-beta3/et.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"et\";i:2;s:3:\"est\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Jätka\";}}s:2:\"eu\";a:8:{s:8:\"language\";s:2:\"eu\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-09 21:12:23\";s:12:\"english_name\";s:6:\"Basque\";s:11:\"native_name\";s:7:\"Euskara\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.2/eu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eu\";i:2;s:3:\"eus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Jarraitu\";}}s:5:\"fa_IR\";a:8:{s:8:\"language\";s:5:\"fa_IR\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-17 14:57:05\";s:12:\"english_name\";s:7:\"Persian\";s:11:\"native_name\";s:10:\"فارسی\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/fa_IR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fa\";i:2;s:3:\"fas\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:2:\"fi\";a:8:{s:8:\"language\";s:2:\"fi\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-11 08:13:31\";s:12:\"english_name\";s:7:\"Finnish\";s:11:\"native_name\";s:5:\"Suomi\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/5.2/fi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fi\";i:2;s:3:\"fin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Jatka\";}}s:5:\"fr_FR\";a:8:{s:8:\"language\";s:5:\"fr_FR\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-17 16:28:25\";s:12:\"english_name\";s:15:\"French (France)\";s:11:\"native_name\";s:9:\"Français\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/fr_FR.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"fr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_CA\";a:8:{s:8:\"language\";s:5:\"fr_CA\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-17 20:58:21\";s:12:\"english_name\";s:15:\"French (Canada)\";s:11:\"native_name\";s:19:\"Français du Canada\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/fr_CA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_BE\";a:8:{s:8:\"language\";s:5:\"fr_BE\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-01-31 11:16:06\";s:12:\"english_name\";s:16:\"French (Belgium)\";s:11:\"native_name\";s:21:\"Français de Belgique\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/fr_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:3:\"fur\";a:8:{s:8:\"language\";s:3:\"fur\";s:7:\"version\";s:5:\"4.8.6\";s:7:\"updated\";s:19:\"2018-01-29 17:32:35\";s:12:\"english_name\";s:8:\"Friulian\";s:11:\"native_name\";s:8:\"Friulian\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8.6/fur.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"fur\";i:3;s:3:\"fur\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"gd\";a:8:{s:8:\"language\";s:2:\"gd\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-08-23 17:41:37\";s:12:\"english_name\";s:15:\"Scottish Gaelic\";s:11:\"native_name\";s:9:\"Gàidhlig\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/gd.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"gd\";i:2;s:3:\"gla\";i:3;s:3:\"gla\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"Lean air adhart\";}}s:5:\"gl_ES\";a:8:{s:8:\"language\";s:5:\"gl_ES\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-17 20:55:43\";s:12:\"english_name\";s:8:\"Galician\";s:11:\"native_name\";s:6:\"Galego\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/gl_ES.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gl\";i:2;s:3:\"glg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"gu\";a:8:{s:8:\"language\";s:2:\"gu\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-14 12:33:48\";s:12:\"english_name\";s:8:\"Gujarati\";s:11:\"native_name\";s:21:\"ગુજરાતી\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/gu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gu\";i:2;s:3:\"guj\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:31:\"ચાલુ રાખવું\";}}s:3:\"haz\";a:8:{s:8:\"language\";s:3:\"haz\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-05 00:59:09\";s:12:\"english_name\";s:8:\"Hazaragi\";s:11:\"native_name\";s:15:\"هزاره گی\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.4.2/haz.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"haz\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:5:\"he_IL\";a:8:{s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:5:\"5.1.1\";s:7:\"updated\";s:19:\"2019-04-28 16:40:42\";s:12:\"english_name\";s:6:\"Hebrew\";s:11:\"native_name\";s:16:\"עִבְרִית\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.1.1/he_IL.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"he\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"המשך\";}}s:5:\"hi_IN\";a:8:{s:8:\"language\";s:5:\"hi_IN\";s:7:\"version\";s:5:\"4.9.7\";s:7:\"updated\";s:19:\"2018-06-17 09:33:44\";s:12:\"english_name\";s:5:\"Hindi\";s:11:\"native_name\";s:18:\"हिन्दी\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.7/hi_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hi\";i:2;s:3:\"hin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"जारी\";}}s:2:\"hr\";a:8:{s:8:\"language\";s:2:\"hr\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-17 07:53:28\";s:12:\"english_name\";s:8:\"Croatian\";s:11:\"native_name\";s:8:\"Hrvatski\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/5.2/hr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hr\";i:2;s:3:\"hrv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:5:\"hu_HU\";a:8:{s:8:\"language\";s:5:\"hu_HU\";s:7:\"version\";s:5:\"5.1.1\";s:7:\"updated\";s:19:\"2019-03-19 14:36:40\";s:12:\"english_name\";s:9:\"Hungarian\";s:11:\"native_name\";s:6:\"Magyar\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.1.1/hu_HU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hu\";i:2;s:3:\"hun\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Folytatás\";}}s:2:\"hy\";a:8:{s:8:\"language\";s:2:\"hy\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-03 16:21:10\";s:12:\"english_name\";s:8:\"Armenian\";s:11:\"native_name\";s:14:\"Հայերեն\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/hy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hy\";i:2;s:3:\"hye\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Շարունակել\";}}s:5:\"id_ID\";a:8:{s:8:\"language\";s:5:\"id_ID\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-07-28 13:16:13\";s:12:\"english_name\";s:10:\"Indonesian\";s:11:\"native_name\";s:16:\"Bahasa Indonesia\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/id_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"id\";i:2;s:3:\"ind\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Lanjutkan\";}}s:5:\"is_IS\";a:8:{s:8:\"language\";s:5:\"is_IS\";s:7:\"version\";s:6:\"4.7.11\";s:7:\"updated\";s:19:\"2018-09-20 11:13:37\";s:12:\"english_name\";s:9:\"Icelandic\";s:11:\"native_name\";s:9:\"Íslenska\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.7.11/is_IS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"is\";i:2;s:3:\"isl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Áfram\";}}s:5:\"it_IT\";a:8:{s:8:\"language\";s:5:\"it_IT\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-15 17:17:49\";s:12:\"english_name\";s:7:\"Italian\";s:11:\"native_name\";s:8:\"Italiano\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/it_IT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"it\";i:2;s:3:\"ita\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:2:\"ja\";a:8:{s:8:\"language\";s:2:\"ja\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-11 20:52:37\";s:12:\"english_name\";s:8:\"Japanese\";s:11:\"native_name\";s:9:\"日本語\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/5.2/ja.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"ja\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"続ける\";}}s:5:\"jv_ID\";a:8:{s:8:\"language\";s:5:\"jv_ID\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-24 13:53:29\";s:12:\"english_name\";s:8:\"Javanese\";s:11:\"native_name\";s:9:\"Basa Jawa\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/jv_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"jv\";i:2;s:3:\"jav\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Nerusaké\";}}s:5:\"ka_GE\";a:8:{s:8:\"language\";s:5:\"ka_GE\";s:7:\"version\";s:3:\"5.1\";s:7:\"updated\";s:19:\"2019-02-21 08:17:32\";s:12:\"english_name\";s:8:\"Georgian\";s:11:\"native_name\";s:21:\"ქართული\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.1/ka_GE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ka\";i:2;s:3:\"kat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"გაგრძელება\";}}s:3:\"kab\";a:8:{s:8:\"language\";s:3:\"kab\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-21 14:15:57\";s:12:\"english_name\";s:6:\"Kabyle\";s:11:\"native_name\";s:9:\"Taqbaylit\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.8/kab.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"kab\";i:3;s:3:\"kab\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Kemmel\";}}s:2:\"kk\";a:8:{s:8:\"language\";s:2:\"kk\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-12 08:08:32\";s:12:\"english_name\";s:6:\"Kazakh\";s:11:\"native_name\";s:19:\"Қазақ тілі\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.5/kk.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"kk\";i:2;s:3:\"kaz\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Жалғастыру\";}}s:2:\"km\";a:8:{s:8:\"language\";s:2:\"km\";s:7:\"version\";s:5:\"5.0.3\";s:7:\"updated\";s:19:\"2019-01-09 07:34:10\";s:12:\"english_name\";s:5:\"Khmer\";s:11:\"native_name\";s:27:\"ភាសាខ្មែរ\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.0.3/km.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"km\";i:2;s:3:\"khm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"បន្ត\";}}s:2:\"kn\";a:8:{s:8:\"language\";s:2:\"kn\";s:7:\"version\";s:6:\"4.9.10\";s:7:\"updated\";s:19:\"2019-05-08 04:00:57\";s:12:\"english_name\";s:7:\"Kannada\";s:11:\"native_name\";s:15:\"ಕನ್ನಡ\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.10/kn.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"kn\";i:2;s:3:\"kan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"ಮುಂದುವರೆಸಿ\";}}s:5:\"ko_KR\";a:8:{s:8:\"language\";s:5:\"ko_KR\";s:7:\"version\";s:5:\"5.0.3\";s:7:\"updated\";s:19:\"2019-01-09 14:27:41\";s:12:\"english_name\";s:6:\"Korean\";s:11:\"native_name\";s:9:\"한국어\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.0.3/ko_KR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ko\";i:2;s:3:\"kor\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"계속\";}}s:3:\"ckb\";a:8:{s:8:\"language\";s:3:\"ckb\";s:7:\"version\";s:5:\"4.9.9\";s:7:\"updated\";s:19:\"2018-12-18 14:32:44\";s:12:\"english_name\";s:16:\"Kurdish (Sorani)\";s:11:\"native_name\";s:13:\"كوردی‎\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.9/ckb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ku\";i:3;s:3:\"ckb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"به‌رده‌وام به‌\";}}s:2:\"lo\";a:8:{s:8:\"language\";s:2:\"lo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 09:59:23\";s:12:\"english_name\";s:3:\"Lao\";s:11:\"native_name\";s:21:\"ພາສາລາວ\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/lo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lo\";i:2;s:3:\"lao\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:18:\"ຕໍ່​ໄປ\";}}s:5:\"lt_LT\";a:8:{s:8:\"language\";s:5:\"lt_LT\";s:7:\"version\";s:5:\"5.0.3\";s:7:\"updated\";s:19:\"2019-01-09 18:11:00\";s:12:\"english_name\";s:10:\"Lithuanian\";s:11:\"native_name\";s:15:\"Lietuvių kalba\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.0.3/lt_LT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lt\";i:2;s:3:\"lit\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Tęsti\";}}s:2:\"lv\";a:8:{s:8:\"language\";s:2:\"lv\";s:7:\"version\";s:6:\"4.7.13\";s:7:\"updated\";s:19:\"2019-05-10 10:24:08\";s:12:\"english_name\";s:7:\"Latvian\";s:11:\"native_name\";s:16:\"Latviešu valoda\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.13/lv.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lv\";i:2;s:3:\"lav\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Turpināt\";}}s:5:\"mk_MK\";a:8:{s:8:\"language\";s:5:\"mk_MK\";s:7:\"version\";s:5:\"4.7.7\";s:7:\"updated\";s:19:\"2017-01-26 15:54:41\";s:12:\"english_name\";s:10:\"Macedonian\";s:11:\"native_name\";s:31:\"Македонски јазик\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.7/mk_MK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mk\";i:2;s:3:\"mkd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Продолжи\";}}s:5:\"ml_IN\";a:8:{s:8:\"language\";s:5:\"ml_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:43:32\";s:12:\"english_name\";s:9:\"Malayalam\";s:11:\"native_name\";s:18:\"മലയാളം\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ml_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ml\";i:2;s:3:\"mal\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:18:\"തുടരുക\";}}s:2:\"mn\";a:8:{s:8:\"language\";s:2:\"mn\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-12 07:29:35\";s:12:\"english_name\";s:9:\"Mongolian\";s:11:\"native_name\";s:12:\"Монгол\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/mn.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mn\";i:2;s:3:\"mon\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"Үргэлжлүүлэх\";}}s:2:\"mr\";a:8:{s:8:\"language\";s:2:\"mr\";s:7:\"version\";s:5:\"4.8.6\";s:7:\"updated\";s:19:\"2018-02-13 07:38:55\";s:12:\"english_name\";s:7:\"Marathi\";s:11:\"native_name\";s:15:\"मराठी\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.8.6/mr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mr\";i:2;s:3:\"mar\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"सुरु ठेवा\";}}s:5:\"ms_MY\";a:8:{s:8:\"language\";s:5:\"ms_MY\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-30 20:27:25\";s:12:\"english_name\";s:5:\"Malay\";s:11:\"native_name\";s:13:\"Bahasa Melayu\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/ms_MY.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ms\";i:2;s:3:\"msa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Teruskan\";}}s:5:\"my_MM\";a:8:{s:8:\"language\";s:5:\"my_MM\";s:7:\"version\";s:6:\"4.1.20\";s:7:\"updated\";s:19:\"2015-03-26 15:57:42\";s:12:\"english_name\";s:17:\"Myanmar (Burmese)\";s:11:\"native_name\";s:15:\"ဗမာစာ\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.1.20/my_MM.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"my\";i:2;s:3:\"mya\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:54:\"ဆက်လက်လုပ်ဆောင်ပါ။\";}}s:5:\"nb_NO\";a:8:{s:8:\"language\";s:5:\"nb_NO\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-15 16:55:21\";s:12:\"english_name\";s:19:\"Norwegian (Bokmål)\";s:11:\"native_name\";s:13:\"Norsk bokmål\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/nb_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nb\";i:2;s:3:\"nob\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Fortsett\";}}s:5:\"ne_NP\";a:8:{s:8:\"language\";s:5:\"ne_NP\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-27 10:30:26\";s:12:\"english_name\";s:6:\"Nepali\";s:11:\"native_name\";s:18:\"नेपाली\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/ne_NP.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ne\";i:2;s:3:\"nep\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:43:\"जारी राख्नुहोस्\";}}s:5:\"nl_BE\";a:8:{s:8:\"language\";s:5:\"nl_BE\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-17 05:17:15\";s:12:\"english_name\";s:15:\"Dutch (Belgium)\";s:11:\"native_name\";s:20:\"Nederlands (België)\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/nl_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:12:\"nl_NL_formal\";a:8:{s:8:\"language\";s:12:\"nl_NL_formal\";s:7:\"version\";s:5:\"5.1.1\";s:7:\"updated\";s:19:\"2019-03-23 09:40:21\";s:12:\"english_name\";s:14:\"Dutch (Formal)\";s:11:\"native_name\";s:20:\"Nederlands (Formeel)\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/core/5.1.1/nl_NL_formal.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nl_NL\";a:8:{s:8:\"language\";s:5:\"nl_NL\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-17 15:32:59\";s:12:\"english_name\";s:5:\"Dutch\";s:11:\"native_name\";s:10:\"Nederlands\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/nl_NL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nn_NO\";a:8:{s:8:\"language\";s:5:\"nn_NO\";s:7:\"version\";s:5:\"5.1.1\";s:7:\"updated\";s:19:\"2019-03-09 16:36:42\";s:12:\"english_name\";s:19:\"Norwegian (Nynorsk)\";s:11:\"native_name\";s:13:\"Norsk nynorsk\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.1.1/nn_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nn\";i:2;s:3:\"nno\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Hald fram\";}}s:3:\"oci\";a:8:{s:8:\"language\";s:3:\"oci\";s:7:\"version\";s:5:\"4.8.3\";s:7:\"updated\";s:19:\"2017-08-25 10:03:08\";s:12:\"english_name\";s:7:\"Occitan\";s:11:\"native_name\";s:7:\"Occitan\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8.3/oci.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"oc\";i:2;s:3:\"oci\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Contunhar\";}}s:5:\"pa_IN\";a:8:{s:8:\"language\";s:5:\"pa_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-16 05:19:43\";s:12:\"english_name\";s:7:\"Punjabi\";s:11:\"native_name\";s:18:\"ਪੰਜਾਬੀ\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/pa_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pa\";i:2;s:3:\"pan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"ਜਾਰੀ ਰੱਖੋ\";}}s:5:\"pl_PL\";a:8:{s:8:\"language\";s:5:\"pl_PL\";s:7:\"version\";s:8:\"5.2-beta\";s:7:\"updated\";s:19:\"2019-04-09 16:46:27\";s:12:\"english_name\";s:6:\"Polish\";s:11:\"native_name\";s:6:\"Polski\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/translation/core/5.2-beta/pl_PL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pl\";i:2;s:3:\"pol\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Kontynuuj\";}}s:2:\"ps\";a:8:{s:8:\"language\";s:2:\"ps\";s:7:\"version\";s:6:\"4.1.20\";s:7:\"updated\";s:19:\"2015-03-29 22:19:48\";s:12:\"english_name\";s:6:\"Pashto\";s:11:\"native_name\";s:8:\"پښتو\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.1.20/ps.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ps\";i:2;s:3:\"pus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:19:\"دوام ورکړه\";}}s:5:\"pt_BR\";a:8:{s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-06 22:27:27\";s:12:\"english_name\";s:19:\"Portuguese (Brazil)\";s:11:\"native_name\";s:20:\"Português do Brasil\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/pt_BR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pt\";i:2;s:3:\"por\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"pt_PT\";a:8:{s:8:\"language\";s:5:\"pt_PT\";s:7:\"version\";s:5:\"5.1.1\";s:7:\"updated\";s:19:\"2019-04-08 09:37:30\";s:12:\"english_name\";s:21:\"Portuguese (Portugal)\";s:11:\"native_name\";s:10:\"Português\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.1.1/pt_PT.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:10:\"pt_PT_ao90\";a:8:{s:8:\"language\";s:10:\"pt_PT_ao90\";s:7:\"version\";s:3:\"5.1\";s:7:\"updated\";s:19:\"2019-02-22 12:37:09\";s:12:\"english_name\";s:27:\"Portuguese (Portugal, AO90)\";s:11:\"native_name\";s:17:\"Português (AO90)\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/translation/core/5.1/pt_PT_ao90.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"pt_AO\";a:8:{s:8:\"language\";s:5:\"pt_AO\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-08 11:59:02\";s:12:\"english_name\";s:19:\"Portuguese (Angola)\";s:11:\"native_name\";s:20:\"Português de Angola\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/pt_AO.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:3:\"rhg\";a:8:{s:8:\"language\";s:3:\"rhg\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-16 13:03:18\";s:12:\"english_name\";s:8:\"Rohingya\";s:11:\"native_name\";s:8:\"Ruáinga\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/rhg.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"rhg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:5:\"ro_RO\";a:8:{s:8:\"language\";s:5:\"ro_RO\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-17 21:26:54\";s:12:\"english_name\";s:8:\"Romanian\";s:11:\"native_name\";s:8:\"Română\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/ro_RO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ro\";i:2;s:3:\"ron\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuă\";}}s:5:\"ru_RU\";a:8:{s:8:\"language\";s:5:\"ru_RU\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-17 16:43:11\";s:12:\"english_name\";s:7:\"Russian\";s:11:\"native_name\";s:14:\"Русский\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/ru_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ru\";i:2;s:3:\"rus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продолжить\";}}s:3:\"sah\";a:8:{s:8:\"language\";s:3:\"sah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-21 02:06:41\";s:12:\"english_name\";s:5:\"Sakha\";s:11:\"native_name\";s:14:\"Сахалыы\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/sah.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"sah\";i:3;s:3:\"sah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Салҕаа\";}}s:5:\"si_LK\";a:8:{s:8:\"language\";s:5:\"si_LK\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 06:00:52\";s:12:\"english_name\";s:7:\"Sinhala\";s:11:\"native_name\";s:15:\"සිංහල\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/si_LK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"si\";i:2;s:3:\"sin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:44:\"දිගටම කරගෙන යන්න\";}}s:5:\"sk_SK\";a:8:{s:8:\"language\";s:5:\"sk_SK\";s:7:\"version\";s:5:\"5.1.1\";s:7:\"updated\";s:19:\"2019-03-08 15:30:32\";s:12:\"english_name\";s:6:\"Slovak\";s:11:\"native_name\";s:11:\"Slovenčina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.1.1/sk_SK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sk\";i:2;s:3:\"slk\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Pokračovať\";}}s:3:\"skr\";a:8:{s:8:\"language\";s:3:\"skr\";s:7:\"version\";s:5:\"5.1.1\";s:7:\"updated\";s:19:\"2019-03-09 14:38:41\";s:12:\"english_name\";s:7:\"Saraiki\";s:11:\"native_name\";s:14:\"سرائیکی\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.1.1/skr.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"skr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:17:\"جاری رکھو\";}}s:5:\"sl_SI\";a:8:{s:8:\"language\";s:5:\"sl_SI\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-04 13:33:13\";s:12:\"english_name\";s:9:\"Slovenian\";s:11:\"native_name\";s:13:\"Slovenščina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/sl_SI.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sl\";i:2;s:3:\"slv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Nadaljuj\";}}s:2:\"sq\";a:8:{s:8:\"language\";s:2:\"sq\";s:7:\"version\";s:5:\"5.1.1\";s:7:\"updated\";s:19:\"2019-04-02 15:10:17\";s:12:\"english_name\";s:8:\"Albanian\";s:11:\"native_name\";s:5:\"Shqip\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.1.1/sq.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sq\";i:2;s:3:\"sqi\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Vazhdo\";}}s:5:\"sr_RS\";a:8:{s:8:\"language\";s:5:\"sr_RS\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-07 19:44:35\";s:12:\"english_name\";s:7:\"Serbian\";s:11:\"native_name\";s:23:\"Српски језик\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/sr_RS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sr\";i:2;s:3:\"srp\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:14:\"Настави\";}}s:5:\"sv_SE\";a:8:{s:8:\"language\";s:5:\"sv_SE\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-17 14:03:40\";s:12:\"english_name\";s:7:\"Swedish\";s:11:\"native_name\";s:7:\"Svenska\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/sv_SE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sv\";i:2;s:3:\"swe\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Fortsätt\";}}s:3:\"szl\";a:8:{s:8:\"language\";s:3:\"szl\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-24 19:58:14\";s:12:\"english_name\";s:8:\"Silesian\";s:11:\"native_name\";s:17:\"Ślōnskŏ gŏdka\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/szl.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"szl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:13:\"Kōntynuować\";}}s:5:\"ta_IN\";a:8:{s:8:\"language\";s:5:\"ta_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:22:47\";s:12:\"english_name\";s:5:\"Tamil\";s:11:\"native_name\";s:15:\"தமிழ்\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ta_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ta\";i:2;s:3:\"tam\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"தொடரவும்\";}}s:2:\"te\";a:8:{s:8:\"language\";s:2:\"te\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-26 15:47:39\";s:12:\"english_name\";s:6:\"Telugu\";s:11:\"native_name\";s:18:\"తెలుగు\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/te.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"te\";i:2;s:3:\"tel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"కొనసాగించు\";}}s:2:\"th\";a:8:{s:8:\"language\";s:2:\"th\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-17 12:39:58\";s:12:\"english_name\";s:4:\"Thai\";s:11:\"native_name\";s:9:\"ไทย\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/5.2/th.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"th\";i:2;s:3:\"tha\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"ต่อไป\";}}s:2:\"tl\";a:8:{s:8:\"language\";s:2:\"tl\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-30 02:38:08\";s:12:\"english_name\";s:7:\"Tagalog\";s:11:\"native_name\";s:7:\"Tagalog\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/tl.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tl\";i:2;s:3:\"tgl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Magpatuloy\";}}s:5:\"tr_TR\";a:8:{s:8:\"language\";s:5:\"tr_TR\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-11 18:24:05\";s:12:\"english_name\";s:7:\"Turkish\";s:11:\"native_name\";s:8:\"Türkçe\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/tr_TR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tr\";i:2;s:3:\"tur\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Devam\";}}s:5:\"tt_RU\";a:8:{s:8:\"language\";s:5:\"tt_RU\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-20 20:20:50\";s:12:\"english_name\";s:5:\"Tatar\";s:11:\"native_name\";s:19:\"Татар теле\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/tt_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tt\";i:2;s:3:\"tat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:17:\"дәвам итү\";}}s:3:\"tah\";a:8:{s:8:\"language\";s:3:\"tah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-06 18:39:39\";s:12:\"english_name\";s:8:\"Tahitian\";s:11:\"native_name\";s:10:\"Reo Tahiti\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/tah.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"ty\";i:2;s:3:\"tah\";i:3;s:3:\"tah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:5:\"ug_CN\";a:8:{s:8:\"language\";s:5:\"ug_CN\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-04-12 12:31:53\";s:12:\"english_name\";s:6:\"Uighur\";s:11:\"native_name\";s:16:\"ئۇيغۇرچە\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/ug_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ug\";i:2;s:3:\"uig\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:26:\"داۋاملاشتۇرۇش\";}}s:2:\"uk\";a:8:{s:8:\"language\";s:2:\"uk\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-17 21:12:43\";s:12:\"english_name\";s:9:\"Ukrainian\";s:11:\"native_name\";s:20:\"Українська\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/5.2/uk.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uk\";i:2;s:3:\"ukr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продовжити\";}}s:2:\"ur\";a:8:{s:8:\"language\";s:2:\"ur\";s:7:\"version\";s:5:\"5.1.1\";s:7:\"updated\";s:19:\"2019-03-31 10:39:40\";s:12:\"english_name\";s:4:\"Urdu\";s:11:\"native_name\";s:8:\"اردو\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.1.1/ur.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ur\";i:2;s:3:\"urd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:19:\"جاری رکھیں\";}}s:5:\"uz_UZ\";a:8:{s:8:\"language\";s:5:\"uz_UZ\";s:7:\"version\";s:5:\"5.0.3\";s:7:\"updated\";s:19:\"2019-01-23 12:32:40\";s:12:\"english_name\";s:5:\"Uzbek\";s:11:\"native_name\";s:11:\"O‘zbekcha\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.0.3/uz_UZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uz\";i:2;s:3:\"uzb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:11:\"Davom etish\";}}s:2:\"vi\";a:8:{s:8:\"language\";s:2:\"vi\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-17 08:00:00\";s:12:\"english_name\";s:10:\"Vietnamese\";s:11:\"native_name\";s:14:\"Tiếng Việt\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/5.2/vi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"vi\";i:2;s:3:\"vie\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Tiếp tục\";}}s:5:\"zh_TW\";a:8:{s:8:\"language\";s:5:\"zh_TW\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-10 05:35:48\";s:12:\"english_name\";s:16:\"Chinese (Taiwan)\";s:11:\"native_name\";s:12:\"繁體中文\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/zh_TW.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}s:5:\"zh_CN\";a:8:{s:8:\"language\";s:5:\"zh_CN\";s:7:\"version\";s:5:\"5.0.3\";s:7:\"updated\";s:19:\"2018-12-21 00:57:14\";s:12:\"english_name\";s:15:\"Chinese (China)\";s:11:\"native_name\";s:12:\"简体中文\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.0.3/zh_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"继续\";}}s:5:\"zh_HK\";a:8:{s:8:\"language\";s:5:\"zh_HK\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-09 17:07:08\";s:12:\"english_name\";s:19:\"Chinese (Hong Kong)\";s:11:\"native_name\";s:16:\"香港中文版	\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/zh_HK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}}', 'no'),
(653, '_transient_timeout_GFCache_b56c26d66c75565f1c7a1e6491625d45', '1558132890', 'no'),
(654, '_transient_GFCache_b56c26d66c75565f1c7a1e6491625d45', '1', 'no'),
(659, '_transient_timeout_GFCache_c4f39db4bb8b6524b9a4f6d7097c0f23', '1558132893', 'no'),
(660, '_transient_GFCache_c4f39db4bb8b6524b9a4f6d7097c0f23', '1', 'no'),
(661, '_transient_timeout_GFCache_c8c5064efef84069449e7d7ba2e1dc22', '1558132897', 'no'),
(662, '_transient_GFCache_c8c5064efef84069449e7d7ba2e1dc22', '1', 'no'),
(663, '_transient_timeout_GFCache_5c61b3e751b51e5cdf52ee3313bcc2a9', '1558133212', 'no'),
(664, '_transient_GFCache_5c61b3e751b51e5cdf52ee3313bcc2a9', 'a:0:{}', 'no'),
(665, '_transient_timeout_GFCache_db097dabe62c41e2abaa5e3390338449', '1558133212', 'no'),
(666, '_transient_GFCache_db097dabe62c41e2abaa5e3390338449', 'a:1:{i:0;O:8:\"stdClass\":2:{s:7:\"form_id\";s:1:\"1\";s:10:\"view_count\";s:3:\"119\";}}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `cs_postmeta`
--

CREATE TABLE `cs_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cs_postmeta`
--

INSERT INTO `cs_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'views/template-home.blade.php'),
(3, 2, '_edit_lock', '1558133182:1'),
(4, 2, '_edit_last', '1'),
(5, 6, '_edit_lock', '1557268224:1'),
(6, 6, '_edit_last', '1'),
(7, 22, '_edit_lock', '1557266345:1'),
(21, 31, '_wp_attached_file', '2019/04/300.png'),
(22, 31, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:300;s:6:\"height\";i:300;s:4:\"file\";s:15:\"2019/04/300.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"300-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(23, 32, '_edit_lock', '1557266359:1'),
(24, 32, '_edit_last', '1'),
(25, 36, '_wp_attached_file', '2019/04/placeholder-1920x600.png'),
(26, 36, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:600;s:4:\"file\";s:32:\"2019/04/placeholder-1920x600.png\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"placeholder-1920x600-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"placeholder-1920x600-300x94.png\";s:5:\"width\";i:300;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"placeholder-1920x600-768x240.png\";s:5:\"width\";i:768;s:6:\"height\";i:240;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:33:\"placeholder-1920x600-1024x320.png\";s:5:\"width\";i:1024;s:6:\"height\";i:320;s:9:\"mime-type\";s:9:\"image/png\";}s:8:\"w960x600\";a:4:{s:4:\"file\";s:32:\"placeholder-1920x600-960x600.png\";s:5:\"width\";i:960;s:6:\"height\";i:600;s:9:\"mime-type\";s:9:\"image/png\";}s:8:\"w760x420\";a:4:{s:4:\"file\";s:32:\"placeholder-1920x600-615x600.png\";s:5:\"width\";i:615;s:6:\"height\";i:600;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:13:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}s:14:\"resized_images\";a:1:{i:0;s:10:\"1024x768_c\";}}}'),
(31, 2, 'repeater_heroslider', ''),
(32, 2, '_repeater_heroslider', 'field_5b96d6257871e'),
(41, 2, 'section_creator', ''),
(42, 2, '_section_creator', 'field_5c3fb135f22f5'),
(59, 39, '_wp_attached_file', '2019/04/xga.png'),
(60, 39, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1024;s:6:\"height\";i:768;s:4:\"file\";s:15:\"2019/04/xga.png\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"xga-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"xga-300x225.png\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"xga-768x576.png\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:16:\"xga-1024x768.png\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"w1920x600\";a:4:{s:4:\"file\";s:16:\"xga-1024x600.png\";s:5:\"width\";i:1024;s:6:\"height\";i:600;s:9:\"mime-type\";s:9:\"image/png\";}s:8:\"w960x600\";a:4:{s:4:\"file\";s:15:\"xga-960x600.png\";s:5:\"width\";i:960;s:6:\"height\";i:600;s:9:\"mime-type\";s:9:\"image/png\";}s:8:\"w760x420\";a:4:{s:4:\"file\";s:15:\"xga-615x650.png\";s:5:\"width\";i:615;s:6:\"height\";i:650;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(280, 39, '_sol_has_slider', ''),
(281, 36, '_sol_has_slider', ''),
(307, 2, 'section_creator_0_background_field', '#42668f'),
(308, 2, '_section_creator_0_background_field', 'field_5c3fb19ff22f7'),
(309, 2, 'section_creator_0_default_editor', '[row]\r\n\r\n[column columns=6]\r\n\r\n<strong>Hours from</strong>\r\nSunday 11:00 am – 9:00 pm\r\nMonday 11:00 am – 8:00 pm\r\nTuesday 11:00 am – 8:00 pm\r\nWednesday 11:00 am – 8:00 pm\r\nThursday 11:00 am – 8:00 pm\r\nFriday 11:00 am – 9:00 pm\r\nSaturday 11:00 am – 9:00 pm\r\n\r\n[/column]\r\n\r\n[column columns=6]\r\n\r\n<img src=\"http://coming-soon.test/app/uploads/2019/05/restaurant.jpg\" alt=\"\" width=\"1000\" height=\"668\" class=\"alignnone size-full wp-image-69\" />\r\n\r\n[/column]\r\n\r\n[/row]'),
(310, 2, '_section_creator_0_default_editor', 'field_5c3fb1c9f22f8'),
(319, 73, '_edit_lock', '1557266364:1'),
(320, 2, 'section_f_creator', ''),
(321, 2, '_section_f_creator', 'field_5cba18ec5a507'),
(900, 104, 'repeater_heroslider', '1'),
(901, 104, '_repeater_heroslider', 'field_5b96d6257871e'),
(902, 104, 'section_creator', '1'),
(903, 104, '_section_creator', 'field_5c3fb135f22f5'),
(904, 104, 'section_creator_0_background_field', '#42668f'),
(905, 104, '_section_creator_0_background_field', 'field_5c3fb19ff22f7'),
(906, 104, 'section_creator_0_default_editor', '[row]\r\n\r\n[column columns=6]\r\n\r\n<strong>Hours from</strong>\r\nSunday 11:00 am – 9:00 pm\r\nMonday 11:00 am – 8:00 pm\r\nTuesday 11:00 am – 8:00 pm\r\nWednesday 11:00 am – 8:00 pm\r\nThursday 11:00 am – 8:00 pm\r\nFriday 11:00 am – 9:00 pm\r\nSaturday 11:00 am – 9:00 pm\r\n\r\n[/column]\r\n\r\n[column columns=6]\r\n\r\n<img src=\"http://coming-soon.test/app/uploads/2019/05/restaurant.jpg\" alt=\"\" width=\"1000\" height=\"668\" class=\"alignnone size-full wp-image-69\" />\r\n\r\n[/column]\r\n\r\n[/row]'),
(907, 104, '_section_creator_0_default_editor', 'field_5c3fb1c9f22f8'),
(908, 104, 'section_f_creator', '2'),
(909, 104, '_section_f_creator', 'field_5cba18ec5a507'),
(910, 104, 'repeater_heroslider_0_sub_field_image', '86'),
(911, 104, '_repeater_heroslider_0_sub_field_image', 'field_5b96d6817871f'),
(912, 104, 'repeater_heroslider_0_sub_field_blurb', ''),
(913, 104, '_repeater_heroslider_0_sub_field_blurb', 'field_5b96d69378720'),
(914, 104, 'section_f_creator_0_order_num', 'order-one'),
(915, 104, '_section_f_creator_0_order_num', 'field_5cba1834ed9e6'),
(916, 104, 'section_f_creator_0_background_field', '69'),
(917, 104, '_section_f_creator_0_background_field', 'field_5cba180ced9e5'),
(918, 104, 'section_f_creator_0_order_num_two', 'order-two'),
(919, 104, '_section_f_creator_0_order_num_two', 'field_5cba1a10586b3'),
(920, 104, 'section_f_creator_0_default_f_editor', '<h1>Hours Open</h1>\r\n<strong>Sunday</strong> 11:00 am – 9:00 pm\r\n<strong>Monday</strong> 11:00 am – 8:00 pm\r\n<strong>Tuesday</strong> 11:00 am – 8:00 pm\r\n<strong>Wednesday</strong> 11:00 am – 8:00 pm\r\n<strong>Thursday</strong> 11:00 am – 8:00 pm\r\n<strong>Friday</strong> 11:00 am – 9:00 pm\r\n<strong>Saturday</strong> 11:00 am – 9:00 pm'),
(921, 104, '_section_f_creator_0_default_f_editor', 'field_5cba1a24586b4'),
(922, 104, 'section_f_creator_0_bg_clr_f', '#42668f'),
(923, 104, '_section_f_creator_0_bg_clr_f', 'field_5cba1accd7240'),
(924, 104, 'section_f_creator_0_dark_back_color', 'dark-back'),
(925, 104, '_section_f_creator_0_dark_back_color', 'field_5cba19bdcf299'),
(926, 104, 'section_f_creator_1_order_num', 'order-two'),
(927, 104, '_section_f_creator_1_order_num', 'field_5cba1834ed9e6'),
(928, 104, 'section_f_creator_1_background_field', '86'),
(929, 104, '_section_f_creator_1_background_field', 'field_5cba180ced9e5'),
(930, 104, 'section_f_creator_1_order_num_two', 'order-one'),
(931, 104, '_section_f_creator_1_order_num_two', 'field_5cba1a10586b3'),
(932, 104, 'section_f_creator_1_default_f_editor', '<h1>Here’s What You’ll Find On Our Three Floors:</h1>\r\n<strong>FIRST FLOOR:</strong>\r\nThis is a great hang-out area with outdoor picnic tables for those times when you want a bit more seclusion from the hustle and bustle.\r\n\r\n<strong>SECOND FLOOR:</strong>\r\nThe Blue Water Bar &amp; Grill provides spacious indoor seating plus a bar which extends all-around for indoor and outdoor seating. Enjoy exceptional local seafood, steak, chicken, hamburgers, salads and pizzas. Eat on-site, or order for take-out.\r\n\r\n<strong>THIRD FLOOR:</strong>\r\nOur 1,500 square foot banquet area offers large windowed walls, balconies and breathtaking 360-views. This spacious facility includes a long granite bar that circles the room with a utility sink and other bar amenities.'),
(933, 104, '_section_f_creator_1_default_f_editor', 'field_5cba1a24586b4'),
(934, 104, 'section_f_creator_1_bg_clr_f', ''),
(935, 104, '_section_f_creator_1_bg_clr_f', 'field_5cba1accd7240'),
(936, 104, 'section_f_creator_1_dark_back_color', ''),
(937, 104, '_section_f_creator_1_dark_back_color', 'field_5cba19bdcf299'),
(938, 104, 'section_creator_0_background_field_contained', ''),
(939, 104, '_section_creator_0_background_field_contained', 'field_5c3fb19ff22f7'),
(940, 104, 'section_creator_0_background_color', ''),
(941, 104, '_section_creator_0_background_color', 'field_5cbdfb3a31d8e'),
(942, 104, 'section_creator_0_dark_background', ''),
(943, 104, '_section_creator_0_dark_background', 'field_5cba4691d9c3d'),
(944, 104, 'section_creator_0_contained_editor', '<hr />\r\n\r\n<h2 style=\"text-align: center;\">Contact us today!</h2>\r\n<p style=\"text-align: center;\">[gravityform id=\"1\" title=\"false\" description=\"false\"]</p>'),
(945, 104, '_section_creator_0_contained_editor', 'field_5c3fb1c9f22f8'),
(946, 105, 'repeater_heroslider', '1'),
(947, 105, '_repeater_heroslider', 'field_5b96d6257871e'),
(948, 105, 'section_creator', '1'),
(949, 105, '_section_creator', 'field_5c3fb135f22f5'),
(950, 105, 'section_creator_0_background_field', '#42668f'),
(951, 105, '_section_creator_0_background_field', 'field_5c3fb19ff22f7'),
(952, 105, 'section_creator_0_default_editor', '[row]\r\n\r\n[column columns=6]\r\n\r\n<strong>Hours from</strong>\r\nSunday 11:00 am – 9:00 pm\r\nMonday 11:00 am – 8:00 pm\r\nTuesday 11:00 am – 8:00 pm\r\nWednesday 11:00 am – 8:00 pm\r\nThursday 11:00 am – 8:00 pm\r\nFriday 11:00 am – 9:00 pm\r\nSaturday 11:00 am – 9:00 pm\r\n\r\n[/column]\r\n\r\n[column columns=6]\r\n\r\n<img src=\"http://coming-soon.test/app/uploads/2019/05/restaurant.jpg\" alt=\"\" width=\"1000\" height=\"668\" class=\"alignnone size-full wp-image-69\" />\r\n\r\n[/column]\r\n\r\n[/row]'),
(953, 105, '_section_creator_0_default_editor', 'field_5c3fb1c9f22f8'),
(954, 105, 'section_f_creator', '2'),
(955, 105, '_section_f_creator', 'field_5cba18ec5a507'),
(956, 105, 'repeater_heroslider_0_sub_field_image', '86'),
(957, 105, '_repeater_heroslider_0_sub_field_image', 'field_5b96d6817871f'),
(958, 105, 'repeater_heroslider_0_sub_field_blurb', ''),
(959, 105, '_repeater_heroslider_0_sub_field_blurb', 'field_5b96d69378720'),
(960, 105, 'section_f_creator_0_order_num', 'order-one'),
(961, 105, '_section_f_creator_0_order_num', 'field_5cba1834ed9e6'),
(962, 105, 'section_f_creator_0_background_field', '69'),
(963, 105, '_section_f_creator_0_background_field', 'field_5cba180ced9e5'),
(964, 105, 'section_f_creator_0_order_num_two', 'order-two'),
(965, 105, '_section_f_creator_0_order_num_two', 'field_5cba1a10586b3'),
(966, 105, 'section_f_creator_0_default_f_editor', '<h1>Hours Open</h1>\r\n<strong>Sunday</strong> 11:00 am – 9:00 pm\r\n<strong>Monday</strong> 11:00 am – 8:00 pm\r\n<strong>Tuesday</strong> 11:00 am – 8:00 pm\r\n<strong>Wednesday</strong> 11:00 am – 8:00 pm\r\n<strong>Thursday</strong> 11:00 am – 8:00 pm\r\n<strong>Friday</strong> 11:00 am – 9:00 pm\r\n<strong>Saturday</strong> 11:00 am – 9:00 pm'),
(967, 105, '_section_f_creator_0_default_f_editor', 'field_5cba1a24586b4'),
(968, 105, 'section_f_creator_0_bg_clr_f', '#42668f'),
(969, 105, '_section_f_creator_0_bg_clr_f', 'field_5cba1accd7240'),
(970, 105, 'section_f_creator_0_dark_back_color', 'dark-back'),
(971, 105, '_section_f_creator_0_dark_back_color', 'field_5cba19bdcf299'),
(972, 105, 'section_f_creator_1_order_num', 'order-two'),
(973, 105, '_section_f_creator_1_order_num', 'field_5cba1834ed9e6'),
(974, 105, 'section_f_creator_1_background_field', '86'),
(975, 105, '_section_f_creator_1_background_field', 'field_5cba180ced9e5'),
(976, 105, 'section_f_creator_1_order_num_two', 'order-one'),
(977, 105, '_section_f_creator_1_order_num_two', 'field_5cba1a10586b3'),
(978, 105, 'section_f_creator_1_default_f_editor', '<h1>Here’s What You’ll Find On Our Three Floors:</h1>\r\n<strong>FIRST FLOOR:</strong>\r\nThis is a great hang-out area with outdoor picnic tables for those times when you want a bit more seclusion from the hustle and bustle.\r\n\r\n<strong>SECOND FLOOR:</strong>\r\nThe Blue Water Bar &amp; Grill provides spacious indoor seating plus a bar which extends all-around for indoor and outdoor seating. Enjoy exceptional local seafood, steak, chicken, hamburgers, salads and pizzas. Eat on-site, or order for take-out.\r\n\r\n<strong>THIRD FLOOR:</strong>\r\nOur 1,500 square foot banquet area offers large windowed walls, balconies and breathtaking 360-views. This spacious facility includes a long granite bar that circles the room with a utility sink and other bar amenities.'),
(979, 105, '_section_f_creator_1_default_f_editor', 'field_5cba1a24586b4'),
(980, 105, 'section_f_creator_1_bg_clr_f', ''),
(981, 105, '_section_f_creator_1_bg_clr_f', 'field_5cba1accd7240'),
(982, 105, 'section_f_creator_1_dark_back_color', ''),
(983, 105, '_section_f_creator_1_dark_back_color', 'field_5cba19bdcf299'),
(984, 105, 'section_creator_0_background_field_contained', ''),
(985, 105, '_section_creator_0_background_field_contained', 'field_5c3fb19ff22f7'),
(986, 105, 'section_creator_0_background_color', ''),
(987, 105, '_section_creator_0_background_color', 'field_5cbdfb3a31d8e'),
(988, 105, 'section_creator_0_dark_background', ''),
(989, 105, '_section_creator_0_dark_background', 'field_5cba4691d9c3d'),
(990, 105, 'section_creator_0_contained_editor', '<h2 style=\"text-align: center;\"></h2>\r\n<p style=\"text-align: center;\">[gravityform id=\"1\" title=\"false\" description=\"false\"]</p>'),
(991, 105, '_section_creator_0_contained_editor', 'field_5c3fb1c9f22f8'),
(992, 106, 'repeater_heroslider', '1'),
(993, 106, '_repeater_heroslider', 'field_5b96d6257871e'),
(994, 106, 'section_creator', '1'),
(995, 106, '_section_creator', 'field_5c3fb135f22f5'),
(996, 106, 'section_creator_0_background_field', '#42668f'),
(997, 106, '_section_creator_0_background_field', 'field_5c3fb19ff22f7'),
(998, 106, 'section_creator_0_default_editor', '[row]\r\n\r\n[column columns=6]\r\n\r\n<strong>Hours from</strong>\r\nSunday 11:00 am – 9:00 pm\r\nMonday 11:00 am – 8:00 pm\r\nTuesday 11:00 am – 8:00 pm\r\nWednesday 11:00 am – 8:00 pm\r\nThursday 11:00 am – 8:00 pm\r\nFriday 11:00 am – 9:00 pm\r\nSaturday 11:00 am – 9:00 pm\r\n\r\n[/column]\r\n\r\n[column columns=6]\r\n\r\n<img src=\"http://coming-soon.test/app/uploads/2019/05/restaurant.jpg\" alt=\"\" width=\"1000\" height=\"668\" class=\"alignnone size-full wp-image-69\" />\r\n\r\n[/column]\r\n\r\n[/row]'),
(999, 106, '_section_creator_0_default_editor', 'field_5c3fb1c9f22f8'),
(1000, 106, 'section_f_creator', '2'),
(1001, 106, '_section_f_creator', 'field_5cba18ec5a507'),
(1002, 106, 'repeater_heroslider_0_sub_field_image', '86'),
(1003, 106, '_repeater_heroslider_0_sub_field_image', 'field_5b96d6817871f'),
(1004, 106, 'repeater_heroslider_0_sub_field_blurb', ''),
(1005, 106, '_repeater_heroslider_0_sub_field_blurb', 'field_5b96d69378720'),
(1006, 106, 'section_f_creator_0_order_num', 'order-one'),
(1007, 106, '_section_f_creator_0_order_num', 'field_5cba1834ed9e6'),
(1008, 106, 'section_f_creator_0_background_field', '69'),
(1009, 106, '_section_f_creator_0_background_field', 'field_5cba180ced9e5'),
(1010, 106, 'section_f_creator_0_order_num_two', 'order-two'),
(1011, 106, '_section_f_creator_0_order_num_two', 'field_5cba1a10586b3'),
(1012, 106, 'section_f_creator_0_default_f_editor', '<h1>Hours Open</h1>\r\n<strong>Sunday</strong> 11:00 am – 9:00 pm\r\n<strong>Monday</strong> 11:00 am – 8:00 pm\r\n<strong>Tuesday</strong> 11:00 am – 8:00 pm\r\n<strong>Wednesday</strong> 11:00 am – 8:00 pm\r\n<strong>Thursday</strong> 11:00 am – 8:00 pm\r\n<strong>Friday</strong> 11:00 am – 9:00 pm\r\n<strong>Saturday</strong> 11:00 am – 9:00 pm'),
(1013, 106, '_section_f_creator_0_default_f_editor', 'field_5cba1a24586b4'),
(1014, 106, 'section_f_creator_0_bg_clr_f', '#42668f'),
(1015, 106, '_section_f_creator_0_bg_clr_f', 'field_5cba1accd7240'),
(1016, 106, 'section_f_creator_0_dark_back_color', 'dark-back'),
(1017, 106, '_section_f_creator_0_dark_back_color', 'field_5cba19bdcf299'),
(1018, 106, 'section_f_creator_1_order_num', 'order-two'),
(1019, 106, '_section_f_creator_1_order_num', 'field_5cba1834ed9e6'),
(1020, 106, 'section_f_creator_1_background_field', '86'),
(1021, 106, '_section_f_creator_1_background_field', 'field_5cba180ced9e5'),
(1022, 106, 'section_f_creator_1_order_num_two', 'order-one'),
(1023, 106, '_section_f_creator_1_order_num_two', 'field_5cba1a10586b3'),
(1024, 106, 'section_f_creator_1_default_f_editor', '<h1>Here’s What You’ll Find On Our Three Floors:</h1>\r\n<strong>FIRST FLOOR:</strong>\r\nThis is a great hang-out area with outdoor picnic tables for those times when you want a bit more seclusion from the hustle and bustle.\r\n\r\n<strong>SECOND FLOOR:</strong>\r\nThe Blue Water Bar &amp; Grill provides spacious indoor seating plus a bar which extends all-around for indoor and outdoor seating. Enjoy exceptional local seafood, steak, chicken, hamburgers, salads and pizzas. Eat on-site, or order for take-out.\r\n\r\n<strong>THIRD FLOOR:</strong>\r\nOur 1,500 square foot banquet area offers large windowed walls, balconies and breathtaking 360-views. This spacious facility includes a long granite bar that circles the room with a utility sink and other bar amenities.'),
(1025, 106, '_section_f_creator_1_default_f_editor', 'field_5cba1a24586b4'),
(1026, 106, 'section_f_creator_1_bg_clr_f', ''),
(1027, 106, '_section_f_creator_1_bg_clr_f', 'field_5cba1accd7240'),
(1028, 106, 'section_f_creator_1_dark_back_color', ''),
(1029, 106, '_section_f_creator_1_dark_back_color', 'field_5cba19bdcf299'),
(1030, 106, 'section_creator_0_background_field_contained', ''),
(1031, 106, '_section_creator_0_background_field_contained', 'field_5c3fb19ff22f7'),
(1032, 106, 'section_creator_0_background_color', ''),
(1033, 106, '_section_creator_0_background_color', 'field_5cbdfb3a31d8e'),
(1034, 106, 'section_creator_0_dark_background', ''),
(1035, 106, '_section_creator_0_dark_background', 'field_5cba4691d9c3d'),
(1036, 106, 'section_creator_0_contained_editor', '<hr />\r\n<p style=\"text-align: center;\">[gravityform id=\"1\" title=\"false\" description=\"false\"]</p>'),
(1037, 106, '_section_creator_0_contained_editor', 'field_5c3fb1c9f22f8'),
(1038, 107, 'repeater_heroslider', '1'),
(1039, 107, '_repeater_heroslider', 'field_5b96d6257871e'),
(1040, 107, 'section_creator', '1'),
(1041, 107, '_section_creator', 'field_5c3fb135f22f5'),
(1042, 107, 'section_creator_0_background_field', '#42668f'),
(1043, 107, '_section_creator_0_background_field', 'field_5c3fb19ff22f7'),
(1044, 107, 'section_creator_0_default_editor', '[row]\r\n\r\n[column columns=6]\r\n\r\n<strong>Hours from</strong>\r\nSunday 11:00 am – 9:00 pm\r\nMonday 11:00 am – 8:00 pm\r\nTuesday 11:00 am – 8:00 pm\r\nWednesday 11:00 am – 8:00 pm\r\nThursday 11:00 am – 8:00 pm\r\nFriday 11:00 am – 9:00 pm\r\nSaturday 11:00 am – 9:00 pm\r\n\r\n[/column]\r\n\r\n[column columns=6]\r\n\r\n<img src=\"http://coming-soon.test/app/uploads/2019/05/restaurant.jpg\" alt=\"\" width=\"1000\" height=\"668\" class=\"alignnone size-full wp-image-69\" />\r\n\r\n[/column]\r\n\r\n[/row]'),
(1045, 107, '_section_creator_0_default_editor', 'field_5c3fb1c9f22f8'),
(1046, 107, 'section_f_creator', '2'),
(1047, 107, '_section_f_creator', 'field_5cba18ec5a507'),
(1048, 107, 'repeater_heroslider_0_sub_field_image', '86'),
(1049, 107, '_repeater_heroslider_0_sub_field_image', 'field_5b96d6817871f'),
(1050, 107, 'repeater_heroslider_0_sub_field_blurb', ''),
(1051, 107, '_repeater_heroslider_0_sub_field_blurb', 'field_5b96d69378720'),
(1052, 107, 'section_f_creator_0_order_num', 'order-one'),
(1053, 107, '_section_f_creator_0_order_num', 'field_5cba1834ed9e6'),
(1054, 107, 'section_f_creator_0_background_field', '69'),
(1055, 107, '_section_f_creator_0_background_field', 'field_5cba180ced9e5'),
(1056, 107, 'section_f_creator_0_order_num_two', 'order-two'),
(1057, 107, '_section_f_creator_0_order_num_two', 'field_5cba1a10586b3'),
(1058, 107, 'section_f_creator_0_default_f_editor', '<h1>Hours Open</h1>\r\n<strong>Sunday</strong> 11:00 am – 9:00 pm\r\n<strong>Monday</strong> 11:00 am – 8:00 pm\r\n<strong>Tuesday</strong> 11:00 am – 8:00 pm\r\n<strong>Wednesday</strong> 11:00 am – 8:00 pm\r\n<strong>Thursday</strong> 11:00 am – 8:00 pm\r\n<strong>Friday</strong> 11:00 am – 9:00 pm\r\n<strong>Saturday</strong> 11:00 am – 9:00 pm'),
(1059, 107, '_section_f_creator_0_default_f_editor', 'field_5cba1a24586b4'),
(1060, 107, 'section_f_creator_0_bg_clr_f', '#42668f'),
(1061, 107, '_section_f_creator_0_bg_clr_f', 'field_5cba1accd7240'),
(1062, 107, 'section_f_creator_0_dark_back_color', 'dark-back'),
(1063, 107, '_section_f_creator_0_dark_back_color', 'field_5cba19bdcf299'),
(1064, 107, 'section_f_creator_1_order_num', 'order-two'),
(1065, 107, '_section_f_creator_1_order_num', 'field_5cba1834ed9e6'),
(1066, 107, 'section_f_creator_1_background_field', '86'),
(1067, 107, '_section_f_creator_1_background_field', 'field_5cba180ced9e5'),
(1068, 107, 'section_f_creator_1_order_num_two', 'order-one'),
(1069, 107, '_section_f_creator_1_order_num_two', 'field_5cba1a10586b3'),
(1070, 107, 'section_f_creator_1_default_f_editor', '<h1>Here’s What You’ll Find On Our Three Floors:</h1>\r\n<strong>FIRST FLOOR:</strong>\r\nThis is a great hang-out area with outdoor picnic tables for those times when you want a bit more seclusion from the hustle and bustle.\r\n\r\n<strong>SECOND FLOOR:</strong>\r\nThe Blue Water Bar &amp; Grill provides spacious indoor seating plus a bar which extends all-around for indoor and outdoor seating. Enjoy exceptional local seafood, steak, chicken, hamburgers, salads and pizzas. Eat on-site, or order for take-out.\r\n\r\n<strong>THIRD FLOOR:</strong>\r\nOur 1,500 square foot banquet area offers large windowed walls, balconies and breathtaking 360-views. This spacious facility includes a long granite bar that circles the room with a utility sink and other bar amenities.'),
(1071, 107, '_section_f_creator_1_default_f_editor', 'field_5cba1a24586b4'),
(1072, 107, 'section_f_creator_1_bg_clr_f', ''),
(1073, 107, '_section_f_creator_1_bg_clr_f', 'field_5cba1accd7240'),
(1074, 107, 'section_f_creator_1_dark_back_color', ''),
(1075, 107, '_section_f_creator_1_dark_back_color', 'field_5cba19bdcf299'),
(1076, 107, 'section_creator_0_background_field_contained', ''),
(1077, 107, '_section_creator_0_background_field_contained', 'field_5c3fb19ff22f7'),
(1078, 107, 'section_creator_0_background_color', ''),
(1079, 107, '_section_creator_0_background_color', 'field_5cbdfb3a31d8e'),
(1080, 107, 'section_creator_0_dark_background', ''),
(1081, 107, '_section_creator_0_dark_background', 'field_5cba4691d9c3d'),
(1082, 107, 'section_creator_0_contained_editor', '<hr />\r\n\r\n<h2 style=\"text-align: center;\">Contact Form</h2>\r\n<p style=\"text-align: center;\">[gravityform id=\"1\" title=\"false\" description=\"false\"]</p>'),
(1083, 107, '_section_creator_0_contained_editor', 'field_5c3fb1c9f22f8'),
(1084, 108, 'repeater_heroslider', '1'),
(1085, 108, '_repeater_heroslider', 'field_5b96d6257871e'),
(1086, 108, 'section_creator', ''),
(1087, 108, '_section_creator', 'field_5c3fb135f22f5'),
(1088, 108, 'section_creator_0_background_field', '#42668f'),
(1089, 108, '_section_creator_0_background_field', 'field_5c3fb19ff22f7'),
(1090, 108, 'section_creator_0_default_editor', '[row]\r\n\r\n[column columns=6]\r\n\r\n<strong>Hours from</strong>\r\nSunday 11:00 am – 9:00 pm\r\nMonday 11:00 am – 8:00 pm\r\nTuesday 11:00 am – 8:00 pm\r\nWednesday 11:00 am – 8:00 pm\r\nThursday 11:00 am – 8:00 pm\r\nFriday 11:00 am – 9:00 pm\r\nSaturday 11:00 am – 9:00 pm\r\n\r\n[/column]\r\n\r\n[column columns=6]\r\n\r\n<img src=\"http://coming-soon.test/app/uploads/2019/05/restaurant.jpg\" alt=\"\" width=\"1000\" height=\"668\" class=\"alignnone size-full wp-image-69\" />\r\n\r\n[/column]\r\n\r\n[/row]'),
(1091, 108, '_section_creator_0_default_editor', 'field_5c3fb1c9f22f8'),
(1092, 108, 'section_f_creator', '2'),
(1093, 108, '_section_f_creator', 'field_5cba18ec5a507'),
(1094, 108, 'repeater_heroslider_0_sub_field_image', '86'),
(1095, 108, '_repeater_heroslider_0_sub_field_image', 'field_5b96d6817871f'),
(1096, 108, 'repeater_heroslider_0_sub_field_blurb', ''),
(1097, 108, '_repeater_heroslider_0_sub_field_blurb', 'field_5b96d69378720'),
(1098, 108, 'section_f_creator_0_order_num', 'order-one'),
(1099, 108, '_section_f_creator_0_order_num', 'field_5cba1834ed9e6'),
(1100, 108, 'section_f_creator_0_background_field', '69'),
(1101, 108, '_section_f_creator_0_background_field', 'field_5cba180ced9e5'),
(1102, 108, 'section_f_creator_0_order_num_two', 'order-two'),
(1103, 108, '_section_f_creator_0_order_num_two', 'field_5cba1a10586b3'),
(1104, 108, 'section_f_creator_0_default_f_editor', '<h1>Hours Open</h1>\r\n<strong>Sunday</strong> 11:00 am – 9:00 pm\r\n<strong>Monday</strong> 11:00 am – 8:00 pm\r\n<strong>Tuesday</strong> 11:00 am – 8:00 pm\r\n<strong>Wednesday</strong> 11:00 am – 8:00 pm\r\n<strong>Thursday</strong> 11:00 am – 8:00 pm\r\n<strong>Friday</strong> 11:00 am – 9:00 pm\r\n<strong>Saturday</strong> 11:00 am – 9:00 pm'),
(1105, 108, '_section_f_creator_0_default_f_editor', 'field_5cba1a24586b4'),
(1106, 108, 'section_f_creator_0_bg_clr_f', '#42668f'),
(1107, 108, '_section_f_creator_0_bg_clr_f', 'field_5cba1accd7240'),
(1108, 108, 'section_f_creator_0_dark_back_color', 'dark-back'),
(1109, 108, '_section_f_creator_0_dark_back_color', 'field_5cba19bdcf299'),
(1110, 108, 'section_f_creator_1_order_num', 'order-two'),
(1111, 108, '_section_f_creator_1_order_num', 'field_5cba1834ed9e6'),
(1112, 108, 'section_f_creator_1_background_field', '86'),
(1113, 108, '_section_f_creator_1_background_field', 'field_5cba180ced9e5'),
(1114, 108, 'section_f_creator_1_order_num_two', 'order-one'),
(1115, 108, '_section_f_creator_1_order_num_two', 'field_5cba1a10586b3'),
(1116, 108, 'section_f_creator_1_default_f_editor', '<h1>Here’s What You’ll Find On Our Three Floors:</h1>\r\n<strong>FIRST FLOOR:</strong>\r\nThis is a great hang-out area with outdoor picnic tables for those times when you want a bit more seclusion from the hustle and bustle.\r\n\r\n<strong>SECOND FLOOR:</strong>\r\nThe Blue Water Bar &amp; Grill provides spacious indoor seating plus a bar which extends all-around for indoor and outdoor seating. Enjoy exceptional local seafood, steak, chicken, hamburgers, salads and pizzas. Eat on-site, or order for take-out.\r\n\r\n<strong>THIRD FLOOR:</strong>\r\nOur 1,500 square foot banquet area offers large windowed walls, balconies and breathtaking 360-views. This spacious facility includes a long granite bar that circles the room with a utility sink and other bar amenities.'),
(1117, 108, '_section_f_creator_1_default_f_editor', 'field_5cba1a24586b4'),
(1118, 108, 'section_f_creator_1_bg_clr_f', ''),
(1119, 108, '_section_f_creator_1_bg_clr_f', 'field_5cba1accd7240'),
(1120, 108, 'section_f_creator_1_dark_back_color', ''),
(1121, 108, '_section_f_creator_1_dark_back_color', 'field_5cba19bdcf299'),
(1122, 109, 'repeater_heroslider', '1'),
(1123, 109, '_repeater_heroslider', 'field_5b96d6257871e'),
(1124, 109, 'section_creator', ''),
(1125, 109, '_section_creator', 'field_5c3fb135f22f5'),
(1126, 109, 'section_creator_0_background_field', '#42668f'),
(1127, 109, '_section_creator_0_background_field', 'field_5c3fb19ff22f7'),
(1128, 109, 'section_creator_0_default_editor', '[row]\r\n\r\n[column columns=6]\r\n\r\n<strong>Hours from</strong>\r\nSunday 11:00 am – 9:00 pm\r\nMonday 11:00 am – 8:00 pm\r\nTuesday 11:00 am – 8:00 pm\r\nWednesday 11:00 am – 8:00 pm\r\nThursday 11:00 am – 8:00 pm\r\nFriday 11:00 am – 9:00 pm\r\nSaturday 11:00 am – 9:00 pm\r\n\r\n[/column]\r\n\r\n[column columns=6]\r\n\r\n<img src=\"http://coming-soon.test/app/uploads/2019/05/restaurant.jpg\" alt=\"\" width=\"1000\" height=\"668\" class=\"alignnone size-full wp-image-69\" />\r\n\r\n[/column]\r\n\r\n[/row]'),
(1129, 109, '_section_creator_0_default_editor', 'field_5c3fb1c9f22f8'),
(1130, 109, 'section_f_creator', '2'),
(1131, 109, '_section_f_creator', 'field_5cba18ec5a507'),
(1132, 109, 'repeater_heroslider_0_sub_field_image', '86'),
(1133, 109, '_repeater_heroslider_0_sub_field_image', 'field_5b96d6817871f'),
(1134, 109, 'repeater_heroslider_0_sub_field_blurb', ''),
(1135, 109, '_repeater_heroslider_0_sub_field_blurb', 'field_5b96d69378720'),
(1136, 109, 'section_f_creator_0_order_num', 'order-one'),
(1137, 109, '_section_f_creator_0_order_num', 'field_5cba1834ed9e6'),
(1138, 109, 'section_f_creator_0_background_field', '69'),
(1139, 109, '_section_f_creator_0_background_field', 'field_5cba180ced9e5'),
(1140, 109, 'section_f_creator_0_order_num_two', 'order-two'),
(1141, 109, '_section_f_creator_0_order_num_two', 'field_5cba1a10586b3'),
(1142, 109, 'section_f_creator_0_default_f_editor', '<h1>Hours Open</h1>\r\n<strong>Sunday</strong> 11:00 am – 9:00 pm\r\n<strong>Monday</strong> 11:00 am – 8:00 pm\r\n<strong>Tuesday</strong> 11:00 am – 8:00 pm\r\n<strong>Wednesday</strong> 11:00 am – 8:00 pm\r\n<strong>Thursday</strong> 11:00 am – 8:00 pm\r\n<strong>Friday</strong> 11:00 am – 9:00 pm\r\n<strong>Saturday</strong> 11:00 am – 9:00 pm\r\n\r\n<a class=\"btn btn-o\" href=\"tel:9792394305\">Call Now</a>'),
(1143, 109, '_section_f_creator_0_default_f_editor', 'field_5cba1a24586b4'),
(1144, 109, 'section_f_creator_0_bg_clr_f', '#42668f'),
(1145, 109, '_section_f_creator_0_bg_clr_f', 'field_5cba1accd7240'),
(1146, 109, 'section_f_creator_0_dark_back_color', 'dark-back'),
(1147, 109, '_section_f_creator_0_dark_back_color', 'field_5cba19bdcf299'),
(1148, 109, 'section_f_creator_1_order_num', 'order-two'),
(1149, 109, '_section_f_creator_1_order_num', 'field_5cba1834ed9e6'),
(1150, 109, 'section_f_creator_1_background_field', '86'),
(1151, 109, '_section_f_creator_1_background_field', 'field_5cba180ced9e5'),
(1152, 109, 'section_f_creator_1_order_num_two', 'order-one'),
(1153, 109, '_section_f_creator_1_order_num_two', 'field_5cba1a10586b3'),
(1154, 109, 'section_f_creator_1_default_f_editor', '<h1>Here’s What You’ll Find On Our Three Floors:</h1>\r\n<strong>FIRST FLOOR:</strong>\r\nThis is a great hang-out area with outdoor picnic tables for those times when you want a bit more seclusion from the hustle and bustle.\r\n\r\n<strong>SECOND FLOOR:</strong>\r\nThe Blue Water Bar &amp; Grill provides spacious indoor seating plus a bar which extends all-around for indoor and outdoor seating. Enjoy exceptional local seafood, steak, chicken, hamburgers, salads and pizzas. Eat on-site, or order for take-out.\r\n\r\n<strong>THIRD FLOOR:</strong>\r\nOur 1,500 square foot banquet area offers large windowed walls, balconies and breathtaking 360-views. This spacious facility includes a long granite bar that circles the room with a utility sink and other bar amenities.\r\n\r\n<a class=\"btn btn-o\" href=\"tel:9792394305\">Call Now</a>'),
(1155, 109, '_section_f_creator_1_default_f_editor', 'field_5cba1a24586b4'),
(1156, 109, 'section_f_creator_1_bg_clr_f', ''),
(1157, 109, '_section_f_creator_1_bg_clr_f', 'field_5cba1accd7240'),
(1158, 109, 'section_f_creator_1_dark_back_color', ''),
(1159, 109, '_section_f_creator_1_dark_back_color', 'field_5cba19bdcf299'),
(1160, 110, 'repeater_heroslider', '1'),
(1161, 110, '_repeater_heroslider', 'field_5b96d6257871e'),
(1162, 110, 'section_creator', ''),
(1163, 110, '_section_creator', 'field_5c3fb135f22f5'),
(1164, 110, 'section_creator_0_background_field', '#42668f'),
(1165, 110, '_section_creator_0_background_field', 'field_5c3fb19ff22f7'),
(1166, 110, 'section_creator_0_default_editor', '[row]\r\n\r\n[column columns=6]\r\n\r\n<strong>Hours from</strong>\r\nSunday 11:00 am – 9:00 pm\r\nMonday 11:00 am – 8:00 pm\r\nTuesday 11:00 am – 8:00 pm\r\nWednesday 11:00 am – 8:00 pm\r\nThursday 11:00 am – 8:00 pm\r\nFriday 11:00 am – 9:00 pm\r\nSaturday 11:00 am – 9:00 pm\r\n\r\n[/column]\r\n\r\n[column columns=6]\r\n\r\n<img src=\"http://coming-soon.test/app/uploads/2019/05/restaurant.jpg\" alt=\"\" width=\"1000\" height=\"668\" class=\"alignnone size-full wp-image-69\" />\r\n\r\n[/column]\r\n\r\n[/row]'),
(1167, 110, '_section_creator_0_default_editor', 'field_5c3fb1c9f22f8'),
(1168, 110, 'section_f_creator', '2'),
(1169, 110, '_section_f_creator', 'field_5cba18ec5a507'),
(1170, 110, 'repeater_heroslider_0_sub_field_image', '86'),
(1171, 110, '_repeater_heroslider_0_sub_field_image', 'field_5b96d6817871f'),
(1172, 110, 'repeater_heroslider_0_sub_field_blurb', ''),
(1173, 110, '_repeater_heroslider_0_sub_field_blurb', 'field_5b96d69378720'),
(1174, 110, 'section_f_creator_0_order_num', 'order-one'),
(1175, 110, '_section_f_creator_0_order_num', 'field_5cba1834ed9e6'),
(1176, 110, 'section_f_creator_0_background_field', '69'),
(1177, 110, '_section_f_creator_0_background_field', 'field_5cba180ced9e5'),
(1178, 110, 'section_f_creator_0_order_num_two', 'order-two'),
(1179, 110, '_section_f_creator_0_order_num_two', 'field_5cba1a10586b3'),
(1180, 110, 'section_f_creator_0_default_f_editor', '<h1>Hours Open</h1>\r\n<strong>Sunday</strong> 11:00 am – 9:00 pm\r\n<strong>Monday</strong> 11:00 am – 8:00 pm\r\n<strong>Tuesday</strong> 11:00 am – 8:00 pm\r\n<strong>Wednesday</strong> 11:00 am – 8:00 pm\r\n<strong>Thursday</strong> 11:00 am – 8:00 pm\r\n<strong>Friday</strong> 11:00 am – 9:00 pm\r\n<strong>Saturday</strong> 11:00 am – 9:00 pm\r\n\r\n<a class=\"btn btn-b\" href=\"tel:9792394305\">Call Now</a>'),
(1181, 110, '_section_f_creator_0_default_f_editor', 'field_5cba1a24586b4'),
(1182, 110, 'section_f_creator_0_bg_clr_f', '#42668f'),
(1183, 110, '_section_f_creator_0_bg_clr_f', 'field_5cba1accd7240'),
(1184, 110, 'section_f_creator_0_dark_back_color', 'dark-back'),
(1185, 110, '_section_f_creator_0_dark_back_color', 'field_5cba19bdcf299'),
(1186, 110, 'section_f_creator_1_order_num', 'order-two'),
(1187, 110, '_section_f_creator_1_order_num', 'field_5cba1834ed9e6'),
(1188, 110, 'section_f_creator_1_background_field', '86'),
(1189, 110, '_section_f_creator_1_background_field', 'field_5cba180ced9e5'),
(1190, 110, 'section_f_creator_1_order_num_two', 'order-one'),
(1191, 110, '_section_f_creator_1_order_num_two', 'field_5cba1a10586b3'),
(1192, 110, 'section_f_creator_1_default_f_editor', '<h1>Here’s What You’ll Find On Our Three Floors:</h1>\r\n<strong>FIRST FLOOR:</strong>\r\nThis is a great hang-out area with outdoor picnic tables for those times when you want a bit more seclusion from the hustle and bustle.\r\n\r\n<strong>SECOND FLOOR:</strong>\r\nThe Blue Water Bar &amp; Grill provides spacious indoor seating plus a bar which extends all-around for indoor and outdoor seating. Enjoy exceptional local seafood, steak, chicken, hamburgers, salads and pizzas. Eat on-site, or order for take-out.\r\n\r\n<strong>THIRD FLOOR:</strong>\r\nOur 1,500 square foot banquet area offers large windowed walls, balconies and breathtaking 360-views. This spacious facility includes a long granite bar that circles the room with a utility sink and other bar amenities.\r\n\r\n<a class=\"btn btn-b\" href=\"tel:9792394305\">Call Now</a>'),
(1193, 110, '_section_f_creator_1_default_f_editor', 'field_5cba1a24586b4'),
(1194, 110, 'section_f_creator_1_bg_clr_f', ''),
(1195, 110, '_section_f_creator_1_bg_clr_f', 'field_5cba1accd7240'),
(1196, 110, 'section_f_creator_1_dark_back_color', ''),
(1197, 110, '_section_f_creator_1_dark_back_color', 'field_5cba19bdcf299'),
(1198, 111, 'repeater_heroslider', '1'),
(1199, 111, '_repeater_heroslider', 'field_5b96d6257871e'),
(1200, 111, 'section_creator', ''),
(1201, 111, '_section_creator', 'field_5c3fb135f22f5'),
(1202, 111, 'section_creator_0_background_field', '#42668f'),
(1203, 111, '_section_creator_0_background_field', 'field_5c3fb19ff22f7'),
(1204, 111, 'section_creator_0_default_editor', '[row]\r\n\r\n[column columns=6]\r\n\r\n<strong>Hours from</strong>\r\nSunday 11:00 am – 9:00 pm\r\nMonday 11:00 am – 8:00 pm\r\nTuesday 11:00 am – 8:00 pm\r\nWednesday 11:00 am – 8:00 pm\r\nThursday 11:00 am – 8:00 pm\r\nFriday 11:00 am – 9:00 pm\r\nSaturday 11:00 am – 9:00 pm\r\n\r\n[/column]\r\n\r\n[column columns=6]\r\n\r\n<img src=\"http://coming-soon.test/app/uploads/2019/05/restaurant.jpg\" alt=\"\" width=\"1000\" height=\"668\" class=\"alignnone size-full wp-image-69\" />\r\n\r\n[/column]\r\n\r\n[/row]'),
(1205, 111, '_section_creator_0_default_editor', 'field_5c3fb1c9f22f8'),
(1206, 111, 'section_f_creator', '2'),
(1207, 111, '_section_f_creator', 'field_5cba18ec5a507'),
(1208, 111, 'repeater_heroslider_0_sub_field_image', '86'),
(1209, 111, '_repeater_heroslider_0_sub_field_image', 'field_5b96d6817871f'),
(1210, 111, 'repeater_heroslider_0_sub_field_blurb', ''),
(1211, 111, '_repeater_heroslider_0_sub_field_blurb', 'field_5b96d69378720'),
(1212, 111, 'section_f_creator_0_order_num', 'order-one'),
(1213, 111, '_section_f_creator_0_order_num', 'field_5cba1834ed9e6'),
(1214, 111, 'section_f_creator_0_background_field', '69'),
(1215, 111, '_section_f_creator_0_background_field', 'field_5cba180ced9e5'),
(1216, 111, 'section_f_creator_0_order_num_two', 'order-two'),
(1217, 111, '_section_f_creator_0_order_num_two', 'field_5cba1a10586b3'),
(1218, 111, 'section_f_creator_0_default_f_editor', '<h1>Hours Open</h1>\r\n<strong>Sunday</strong> 11:00 am – 9:00 pm\r\n<strong>Monday</strong> 11:00 am – 8:00 pm\r\n<strong>Tuesday</strong> 11:00 am – 8:00 pm\r\n<strong>Wednesday</strong> 11:00 am – 8:00 pm\r\n<strong>Thursday</strong> 11:00 am – 8:00 pm\r\n<strong>Friday</strong> 11:00 am – 9:00 pm\r\n<strong>Saturday</strong> 11:00 am – 9:00 pm\r\n\r\n<a class=\"btn btn-w\" href=\"tel:9792394305\">Call Now</a>'),
(1219, 111, '_section_f_creator_0_default_f_editor', 'field_5cba1a24586b4'),
(1220, 111, 'section_f_creator_0_bg_clr_f', '#42668f'),
(1221, 111, '_section_f_creator_0_bg_clr_f', 'field_5cba1accd7240'),
(1222, 111, 'section_f_creator_0_dark_back_color', 'dark-back'),
(1223, 111, '_section_f_creator_0_dark_back_color', 'field_5cba19bdcf299'),
(1224, 111, 'section_f_creator_1_order_num', 'order-two'),
(1225, 111, '_section_f_creator_1_order_num', 'field_5cba1834ed9e6'),
(1226, 111, 'section_f_creator_1_background_field', '86'),
(1227, 111, '_section_f_creator_1_background_field', 'field_5cba180ced9e5'),
(1228, 111, 'section_f_creator_1_order_num_two', 'order-one'),
(1229, 111, '_section_f_creator_1_order_num_two', 'field_5cba1a10586b3'),
(1230, 111, 'section_f_creator_1_default_f_editor', '<h1>Here’s What You’ll Find On Our Three Floors:</h1>\r\n<strong>FIRST FLOOR:</strong>\r\nThis is a great hang-out area with outdoor picnic tables for those times when you want a bit more seclusion from the hustle and bustle.\r\n\r\n<strong>SECOND FLOOR:</strong>\r\nThe Blue Water Bar &amp; Grill provides spacious indoor seating plus a bar which extends all-around for indoor and outdoor seating. Enjoy exceptional local seafood, steak, chicken, hamburgers, salads and pizzas. Eat on-site, or order for take-out.\r\n\r\n<strong>THIRD FLOOR:</strong>\r\nOur 1,500 square foot banquet area offers large windowed walls, balconies and breathtaking 360-views. This spacious facility includes a long granite bar that circles the room with a utility sink and other bar amenities.\r\n\r\n<a class=\"btn btn-b\" href=\"tel:9792394305\">Call Now</a>'),
(1231, 111, '_section_f_creator_1_default_f_editor', 'field_5cba1a24586b4'),
(1232, 111, 'section_f_creator_1_bg_clr_f', ''),
(1233, 111, '_section_f_creator_1_bg_clr_f', 'field_5cba1accd7240'),
(1234, 111, 'section_f_creator_1_dark_back_color', ''),
(1235, 111, '_section_f_creator_1_dark_back_color', 'field_5cba19bdcf299'),
(1236, 113, 'repeater_heroslider', ''),
(1237, 113, '_repeater_heroslider', 'field_5b96d6257871e'),
(1238, 113, 'section_creator', ''),
(1239, 113, '_section_creator', 'field_5c3fb135f22f5'),
(1240, 113, 'section_creator_0_background_field', '#42668f'),
(1241, 113, '_section_creator_0_background_field', 'field_5c3fb19ff22f7'),
(1242, 113, 'section_creator_0_default_editor', '[row]\r\n\r\n[column columns=6]\r\n\r\n<strong>Hours from</strong>\r\nSunday 11:00 am – 9:00 pm\r\nMonday 11:00 am – 8:00 pm\r\nTuesday 11:00 am – 8:00 pm\r\nWednesday 11:00 am – 8:00 pm\r\nThursday 11:00 am – 8:00 pm\r\nFriday 11:00 am – 9:00 pm\r\nSaturday 11:00 am – 9:00 pm\r\n\r\n[/column]\r\n\r\n[column columns=6]\r\n\r\n<img src=\"http://coming-soon.test/app/uploads/2019/05/restaurant.jpg\" alt=\"\" width=\"1000\" height=\"668\" class=\"alignnone size-full wp-image-69\" />\r\n\r\n[/column]\r\n\r\n[/row]'),
(1243, 113, '_section_creator_0_default_editor', 'field_5c3fb1c9f22f8'),
(1244, 113, 'section_f_creator', ''),
(1245, 113, '_section_f_creator', 'field_5cba18ec5a507');

-- --------------------------------------------------------

--
-- Table structure for table `cs_posts`
--

CREATE TABLE `cs_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cs_posts`
--

INSERT INTO `cs_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-04-03 21:24:50', '2019-04-03 21:24:50', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2019-04-03 21:24:50', '2019-04-03 21:24:50', '', 0, 'http://coming-soon.test/wp/?p=1', 0, 'post', '', 1),
(2, 1, '2019-04-03 21:24:50', '2019-04-03 21:24:50', '', 'Home', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2019-05-17 22:37:30', '2019-05-17 22:37:30', '', 0, 'http://coming-soon.test/wp/?page_id=2', 0, 'page', '', 0),
(6, 1, '2019-04-03 21:26:48', '2019-04-03 21:26:48', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:15:\"global-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Global', 'global', 'publish', 'closed', 'closed', '', 'group_5c86eabade229', '', '', '2019-05-07 22:32:46', '2019-05-07 22:32:46', '', 0, 'http://coming-soon.test/wp/?post_type=acf-field-group&#038;p=6', 0, 'acf-field-group', '', 0),
(7, 1, '2019-04-03 21:26:48', '2019-04-03 21:26:48', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Street name', 'address_street_name', 'publish', 'closed', 'closed', '', 'field_5c8bf58f1a56f', '', '', '2019-04-03 22:07:48', '2019-04-03 22:07:48', '', 6, 'http://coming-soon.test/wp/?post_type=acf-field&#038;p=7', 3, 'acf-field', '', 0),
(8, 1, '2019-04-03 21:26:48', '2019-04-03 21:26:48', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'City Name', 'address_city_name', 'publish', 'closed', 'closed', '', 'field_5c8bf5a51a570', '', '', '2019-04-03 22:07:48', '2019-04-03 22:07:48', '', 6, 'http://coming-soon.test/wp/?post_type=acf-field&#038;p=8', 4, 'acf-field', '', 0),
(9, 1, '2019-04-03 21:26:48', '2019-04-03 21:26:48', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'State', 'address_state', 'publish', 'closed', 'closed', '', 'field_5c8bf5b51a571', '', '', '2019-04-03 22:07:48', '2019-04-03 22:07:48', '', 6, 'http://coming-soon.test/wp/?post_type=acf-field&#038;p=9', 5, 'acf-field', '', 0),
(10, 1, '2019-04-03 21:26:48', '2019-04-03 21:26:48', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'ZipCode', 'address_zipcode', 'publish', 'closed', 'closed', '', 'field_5c8bf5be1a572', '', '', '2019-04-03 22:07:48', '2019-04-03 22:07:48', '', 6, 'http://coming-soon.test/wp/?post_type=acf-field&#038;p=10', 6, 'acf-field', '', 0),
(11, 1, '2019-04-03 21:26:48', '2019-04-03 21:26:48', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Phone Number', 'phone_number', 'publish', 'closed', 'closed', '', 'field_5c8be52ffb2d6', '', '', '2019-04-03 21:56:10', '2019-04-03 21:56:10', '', 6, 'http://coming-soon.test/wp/?post_type=acf-field&#038;p=11', 1, 'acf-field', '', 0),
(18, 1, '2019-04-03 21:26:48', '2019-04-03 21:26:48', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Facebook URL', 'facebook_url', 'publish', 'closed', 'closed', '', 'field_5c8bec12e62b7', '', '', '2019-05-07 22:30:06', '2019-05-07 22:30:06', '', 6, 'http://coming-soon.test/wp/?post_type=acf-field&#038;p=18', 9, 'acf-field', '', 0),
(19, 1, '2019-04-03 21:26:48', '2019-04-03 21:26:48', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Google Plus URL', 'google_url', 'publish', 'closed', 'closed', '', 'field_5c8bece4fd248', '', '', '2019-05-07 22:30:06', '2019-05-07 22:30:06', '', 6, 'http://coming-soon.test/wp/?post_type=acf-field&#038;p=19', 10, 'acf-field', '', 0),
(20, 1, '2019-04-03 21:26:48', '2019-04-03 21:26:48', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Yelp URL', 'yelp_url', 'publish', 'closed', 'closed', '', 'field_5c8bed63780c5', '', '', '2019-05-07 22:30:06', '2019-05-07 22:30:06', '', 6, 'http://coming-soon.test/wp/?post_type=acf-field&#038;p=20', 11, 'acf-field', '', 0),
(21, 1, '2019-04-03 21:26:48', '2019-04-03 21:26:48', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Instagram URL', 'instagram_url', 'publish', 'closed', 'closed', '', 'field_5c8bed54fd249', '', '', '2019-05-07 22:30:06', '2019-05-07 22:30:06', '', 6, 'http://coming-soon.test/wp/?post_type=acf-field&#038;p=21', 12, 'acf-field', '', 0),
(22, 1, '2019-04-03 21:26:48', '2019-04-03 21:26:48', 'a:8:{s:8:\"location\";a:2:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:7:\"default\";}}i:1;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:29:\"views/template-home.blade.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";s:5:\"local\";s:3:\"php\";}', 'Hero Slider', 'hero-slider', 'publish', 'closed', 'closed', '', 'group_5b96d61b240f4', '', '', '2019-04-03 21:26:48', '2019-04-03 21:26:48', '', 0, 'http://coming-soon.test/wp/?post_type=acf-field-group&p=22', 0, 'acf-field-group', '', 0),
(23, 1, '2019-04-03 21:26:48', '2019-04-03 21:26:48', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";i:0;s:3:\"max\";i:0;s:6:\"layout\";s:5:\"block\";s:12:\"button_label\";s:0:\"\";}', 'Hero Slider', 'repeater_heroslider', 'publish', 'closed', 'closed', '', 'field_5b96d6257871e', '', '', '2019-04-03 21:26:48', '2019-04-03 21:26:48', '', 22, 'http://coming-soon.test/wp/?post_type=acf-field&p=23', 0, 'acf-field', '', 0),
(24, 1, '2019-04-03 21:26:48', '2019-04-03 21:26:48', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:5:\"large\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Image', 'sub_field_image', 'publish', 'closed', 'closed', '', 'field_5b96d6817871f', '', '', '2019-04-03 21:26:48', '2019-04-03 21:26:48', '', 23, 'http://coming-soon.test/wp/?post_type=acf-field&p=24', 0, 'acf-field', '', 0),
(25, 1, '2019-04-03 21:26:48', '2019-04-03 21:26:48', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Hero Blurb', 'sub_field_blurb', 'publish', 'closed', 'closed', '', 'field_5b96d69378720', '', '', '2019-04-03 21:26:48', '2019-04-03 21:26:48', '', 23, 'http://coming-soon.test/wp/?post_type=acf-field&p=25', 1, 'acf-field', '', 0),
(30, 1, '2019-04-03 21:37:35', '2019-04-03 21:37:35', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Website Logo', 'logo_field', 'publish', 'closed', 'closed', '', 'field_5ca5278b43767', '', '', '2019-04-03 21:56:10', '2019-04-03 21:56:10', '', 6, 'http://coming-soon.test/?post_type=acf-field&#038;p=30', 0, 'acf-field', '', 0),
(31, 1, '2019-04-03 21:38:12', '2019-04-03 21:38:12', '', '300', '', 'inherit', 'open', 'closed', '', '300', '', '', '2019-04-03 21:46:37', '2019-04-03 21:46:37', '', 2, 'http://coming-soon.test/app/uploads/2019/04/300.png', 0, 'attachment', 'image/png', 0),
(32, 1, '2019-04-03 21:43:43', '2019-04-03 21:43:43', 'a:8:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"page\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";s:5:\"local\";s:3:\"php\";}', 'Creator Contained', 'creator-contained', 'publish', 'closed', 'closed', '', 'group_5b9966ac2bd0d', '', '', '2019-05-07 22:01:13', '2019-05-07 22:01:13', '', 0, 'http://coming-soon.test/?post_type=acf-field-group&#038;p=32', 0, 'acf-field-group', '', 0),
(33, 1, '2019-04-03 21:43:43', '2019-04-03 21:43:43', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";i:0;s:3:\"max\";i:0;s:6:\"layout\";s:5:\"block\";s:12:\"button_label\";s:0:\"\";}', 'Creator', 'section_creator', 'publish', 'closed', 'closed', '', 'field_5c3fb135f22f5', '', '', '2019-05-07 22:01:13', '2019-05-07 22:01:13', '', 32, 'http://coming-soon.test/?post_type=acf-field&#038;p=33', 0, 'acf-field', '', 0),
(34, 1, '2019-04-03 21:43:43', '2019-04-03 21:43:43', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Background Image', 'background_field_contained', 'publish', 'closed', 'closed', '', 'field_5c3fb19ff22f7', '', '', '2019-05-07 22:01:13', '2019-05-07 22:01:13', '', 33, 'http://coming-soon.test/?post_type=acf-field&#038;p=34', 0, 'acf-field', '', 0),
(35, 1, '2019-04-03 21:43:43', '2019-04-03 21:43:43', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:3:\"100\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Content to Enter', 'contained_editor', 'publish', 'closed', 'closed', '', 'field_5c3fb1c9f22f8', '', '', '2019-05-07 22:01:13', '2019-05-07 22:01:13', '', 33, 'http://coming-soon.test/?post_type=acf-field&#038;p=35', 3, 'acf-field', '', 0),
(36, 1, '2019-04-03 21:45:23', '2019-04-03 21:45:23', '', 'placeholder-1920x600', '', 'inherit', 'open', 'closed', '', 'placeholder-1920x600', '', '', '2019-04-03 21:45:23', '2019-04-03 21:45:23', '', 2, 'http://coming-soon.test/app/uploads/2019/04/placeholder-1920x600.png', 0, 'attachment', 'image/png', 0),
(38, 1, '2019-04-03 21:56:11', '2019-04-03 21:56:11', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Contact Image', 'contact_image', 'publish', 'closed', 'closed', '', 'field_5ca52be54aa57', '', '', '2019-05-07 22:30:06', '2019-05-07 22:30:06', '', 6, 'http://coming-soon.test/?post_type=acf-field&#038;p=38', 8, 'acf-field', '', 0),
(39, 1, '2019-04-03 21:56:31', '2019-04-03 21:56:31', '', 'xga', '', 'inherit', 'open', 'closed', '', 'xga', '', '', '2019-05-06 23:03:13', '2019-05-06 23:03:13', '', 2, 'http://coming-soon.test/app/uploads/2019/04/xga.png', 0, 'attachment', 'image/png', 0),
(41, 1, '2019-04-03 22:07:48', '2019-04-03 22:07:48', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Email Address', 'email_field', 'publish', 'closed', 'closed', '', 'field_5ca52ea416411', '', '', '2019-04-03 22:07:48', '2019-04-03 22:07:48', '', 6, 'http://coming-soon.test/?post_type=acf-field&p=41', 2, 'acf-field', '', 0),
(71, 1, '2019-05-07 22:01:13', '2019-05-07 22:01:13', 'a:6:{s:4:\"type\";s:12:\"color_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";}', 'Background Color', 'background_color', 'publish', 'closed', 'closed', '', 'field_5cbdfb3a31d8e', '', '', '2019-05-07 22:01:13', '2019-05-07 22:01:13', '', 33, 'http://coming-soon.test/?post_type=acf-field&p=71', 1, 'acf-field', '', 0),
(72, 1, '2019-05-07 22:01:13', '2019-05-07 22:01:13', 'a:13:{s:4:\"type\";s:6:\"select\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"choices\";a:1:{s:9:\"dark-back\";s:3:\"Yes\";}s:13:\"default_value\";a:0:{}s:10:\"allow_null\";i:1;s:8:\"multiple\";i:0;s:2:\"ui\";i:0;s:13:\"return_format\";s:5:\"value\";s:4:\"ajax\";i:0;s:11:\"placeholder\";s:0:\"\";}', 'Dark Background?', 'dark_background', 'publish', 'closed', 'closed', '', 'field_5cba4691d9c3d', '', '', '2019-05-07 22:01:13', '2019-05-07 22:01:13', '', 33, 'http://coming-soon.test/?post_type=acf-field&p=72', 2, 'acf-field', '', 0),
(73, 1, '2019-05-07 22:01:13', '2019-05-07 22:01:13', 'a:8:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"page\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";s:5:\"local\";s:3:\"php\";}', 'Creator-Full Width', 'creator-full-width', 'publish', 'closed', 'closed', '', 'group_5cb9fdb05bbe1', '', '', '2019-05-07 22:01:13', '2019-05-07 22:01:13', '', 0, 'http://coming-soon.test/?post_type=acf-field-group&p=73', 0, 'acf-field-group', '', 0),
(74, 1, '2019-05-07 22:01:13', '2019-05-07 22:01:13', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";i:0;s:3:\"max\";i:0;s:6:\"layout\";s:5:\"block\";s:12:\"button_label\";s:0:\"\";}', 'Creator', 'section_f_creator', 'publish', 'closed', 'closed', '', 'field_5cba18ec5a507', '', '', '2019-05-07 22:01:13', '2019-05-07 22:01:13', '', 73, 'http://coming-soon.test/?post_type=acf-field&p=74', 0, 'acf-field', '', 0),
(75, 1, '2019-05-07 22:01:13', '2019-05-07 22:01:13', 'a:13:{s:4:\"type\";s:6:\"select\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"25\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"choices\";a:2:{s:9:\"order-one\";s:5:\"First\";s:9:\"order-two\";s:6:\"Second\";}s:13:\"default_value\";a:0:{}s:10:\"allow_null\";i:1;s:8:\"multiple\";i:0;s:2:\"ui\";i:0;s:13:\"return_format\";s:5:\"value\";s:4:\"ajax\";i:0;s:11:\"placeholder\";s:0:\"\";}', 'Order Control', 'order_num', 'publish', 'closed', 'closed', '', 'field_5cba1834ed9e6', '', '', '2019-05-07 22:01:13', '2019-05-07 22:01:13', '', 74, 'http://coming-soon.test/?post_type=acf-field&p=75', 0, 'acf-field', '', 0),
(76, 1, '2019-05-07 22:01:13', '2019-05-07 22:01:13', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"75\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Image Field', 'background_field', 'publish', 'closed', 'closed', '', 'field_5cba180ced9e5', '', '', '2019-05-07 22:01:13', '2019-05-07 22:01:13', '', 74, 'http://coming-soon.test/?post_type=acf-field&p=76', 1, 'acf-field', '', 0),
(77, 1, '2019-05-07 22:01:13', '2019-05-07 22:01:13', 'a:13:{s:4:\"type\";s:6:\"select\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"25\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"choices\";a:2:{s:9:\"order-one\";s:5:\"First\";s:9:\"order-two\";s:6:\"Second\";}s:13:\"default_value\";a:0:{}s:10:\"allow_null\";i:1;s:8:\"multiple\";i:0;s:2:\"ui\";i:0;s:13:\"return_format\";s:5:\"value\";s:4:\"ajax\";i:0;s:11:\"placeholder\";s:0:\"\";}', 'Order Control', 'order_num_two', 'publish', 'closed', 'closed', '', 'field_5cba1a10586b3', '', '', '2019-05-07 22:01:13', '2019-05-07 22:01:13', '', 74, 'http://coming-soon.test/?post_type=acf-field&p=77', 2, 'acf-field', '', 0),
(78, 1, '2019-05-07 22:01:13', '2019-05-07 22:01:13', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"75\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Content Field', 'default_f_editor', 'publish', 'closed', 'closed', '', 'field_5cba1a24586b4', '', '', '2019-05-07 22:01:13', '2019-05-07 22:01:13', '', 74, 'http://coming-soon.test/?post_type=acf-field&p=78', 3, 'acf-field', '', 0),
(79, 1, '2019-05-07 22:01:13', '2019-05-07 22:01:13', 'a:6:{s:4:\"type\";s:12:\"color_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";}', 'Background Color', 'bg_clr_f', 'publish', 'closed', 'closed', '', 'field_5cba1accd7240', '', '', '2019-05-07 22:01:13', '2019-05-07 22:01:13', '', 74, 'http://coming-soon.test/?post_type=acf-field&p=79', 4, 'acf-field', '', 0),
(80, 1, '2019-05-07 22:01:13', '2019-05-07 22:01:13', 'a:13:{s:4:\"type\";s:6:\"select\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"choices\";a:1:{s:9:\"dark-back\";s:3:\"Yes\";}s:13:\"default_value\";a:0:{}s:10:\"allow_null\";i:1;s:8:\"multiple\";i:0;s:2:\"ui\";i:0;s:13:\"return_format\";s:5:\"value\";s:4:\"ajax\";i:0;s:11:\"placeholder\";s:0:\"\";}', 'Dark Background?', 'dark_back_color', 'publish', 'closed', 'closed', '', 'field_5cba19bdcf299', '', '', '2019-05-07 22:01:13', '2019-05-07 22:01:13', '', 74, 'http://coming-soon.test/?post_type=acf-field&p=80', 5, 'acf-field', '', 0),
(100, 1, '2019-05-07 22:30:06', '2019-05-07 22:30:06', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Google Map', 'contact_iframe', 'publish', 'closed', 'closed', '', 'field_5cd206dbdbba3', '', '', '2019-05-07 22:32:46', '2019-05-07 22:32:46', '', 6, 'http://coming-soon.test/?post_type=acf-field&#038;p=100', 7, 'acf-field', '', 0),
(104, 1, '2019-05-07 22:46:36', '2019-05-07 22:46:36', '<h1 style=\"text-align: center;\">Blue Water Bar &amp; Grill</h1>\r\n&nbsp;\r\n\r\n[row]\r\n\r\n[column columns=6]\r\n<h2>WHERE NEW FRIENDS MEET AND FAMILIES EAT!</h2>\r\nAs our guest, you’ll have continual access to our on-site restaurant and bar, one of the most scenic indoor and outdoor venues on the gulf coast! The Blue Water Bar and Grill is also open to the public, so there’s always plenty of neighborhood hospitality to add to your enjoyment.\r\n\r\n[/column]\r\n\r\n[column columns=6]\r\n\r\n<img class=\"alignnone size-full wp-image-93\" src=\"http://coming-soon.test/app/uploads/2019/05/bar.jpg\" alt=\"\" width=\"1000\" height=\"669\" />\r\n\r\n&nbsp;\r\n\r\n[/column]\r\n\r\n[/row]', 'Home', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2019-05-07 22:46:36', '2019-05-07 22:46:36', '', 2, 'http://coming-soon.test/2-revision-v1/', 0, 'revision', '', 0),
(105, 1, '2019-05-07 22:54:08', '2019-05-07 22:54:08', '<h1 style=\"text-align: center;\">Blue Water Bar &amp; Grill</h1>\r\n&nbsp;\r\n\r\n[row]\r\n\r\n[column columns=6]\r\n<h2>WHERE NEW FRIENDS MEET AND FAMILIES EAT!</h2>\r\nAs our guest, you’ll have continual access to our on-site restaurant and bar, one of the most scenic indoor and outdoor venues on the gulf coast! The Blue Water Bar and Grill is also open to the public, so there’s always plenty of neighborhood hospitality to add to your enjoyment.\r\n\r\n[/column]\r\n\r\n[column columns=6]\r\n\r\n<img class=\"alignnone size-full wp-image-93\" src=\"http://coming-soon.test/app/uploads/2019/05/bar.jpg\" alt=\"\" width=\"1000\" height=\"669\" />\r\n\r\n&nbsp;\r\n\r\n[/column]\r\n\r\n[/row]', 'Home', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2019-05-07 22:54:08', '2019-05-07 22:54:08', '', 2, 'http://coming-soon.test/2-revision-v1/', 0, 'revision', '', 0),
(106, 1, '2019-05-07 22:54:46', '2019-05-07 22:54:46', '<h1 style=\"text-align: center;\">Blue Water Bar &amp; Grill</h1>\r\n&nbsp;\r\n\r\n[row]\r\n\r\n[column columns=6]\r\n<h2>WHERE NEW FRIENDS MEET AND FAMILIES EAT!</h2>\r\nAs our guest, you’ll have continual access to our on-site restaurant and bar, one of the most scenic indoor and outdoor venues on the gulf coast! The Blue Water Bar and Grill is also open to the public, so there’s always plenty of neighborhood hospitality to add to your enjoyment.\r\n\r\n[/column]\r\n\r\n[column columns=6]\r\n\r\n<img class=\"alignnone size-full wp-image-93\" src=\"http://coming-soon.test/app/uploads/2019/05/bar.jpg\" alt=\"\" width=\"1000\" height=\"669\" />\r\n\r\n&nbsp;\r\n\r\n[/column]\r\n\r\n[/row]', 'Home', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2019-05-07 22:54:46', '2019-05-07 22:54:46', '', 2, 'http://coming-soon.test/2-revision-v1/', 0, 'revision', '', 0),
(107, 1, '2019-05-07 22:55:04', '2019-05-07 22:55:04', '<h1 style=\"text-align: center;\">Blue Water Bar &amp; Grill</h1>\r\n&nbsp;\r\n\r\n[row]\r\n\r\n[column columns=6]\r\n<h2>WHERE NEW FRIENDS MEET AND FAMILIES EAT!</h2>\r\nAs our guest, you’ll have continual access to our on-site restaurant and bar, one of the most scenic indoor and outdoor venues on the gulf coast! The Blue Water Bar and Grill is also open to the public, so there’s always plenty of neighborhood hospitality to add to your enjoyment.\r\n\r\n[/column]\r\n\r\n[column columns=6]\r\n\r\n<img class=\"alignnone size-full wp-image-93\" src=\"http://coming-soon.test/app/uploads/2019/05/bar.jpg\" alt=\"\" width=\"1000\" height=\"669\" />\r\n\r\n&nbsp;\r\n\r\n[/column]\r\n\r\n[/row]', 'Home', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2019-05-07 22:55:04', '2019-05-07 22:55:04', '', 2, 'http://coming-soon.test/2-revision-v1/', 0, 'revision', '', 0),
(108, 1, '2019-05-07 23:06:02', '2019-05-07 23:06:02', '<h1 style=\"text-align: center;\">Blue Water Bar &amp; Grill</h1>\r\n&nbsp;\r\n\r\n[row]\r\n\r\n[column columns=6]\r\n<h2>WHERE NEW FRIENDS MEET AND FAMILIES EAT!</h2>\r\nAs our guest, you’ll have continual access to our on-site restaurant and bar, one of the most scenic indoor and outdoor venues on the gulf coast! The Blue Water Bar and Grill is also open to the public, so there’s always plenty of neighborhood hospitality to add to your enjoyment.\r\n\r\n[/column]\r\n\r\n[column columns=6]\r\n\r\n<img class=\"alignnone size-full wp-image-93\" src=\"http://coming-soon.test/app/uploads/2019/05/bar.jpg\" alt=\"\" width=\"1000\" height=\"669\" />\r\n\r\n&nbsp;\r\n\r\n[/column]\r\n\r\n[/row]', 'Home', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2019-05-07 23:06:02', '2019-05-07 23:06:02', '', 2, 'http://coming-soon.test/2-revision-v1/', 0, 'revision', '', 0),
(109, 1, '2019-05-07 23:07:28', '2019-05-07 23:07:28', '<h1 style=\"text-align: center;\">Blue Water Bar &amp; Grill</h1>\r\n&nbsp;\r\n\r\n[row]\r\n\r\n[column columns=6]\r\n<h2>WHERE NEW FRIENDS MEET AND FAMILIES EAT!</h2>\r\nAs our guest, you’ll have continual access to our on-site restaurant and bar, one of the most scenic indoor and outdoor venues on the gulf coast! The Blue Water Bar and Grill is also open to the public, so there’s always plenty of neighborhood hospitality to add to your enjoyment.\r\n\r\n<a href=\"tel:9792394305\" class=\"btn btn-o\">Call Now</a>\r\n\r\n[/column]\r\n\r\n[column columns=6]\r\n\r\n<img class=\"alignnone size-full wp-image-93\" src=\"http://coming-soon.test/app/uploads/2019/05/bar.jpg\" alt=\"\" width=\"1000\" height=\"669\" />\r\n\r\n&nbsp;\r\n\r\n[/column]\r\n\r\n[/row]', 'Home', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2019-05-07 23:07:28', '2019-05-07 23:07:28', '', 2, 'http://coming-soon.test/2-revision-v1/', 0, 'revision', '', 0),
(110, 1, '2019-05-07 23:08:20', '2019-05-07 23:08:20', '<h1 style=\"text-align: center;\">Blue Water Bar &amp; Grill</h1>\r\n&nbsp;\r\n\r\n[row]\r\n\r\n[column columns=6]\r\n<h2>WHERE NEW FRIENDS MEET AND FAMILIES EAT!</h2>\r\nAs our guest, you’ll have continual access to our on-site restaurant and bar, one of the most scenic indoor and outdoor venues on the gulf coast! The Blue Water Bar and Grill is also open to the public, so there’s always plenty of neighborhood hospitality to add to your enjoyment.\r\n\r\n<a class=\"btn btn-b\" href=\"tel:9792394305\">Call Now</a>\r\n\r\n[/column]\r\n\r\n[column columns=6]\r\n\r\n<img class=\"alignnone size-full wp-image-93\" src=\"http://coming-soon.test/app/uploads/2019/05/bar.jpg\" alt=\"\" width=\"1000\" height=\"669\" />\r\n\r\n[/column]\r\n\r\n[/row]', 'Home', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2019-05-07 23:08:20', '2019-05-07 23:08:20', '', 2, 'http://coming-soon.test/2-revision-v1/', 0, 'revision', '', 0),
(111, 1, '2019-05-07 23:08:42', '2019-05-07 23:08:42', '<h1 style=\"text-align: center;\">Blue Water Bar &amp; Grill</h1>\r\n&nbsp;\r\n\r\n[row]\r\n\r\n[column columns=6]\r\n<h2>WHERE NEW FRIENDS MEET AND FAMILIES EAT!</h2>\r\nAs our guest, you’ll have continual access to our on-site restaurant and bar, one of the most scenic indoor and outdoor venues on the gulf coast! The Blue Water Bar and Grill is also open to the public, so there’s always plenty of neighborhood hospitality to add to your enjoyment.\r\n\r\n<a class=\"btn btn-b\" href=\"tel:9792394305\">Call Now</a>\r\n\r\n[/column]\r\n\r\n[column columns=6]\r\n\r\n<img class=\"alignnone size-full wp-image-93\" src=\"http://coming-soon.test/app/uploads/2019/05/bar.jpg\" alt=\"\" width=\"1000\" height=\"669\" />\r\n\r\n[/column]\r\n\r\n[/row]', 'Home', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2019-05-07 23:08:42', '2019-05-07 23:08:42', '', 2, 'http://coming-soon.test/2-revision-v1/', 0, 'revision', '', 0),
(112, 1, '2019-05-17 22:36:57', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-05-17 22:36:57', '0000-00-00 00:00:00', '', 0, 'http://coming-soon.test/?p=112', 0, 'post', '', 0),
(113, 1, '2019-05-17 22:37:30', '2019-05-17 22:37:30', '', 'Home', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2019-05-17 22:37:30', '2019-05-17 22:37:30', '', 2, 'http://coming-soon.test/2-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cs_termmeta`
--

CREATE TABLE `cs_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cs_terms`
--

CREATE TABLE `cs_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cs_terms`
--

INSERT INTO `cs_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cs_term_relationships`
--

CREATE TABLE `cs_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cs_term_relationships`
--

INSERT INTO `cs_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cs_term_taxonomy`
--

CREATE TABLE `cs_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cs_term_taxonomy`
--

INSERT INTO `cs_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cs_usermeta`
--

CREATE TABLE `cs_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cs_usermeta`
--

INSERT INTO `cs_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'bigrigdev'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'cs_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'cs_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy'),
(15, 1, 'show_welcome_panel', '0'),
(17, 1, 'cs_dashboard_quick_press_last_post_id', '112'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}'),
(19, 1, 'cs_user-settings', 'libraryContent=browse&editor=html&hidetb=1&imgsize=full'),
(20, 1, 'cs_user-settings-time', '1557270517'),
(21, 1, 'session_tokens', 'a:1:{s:64:\"b90a505fb6d120ca2a695ea841b26b96ccfdc02675563eb0d19e4e4b0f6ffbf8\";a:4:{s:10:\"expiration\";i:1558305414;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36\";s:5:\"login\";i:1558132614;}}'),
(22, 1, 'gform_recent_forms', 'a:1:{i:0;s:1:\"1\";}'),
(23, 1, 'closedpostboxes_dashboard', 'a:5:{i:0;s:19:\"dashboard_right_now\";i:1;s:18:\"dashboard_activity\";i:2;s:18:\"rg_forms_dashboard\";i:3;s:21:\"dashboard_quick_press\";i:4;s:17:\"dashboard_primary\";}'),
(24, 1, 'metaboxhidden_dashboard', 'a:0:{}'),
(25, 1, 'meta-box-order_dashboard', 'a:4:{s:6:\"normal\";s:38:\"dashboard_right_now,dashboard_activity\";s:4:\"side\";s:21:\"dashboard_quick_press\";s:7:\"column3\";s:36:\"rg_forms_dashboard,dashboard_primary\";s:7:\"column4\";s:0:\"\";}'),
(26, 1, 'closedpostboxes_toplevel_page_global-settings', 'a:1:{i:0;s:23:\"acf-group_5c86eabade229\";}'),
(27, 1, 'metaboxhidden_toplevel_page_global-settings', 'a:0:{}'),
(28, 1, 'closedpostboxes_page', 'a:0:{}'),
(29, 1, 'metaboxhidden_page', 'a:7:{i:0;s:23:\"acf-group_5c86eabade229\";i:1;s:12:\"revisionsdiv\";i:2;s:11:\"postexcerpt\";i:3;s:16:\"commentstatusdiv\";i:4;s:11:\"commentsdiv\";i:5;s:7:\"slugdiv\";i:6;s:9:\"authordiv\";}');

-- --------------------------------------------------------

--
-- Table structure for table `cs_users`
--

CREATE TABLE `cs_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cs_users`
--

INSERT INTO `cs_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'bigrigdev', '$P$B0DE757bxcpB6j5Uy1/hWfSYs.NClE/', 'bigrigdev', 'developer@bigrigmedia.com', '', '2019-04-03 21:24:50', '', 0, 'bigrigdev');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cs_commentmeta`
--
ALTER TABLE `cs_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `cs_comments`
--
ALTER TABLE `cs_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `cs_gf_draft_submissions`
--
ALTER TABLE `cs_gf_draft_submissions`
  ADD PRIMARY KEY (`uuid`),
  ADD KEY `form_id` (`form_id`);

--
-- Indexes for table `cs_gf_entry`
--
ALTER TABLE `cs_gf_entry`
  ADD PRIMARY KEY (`id`),
  ADD KEY `form_id` (`form_id`),
  ADD KEY `form_id_status` (`form_id`,`status`);

--
-- Indexes for table `cs_gf_entry_meta`
--
ALTER TABLE `cs_gf_entry_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meta_key` (`meta_key`(191)),
  ADD KEY `entry_id` (`entry_id`),
  ADD KEY `meta_value` (`meta_value`(191));

--
-- Indexes for table `cs_gf_entry_notes`
--
ALTER TABLE `cs_gf_entry_notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `entry_id` (`entry_id`),
  ADD KEY `entry_user_key` (`entry_id`,`user_id`);

--
-- Indexes for table `cs_gf_form`
--
ALTER TABLE `cs_gf_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_gf_form_meta`
--
ALTER TABLE `cs_gf_form_meta`
  ADD PRIMARY KEY (`form_id`);

--
-- Indexes for table `cs_gf_form_revisions`
--
ALTER TABLE `cs_gf_form_revisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `date_created` (`date_created`),
  ADD KEY `form_id` (`form_id`);

--
-- Indexes for table `cs_gf_form_view`
--
ALTER TABLE `cs_gf_form_view`
  ADD PRIMARY KEY (`id`),
  ADD KEY `date_created` (`date_created`),
  ADD KEY `form_id` (`form_id`);

--
-- Indexes for table `cs_links`
--
ALTER TABLE `cs_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `cs_options`
--
ALTER TABLE `cs_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `cs_postmeta`
--
ALTER TABLE `cs_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `cs_posts`
--
ALTER TABLE `cs_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `cs_termmeta`
--
ALTER TABLE `cs_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `cs_terms`
--
ALTER TABLE `cs_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `cs_term_relationships`
--
ALTER TABLE `cs_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `cs_term_taxonomy`
--
ALTER TABLE `cs_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `cs_usermeta`
--
ALTER TABLE `cs_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `cs_users`
--
ALTER TABLE `cs_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cs_commentmeta`
--
ALTER TABLE `cs_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs_comments`
--
ALTER TABLE `cs_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cs_gf_entry`
--
ALTER TABLE `cs_gf_entry`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs_gf_entry_meta`
--
ALTER TABLE `cs_gf_entry_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs_gf_entry_notes`
--
ALTER TABLE `cs_gf_entry_notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs_gf_form`
--
ALTER TABLE `cs_gf_form`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cs_gf_form_revisions`
--
ALTER TABLE `cs_gf_form_revisions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs_gf_form_view`
--
ALTER TABLE `cs_gf_form_view`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `cs_links`
--
ALTER TABLE `cs_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs_options`
--
ALTER TABLE `cs_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=667;

--
-- AUTO_INCREMENT for table `cs_postmeta`
--
ALTER TABLE `cs_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1249;

--
-- AUTO_INCREMENT for table `cs_posts`
--
ALTER TABLE `cs_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT for table `cs_termmeta`
--
ALTER TABLE `cs_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs_terms`
--
ALTER TABLE `cs_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cs_term_taxonomy`
--
ALTER TABLE `cs_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cs_usermeta`
--
ALTER TABLE `cs_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `cs_users`
--
ALTER TABLE `cs_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
