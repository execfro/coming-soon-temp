<?php
@$street = get_field('address_street_name', 'options');
@$city = get_field('address_city_name', 'options');
@$state = get_field('address_state', 'options');
@$zip = get_field('address_zipcode', 'options');
@$address = $street . '<br>' . $city . ', ' . $state .' '. $zip;
@$fax = get_field('fax_number', 'option');
@$hours = get_field('hours_open', 'option');
@$message = get_field('hours_message', 'option');
@$office = get_field('phone_number', 'option');


?>

<footer class="section section-footer">
  <div class="content">

    <a class="brand header__brand" href="https://doctorsfr.com/">
      <img src="<?= App\asset_path('images/footer-logo.svg'); ?>" title="<?php echo e(get_bloginfo('name', 'display')); ?>" alt="<?php echo e(get_bloginfo('name', 'display')); ?>" width="340" height="200">
    </a>

    <h4>Doctors Identity Guard is presented to you by <strong><a href="https://doctorsfr.com/">Doctors Financial Resources</a></strong></h4>

    <div itemscope itemtype="http://schema.org/RVPark" class="mbd">

      <?php if($address): ?>

      <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
        <span itemprop="streetAddress"><?php echo e($street); ?></span>
        <span itemprop="addressLocality"><?php echo e($city); ?></span>,
        <span itemprop="addressRegion"><?php echo e($state); ?></span>
        <span itemprop="postalCode"><?php echo e($zip); ?></span>
        <a class="footer__link footer__link--phone" href="tel:<?php echo e(preg_replace('/[^0-9]/', '', $office  )); ?>" title="Call Toll Free <?php echo e($office); ?>"><span>Toll Free <?php echo e($office); ?></span></a>

      </address>

      <?php endif; ?>


    </div>



    <p class="f-rights"> &copy; <?= date('Y'); ?> <?php bloginfo('name'); ?> | Website Design by  <a href="http://www.bigrigmedia.com">Big Rig Media LLC</a> &reg;</p>

  </footer>
