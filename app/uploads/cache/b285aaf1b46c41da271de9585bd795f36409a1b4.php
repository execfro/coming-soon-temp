<article <?php post_class() ?>>
  <article <?php post_class('c-post c-post--single'); ?>>
    <h1 class="entry-title"><?php echo get_the_title(); ?></h1>
    <?php echo $__env->make('partials/entry-meta', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php the_content() ?>

  </article>
