
<?php $facebook_url = get_field('facebook_url','option');

@$instagram_url = get_field('instagram_url','option');

@$google_url = get_field('google_url','option');

@$yelp_url = get_field('yelp_url','option');

?>

<ul class="social-media">

  <?php if( $google_url ): ?>

  <li>

    <a href="<?php echo e($google_url); ?>">

      <i class="fab fa-google"></i>

    </a>

  </li>

  <?php endif; ?>

  <?php if( $facebook_url ): ?>

  <li>

    <a href="<?php echo e($facebook_url); ?>">

      <i class="fab fa-facebook-f"></i>

    </a>

  </li>

  <?php endif; ?>

  <?php if( $yelp_url ): ?>

  <li>

    <a href="<?php echo e($yelp_url); ?>">

      <i class="fab fa-yelp"></i>

    </a>

  </li>

  <?php endif; ?>

  <?php if( $instagram_url ): ?>

  <li>

    <a href="<?php echo e($instagram_url); ?>">

      <i class="fab fa-instagram"></i>

    </a>

  </li>

  <?php endif; ?>

</ul>
