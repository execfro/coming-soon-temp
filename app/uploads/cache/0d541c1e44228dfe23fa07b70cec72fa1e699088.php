<?php

@$services_image = get_field('services_image');

@$service_content = get_field('service_content');

@$service_header_text = get_field('service_header_text');

?>

<?php if( $service_content ): ?>

<section class="section section--services">

  <?php if( $service_header_text ): ?>

  <div class="o-content">

    <h3><?php echo e($service_header_text); ?></h3>

  </div>

  <?php endif; ?>

  <div class="fl-btw fl-column">

    <div class="column column-image" style="background-image: url( <?php echo $services_image; ?> )" >
    </div>

    <div class="column">

      <div class="inner">

        <?php echo $service_content; ?>


      </div>

    </div>

  </div>

</section>

<?php endif; ?>
