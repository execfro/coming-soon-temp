<?php
$a = 1;
$b = 1;
?>

<?php if( have_rows('section_creator') ): ?>

<section class="section section--creator">

  <?php while( have_rows('section_creator') ): ?> <?php the_row() ?>

  <div
  class="section creator creator-<?= $a++;?>"
  <?php if(get_sub_field('background_field')): ?>
  style="background-color:<?php echo the_sub_field('background_field'); ?>"
  <?php endif; ?>
  >

  <div class="content">

    <div class="card">

      <?php echo e(the_sub_field('default_editor')); ?>


    </div>

  </div>

</div>

<style>

.creator-<?php echo e($b++); ?> {
  background-color: #F0F0F0;
}

</style>

<?php endwhile; ?>

</section>

<?php endif; ?>
