<div class="owl-carousel c-carousel c-carousel--hero">

  <?php
  // check if the repeater field has rows of data
  if( have_rows('repeater_heroslider') ):

    // loop through the rows of data
    while ( have_rows('repeater_heroslider') ) : the_row(); {?>

      <div class="slider">

        <div class="column column-1">

          <?php the_sub_field('sub_field_blurb');?>

        </div>

        <div class="column column-2 hero-back" style="background-image: url(<?= get_sub_field('sub_field_image');?>)">

        </div>

      </div>

    <?php }
  endwhile;
  else :
    // no rows found
  endif;  ?>
</div>
