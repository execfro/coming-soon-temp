<?php

$i = 1;

// the query
$wpb_all_query = new WP_Query(array(
  'post_type'=>'testimonial',
  'post_status'=>'publish',
  'posts_per_page'=>-1));

  ?>
  <?php if ( $wpb_all_query->have_posts() ) : ?>

    <section class="section section--testimonials">
      <div class="column column-test">
        <section class="inner">
          <h1>Superior Results</h1>
          <img src="/app/themes/sage/resources/assets/images/stars.png">
          <div class="slider">
            <div class="owl-carousel c-carousel c-carousel--test">
              <!-- the loop -->
              <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>

                <div class="testimonial">
                  <p><strong><?= the_content();?></strong></p>
                  <p><?= the_title();?></p>
                </div>
              <?php endwhile; ?>
              <!-- end of the loop -->
              <?php wp_reset_postdata(); ?>
            </div>
          </div>
        </section>
      </div>
      <div class="column column-truck">
      </div>
    </section>

  <?php endif; ?>
