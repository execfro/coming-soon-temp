<?php
@$street = get_field('address_street_name', 'options');
@$city = get_field('address_city_name', 'options');
@$state = get_field('address_state', 'options');
@$zip = get_field('address_zipcode', 'options');
@$address = $street . '<br>' . $city . ', ' . $state .' '. $zip;
@$fax = get_field('fax_number', 'option');
@$hours = get_field('hours_open', 'option');
@$message = get_field('hours_message', 'option');
@$office = get_field('phone_number', 'option');
@$contact_image = get_field('contact_image', 'option');
@$contact_iframe = get_field('contact_iframe', 'option');
@$phone = get_field('phone_number', 'option');
@$email = get_field('email_field', 'option');
?>

<div class="section section--contact fl-btw fl-column">
  <div class="column">
    <div class="inner">

      <h4><?php echo get_bloginfo(); ?></h4>

      <div itemscope itemtype="http://schema.org/RVPark" class="mbd">

        <?php if($address): ?>

        <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
          <span itemprop="streetAddress"><?php echo e($street); ?></span>
          <span itemprop="addressLocality"><?php echo e($city); ?></span>,
          <span itemprop="addressRegion"><?php echo e($state); ?></span>
          <span itemprop="postalCode"><?php echo e($zip); ?></span>

        </address>


        <?php endif; ?>

        <?php if( $phone ): ?>

        <a class="header__link header__link--phone" href="tel:<?php echo e(preg_replace('/[^0-9]/', '', $phone  )); ?>" title="Call Toll Free <?php echo e($phone); ?>"><i class="fas fa-phone"></i> <span class="p_num"><?php echo e($phone); ?></span></a>

        <?php endif; ?>

        <?php if( $email): ?>

        <br>  <a class="email" href="mailto:<?php echo e($email); ?>"><i class="fas fa-envelope"></i> <span class="e_num"><?php echo e($email); ?></span></a>

        <?php endif; ?>

        <?php echo $__env->make('partials.social-media', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      </div>
    </div>
  </div>

  <div class="column" style="background-image: url(<?php echo $contact_image; ?>)">

    <?php if($contact_iframe): ?>

    <?php echo $contact_iframe; ?>


    <?php endif; ?>

  </div>
</div>
