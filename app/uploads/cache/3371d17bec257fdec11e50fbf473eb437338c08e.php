<?php

$i = 1;

// the query
$wpb_all_query = new WP_Query(array(
  'post_type'=>'testimonial',
  'post_status'=>'publish',
  'posts_per_page'=>-1));

  ?>

  <?php if ( $wpb_all_query->have_posts() ) : ?>
    <section class="slider">
        <div class="owl-carousel c-carousel c-carousel--test">
          <!-- the loop -->
          <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>

            <div class="testimonial">
              <p><?= the_content();?></p>
              <a class="source-name"><?= the_title();?></a>
            </div>
          <?php endwhile; ?>
          <!-- end of the loop -->
          <?php wp_reset_postdata(); ?>
        </div>
    </section>
  <?php endif; ?>
