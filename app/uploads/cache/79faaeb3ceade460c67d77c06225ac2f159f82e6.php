<?php

@$whyus_image = get_field('whyus_image');

@$whyus_content = get_field('whyus_content');

@$whyus_header_text = get_field('whyus_header_text');

?>

<?php if( $whyus_content ): ?>

<section class="section section--services why-us">

  <?php if( $whyus_header_text ): ?>

  <div class="o-content">

    <h3><?php echo $whyus_header_text; ?></h3>

  </div>

  <?php endif; ?>

  <div class="fl-btw fl-column">

    <div class="column column-image" style="background-image: url( <?php echo $whyus_image; ?> )" >
    </div>

    <div class="column">

      <div class="inner">

        <?= $whyus_content ;?>

      </div>

    </div>

  </div>

</section>

<?php endif; ?>
