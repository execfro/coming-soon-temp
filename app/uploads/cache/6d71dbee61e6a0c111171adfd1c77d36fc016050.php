<div class="owl-carousel c-carousel c-carousel--hero">

  <?php
  // check if the repeater field has rows of data
  if( have_rows('blog_hero', 'options') ):

    // loop through the rows of data
    while ( have_rows('blog_hero', 'options') ) : the_row(); {?>

      <div class="slider">

        <div class="column column-1">

          <div class="inner">

            <?= get_sub_field('Blog_hero_blurb', 'options');?>

          </div>

        </div>

        <div class="column column-2 hero-back" style="background-image: url(<?= get_sub_field('image_blog', 'options');?>)">

        </div>

      </div>

    <?php }
  endwhile;
  else :
    // no rows found
  endif;  ?>
</div>
