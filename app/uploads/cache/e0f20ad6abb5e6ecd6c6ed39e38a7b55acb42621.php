<?php if(get_field('call_to_action', 'options') ): ?>

<section class="section section--action">
  <div class="content">
      <?php echo get_field('call_to_action', 'options'); ?>

  </div>
</section>

<?php endif; ?>
