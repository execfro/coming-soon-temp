<?php

@$identity_text = get_field('identity_text');

?>

<?php if( $identity_text ): ?>

<section class="section section--identity">

  <div class="content">

    <?php echo $identity_text; ?>


  </div>

</section>

<?php endif; ?>
