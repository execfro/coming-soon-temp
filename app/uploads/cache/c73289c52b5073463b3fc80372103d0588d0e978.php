<?php
$a = 1;
$b = 1;

?>

<?php if( have_rows('section_creator') ): ?>

<section class="section-creator">

  <?php while( have_rows('section_creator') ): ?> <?php the_row() ?>

  <div class="lazyload creator-<?php echo $a++; ?> bg_img <?php echo get_sub_field('dark_background'); ?>"
  <?php if( get_sub_field('background_field_contained') ): ?>
  style="background-image:url(<?php echo get_sub_field('background_field_contained'); ?>)"
  <?php endif; ?>
  <?php if( get_sub_field('background_color') ): ?>
  style="background-color: <?php echo get_sub_field('background_color'); ?>"
  <?php endif; ?>
  >

  <div class="content">

    <?php echo the_sub_field('contained_editor'); ?>


  </div>

</div>

<?php endwhile; ?>

</section>

<?php endif; ?>
