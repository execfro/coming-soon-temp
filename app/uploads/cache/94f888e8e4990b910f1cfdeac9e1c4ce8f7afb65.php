<article <?php post_class('c-post'); ?>>
  <div class="flex-control">
    <div class="column column--1" >
      <a href="<?php echo get_permalink();?>">
        <?php if(has_post_thumbnail() ): ?>

        @the_post_thumbnail()

        <?php else: ?>

        <img src="<?= App\asset_path('images/logo_superior_air.svg'); ?>" title="<?php echo e(get_bloginfo('name', 'display')); ?>" alt="<?php echo e(get_bloginfo('name', 'display')); ?>" width="340" height="200">

        <?php endif; ?>

      </a>

    </div>

    <div class="column column--2" >

      <a href="<?php the_permalink(); ?>">
        <h2><?php the_title(); ?></h2></a>
        <?php the_excerpt(); ?>

      </div>

    </div>

  </article>
