<?php

$i = 1;

?>

<?php if( is_front_page() && is_home() ): ?>


<div class="hero">

  <div class="hero__content">

    <?php echo do_shortcode('[hero_slider]'); ?>


  </div>

</div>

<?php elseif( is_front_page() ): ?>

<div class="hero">
  <?php echo do_shortcode('[hero_slider]'); ?>

</div>

<?php elseif( !is_front_page() && is_home() || is_404() || is_single() ): ?>

<div class="hero">
  <div class="hero__content">
    <?php echo $__env->make('partials.blog-hero', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </div>
</div>

<?php else: ?>

  <?php if(is_page()): ?>
  <div class="hero">
    <div class="hero__content">
      <?php echo do_shortcode('[hero_slider]'); ?>

    </div>
  </div>

  <?php endif; ?>

  <?php if(!is_page() || is_single() || is_archive() ): ?>

  <div class="hero">
    <div class="hero__content">
      <?php echo $__env->make('partials.blog-hero', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
  </div>

  <?php endif; ?>

<?php endif; ?>
