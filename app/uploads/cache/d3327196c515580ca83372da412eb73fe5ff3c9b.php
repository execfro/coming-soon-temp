<?php if( have_rows('location_name', 'option') ): ?>

<ul class="locations fl-start">

  <?php while( have_rows('location_name', 'option') ): ?> <?php the_row() ?>

  <li class="city_name">

    <?php echo e(the_sub_field('city_name', 'option')); ?>


  </li>

  <?php endwhile; ?>

</ul>

<?php endif; ?>
