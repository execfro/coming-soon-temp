<?php
$i =1;
$query = new WP_Query([
  'post_type' => 'page',
  'post__in'  => [31,33],
  'orderby'   => 'post__in'
]);

?>

<section class="section section--portal">

  <div class="content">

    <?php if($query->have_posts()): ?>

    <?php while($query->have_posts()): ?> <?php $query->the_post() ?>

    <?php $url = wp_get_attachment_url( get_post_thumbnail_id() ) ?>

    <div class="column column-<?= $i++;?>">

      <div class="inner">

        <a class="image_link" href="<?php echo get_permalink(); ?>">
          <div class="portal_image" style="background-image:url( <?php echo e($url); ?> )">
            <div class="overlay">

              <p><?php echo the_excerpt(); ?></p>

              <a href="<?php echo get_permalink(); ?>" class="btn btn-r">Range of Services</a>

            </div>
          </div>
        </a>

        <a class="text_link" href="<?php echo get_permalink(); ?>"><h4> <?php echo the_title(); ?> </h4></a>

      </div>

    </div>

    <?php endwhile; ?>

    <?php wp_reset_postdata() ?>

    <?php endif; ?>

  </div>

</section>
