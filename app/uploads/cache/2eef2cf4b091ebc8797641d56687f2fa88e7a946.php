<?php
@$street = get_field('address_street_name', 'options');
@$city = get_field('address_city_name', 'options');
@$state = get_field('address_state', 'options');
@$zip = get_field('address_zipcode', 'options');
@$address = $street . '<br>' . $city . ', ' . $state .' '. $zip;
@$fax = get_field('fax_number', 'option');
@$hours = get_field('hours_open', 'option');
@$message = get_field('hours_message', 'option');
@$office = get_field('phone_number', 'option');

?>

<a href="#anchor_top" class="anchor-top"><i class="fas fa-angle-double-up"></i></a>

<a class="btn btn-r form-action">Schedule Service</a>

<div class="form-container reveal-me">
  <span class="close"><i class="fas fa-times"></i></span>

  <h5>SCHEDULE SERVICE</h5>
  <?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>


</div>

<footer class="section section-footer">
  <div class="o-content f-top">
    <div class="column">
      <h6>Superior Heating & Air</h6>

      <?php echo wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'f-nav']); ?>


    </div>
    <div class="column">
      <h6>Resources</h6>

      <?php echo wp_nav_menu(['theme_location' => 'resources_navigation', 'menu_class' => 'f-nav']); ?>


    </div>
    <div class="column">
      <h6>Contact</h6>

      <div itemscope itemtype="http://schema.org/RVPark" class="mbd">

        <strong>Superior Heating & Air</strong>

        <?php if($address): ?>

        <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
          <span itemprop="streetAddress"><?php echo e($street); ?></span>
          <span itemprop="addressLocality"><?php echo e($city); ?></span>,
          <span itemprop="addressRegion"><?php echo e($state); ?></span>
          <span itemprop="postalCode"><?php echo e($zip); ?></span>
        </address>

        <?php endif; ?>

        <?php if($office): ?>

        <p>Office: <a href="tel:<?php echo e(preg_replace('/[^0-9]/', '', get_field('phone_number', 'option'))); ?>" title="Call Us at <?php echo e($phone); ?>"> <?php echo e(get_field('phone_number', 'option')); ?></a></p>

        <?php endif; ?>

        <?php if($fax): ?>

        <p>Fax: <?php echo e($fax); ?></p>

        <?php endif; ?>

        <?php if($hours): ?>

        <p><?php echo e($hours); ?></p>

        <?php endif; ?>

        <?php if($message): ?>

        <p><?php echo e($message); ?></p>

        <?php endif; ?>

      </div>

    </div>

    <div class="column">

      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8760.785788798059!2d-116.30683441760773!3d33.76575870233439!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dafb9366f230e3%3A0xe8c89e4e1066e2f3!2sSuperior+Heating+%26+Air+Conditioning!5e0!3m2!1sen!2sus!4v1552674442178" width="600" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>

    </div>

  </div>

  <div class="o-content f-bottom">

    <hr>

    <a class="brand footer__brand" href="<?php echo e(home_url('/')); ?>">
      <img src="<?= App\asset_path('images/logo_superior_air.svg'); ?>" title="<?php echo e(get_bloginfo('name', 'display')); ?>" alt="<?php echo e(get_bloginfo('name', 'display')); ?>" width="340" height="200">
    </a>

    <?php echo $__env->make('partials.social-media', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <p class="f-rights"> &copy; <?= date('Y'); ?> <?php bloginfo('name'); ?> | Website by  <a href="http://www.bigrigmedia.com">Big Rig Media LLC</a> &reg;</p>

  </div>

</footer>
