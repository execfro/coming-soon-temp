<section class="section section--partnership">

  <div class="content">

    <div class="owl-carousel c-carousel c-carousel--partnership">

      <?php if( have_rows('repeater_sponsor', 'option') ): ?>

      <?php while( have_rows('repeater_sponsor', 'option') ): ?> <?php the_row() ?>

      <div class="column column-2" style="background-image:url(<?php echo the_sub_field('sub_field_image', 'option'); ?>)"></div>

      <?php endwhile; ?>

      <?php wp_reset_postdata() ?>

      <?php endif; ?>

    </div>

  </div>

</section>
