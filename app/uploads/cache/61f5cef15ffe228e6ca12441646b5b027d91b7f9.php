<?php $loactions = get_field('phone_number', 'option');

@$phone = get_field('phone_number', 'option');

?>

<header class="header">

  <div class="header__top">

    <div class="o-content fl-btw">

      <?php echo $__env->make('partials.locations', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      <?php if( $phone ): ?>

      <a class="header__link header__link--phone" href="tel:<?php echo e(preg_replace('/[^0-9]/', '', $phone  )); ?>" title="Call Toll Free <?php echo e($phone); ?>"><span>Call Toll Free</span> <i class="fas fa-phone"></i> <span class="p_num"><?php echo e($phone); ?></span></a>

      <?php endif; ?>

    </div>

  </div>

  <a class="brand header__brand" href="<?php echo e(home_url('/')); ?>">
    <img src="<?= App\asset_path('images/logo_doctor_id.svg'); ?>" title="<?php echo e(get_bloginfo('name', 'display')); ?>" alt="<?php echo e(get_bloginfo('name', 'display')); ?>" width="340" height="200">
  </a>

</header>
