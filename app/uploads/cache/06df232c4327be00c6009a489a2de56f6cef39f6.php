<?php if(get_field('newsletter_content', 'options') || get_field('newsletter_form', 'options') ): ?>
<section class="section section--newsletter">
  <div class="o-content">
    <div class="column">

      <?php echo get_field('newsletter_content', 'options'); ?>


    </div>
    <div class="column">

      <?php echo get_field('newsletter_form', 'options'); ?>


    </div>
  </div>
</section>
<?php endif; ?>
