<?php
$a = 1;
$b = 1;
$services_recovery = get_field('recovery_services_top_content');

?>


<?php if( have_rows('recovery_services') ): ?>

<section class="section section--recovery">
  <div class="content">

    <?php echo $services_recovery; ?>


    <div class="fl-center fl-column section creator">

      <?php while( have_rows('recovery_services') ): ?> <?php the_row() ?>

      <div class="card card-<?= $a++;?>"   <?php if(get_sub_field('background_field')): ?>
      style="background-color:<?php echo the_sub_field('background_field'); ?>"
      <?php endif; ?>
      >

      <?php echo e(the_sub_field('default_editor')); ?>


    </div>

    <?php endwhile; ?>

  </div>

</div>

<div class="o-content">

  <a href="#form_anchor" class="btn btn-r">get protected</a>

</div>

</section>

<?php endif; ?>
