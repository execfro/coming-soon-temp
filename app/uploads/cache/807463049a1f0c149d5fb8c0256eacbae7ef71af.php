<section id="form_anchor" class="section section--intro">
  <div class="content">
    <div class="intro-bg">
      <?php while (have_posts()) : the_post(); ?>
        <?php the_content(); ?>
      <?php endwhile; ?>
    </div>
  </div>
</section>
