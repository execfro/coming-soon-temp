<?php if(get_field('services_image') || get_field('services_blurb') ): ?>
<section class="section section--services">
  <div class="content">
    <div class="column" style="background-image:url( <?php echo get_field('services_image'); ?> )"></div>
    <div class="column">
      <?php echo get_field('services_blurb'); ?>

    </div>
  </div>
</section>
<?php endif; ?>
