<?php
$a = 1;
$b = 1;

?>

<?php if( have_rows('section_f_creator') ): ?>

<section class="section-creator-full">

  <?php while( have_rows('section_f_creator') ): ?> <?php the_row() ?>

  <div class="creator-f creator-f-<?php echo $a++; ?> flex fl-btw fl-column">

    <div class="column <?php echo get_sub_field('order_num'); ?> bg_img lazyload" <?php if( get_sub_field('background_field') ): ?> style="background-image:url(<?php echo get_sub_field('background_field'); ?>)" <?php endif; ?>></div>

    <div class="column <?php echo get_sub_field('order_num_two'); ?> <?php echo get_sub_field('dark_back_color'); ?>" style="background-color:<?php echo the_sub_field('bg_clr_f'); ?>">

    <div class="inner">

      <?php echo the_sub_field('default_f_editor'); ?>


    </div>

  </div>

</div>

<?php endwhile; ?>

</section>

<?php endif; ?>
