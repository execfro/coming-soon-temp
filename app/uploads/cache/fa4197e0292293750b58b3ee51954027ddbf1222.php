<header class="header">

  <div class="header__top">

    <div class="o-content">

      <?php echo wp_nav_menu(['theme_location' => 'top_quick_navigation', 'menu_class' => 'p_top_nav']); ?>


      <?php if(get_field('phone_number', 'option') ): ?>

      <a class="header__link header__link--phone" href="tel:<?php echo e(preg_replace('/[^0-9]/', '', get_field('phone_number', 'option'))); ?>" title="Call Us at <?php echo e($phone); ?>"> <i class="fas fa-phone"></i> <?php echo e(get_field('phone_number', 'option')); ?></a>

      <?php endif; ?>

      <a class="btn btn-r">Request a Quote</a>

    </div>

  </div>

  <div class="header__bottom">

    <div class="o-content">

      <a class="brand header__brand" href="<?php echo e(home_url('/')); ?>">
        <img src="<?= App\asset_path('images/logo_superior_air.svg'); ?>" title="<?php echo e(get_bloginfo('name', 'display')); ?>" alt="<?php echo e(get_bloginfo('name', 'display')); ?>" width="340" height="200">
      </a>

      <?php echo wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'p-nav']); ?>


    </div>

  </div>

</header>

<button class="c-header__toggle c-header__toggle--htx"><span></span></button>
