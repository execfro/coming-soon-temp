<!doctype html>
<html <?php echo get_language_attributes(); ?>>
<?php echo $__env->make('partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<body <?php body_class() ?> id="anchor_top">
  <?php if($tag): ?>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-<?php echo $tag; ?>"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <?php endif; ?>
    <?php do_action('get_header') ?>
    <?php echo $__env->make('partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('partials.hero', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <main>
      <?php if(!is_front_page()): ?>
      <section class="section section--main">
        <div class="content">
          <?php echo $__env->yieldContent('content'); ?>
          <?php if(App\display_sidebar()): ?>
          <aside class="sidebar">
            <?php echo $__env->make('partials.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          </aside>
          <?php endif; ?>
        </div>
      </section>
      <?php else: ?>
      <?php echo $__env->yieldContent('content'); ?>
      <?php endif; ?>
    </main>

    <?php if( !is_front_page()  ): ?>
    <?php echo $__env->make('partials.creator', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('partials.action', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>

    <?php if(!is_front_page() && is_home() || is_404() || is_single()): ?>
    <?php echo $__env->make('partials.action', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>

    <?php echo $__env->make('partials.testimonial', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('partials.newsletter', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('partials.sponsors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php do_action('get_footer') ?>
    <?php echo $__env->make('partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php wp_footer() ?>
  </body>
  </html>
