<?php

namespace App;

/**
 * Force Gravity Form to scroll to form top position upon submission
 */
add_filter('gform_confirmation_anchor', '__return_true');

/**
 * Custom login url
 *
 * @link https://codex.wordpress.org/Plugin_API/Filter_Reference/login_headerurl
 */
add_filter('login_headerurl', function() {
    return get_home_url();
});

/**
 * Row
 *
 * @param array $atts
 */
add_shortcode('row', function($atts, $content = null) {
    return '<div class="row">'.do_shortcode($content).'</div>';
});

/**
 * Column
 *
 * @param array $atts
 */
add_shortcode('column', function($atts, $content = null) {
    extract(shortcode_atts([
        'columns' => 6,
    ], $atts));

    return '<div class="col md:col-'.$columns.'">'.do_shortcode($content).'</div>';
});

/**
 * Custom image sizes
 *
 * @link https://developer.wordpress.org/reference/functions/add_image_size/
 *
 * e.g. add_image_size('w800x400', 800, 400, true)
 */

$custom_sizes = [
    'w1920x600' => [1920, 600, true],
    'w960x600'  => [960, 600, true],
    'w760x420'  => [615, 650, true]
];

if (!empty($custom_sizes)) {
    foreach ($custom_sizes as $key => $custom_size) {
        add_image_size($key, $custom_size[0], $custom_size[1], $custom_size[2]);
    }
}

/**
 * Add custom image sizes to media library
 *
 * @link https://codex.wordpress.org/Plugin_API/Filter_Reference/image_size_names_choose
 * @param array $sizes
 */
add_filter('image_size_names_choose', function($sizes) {
    $addsizes = [
        'w760x420' => __('Square Ratio Medium')
    ];

    $newsizes = array_merge($sizes, $addsizes);

    return $newsizes;
});
