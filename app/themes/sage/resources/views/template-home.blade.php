{{--
  Template Name: Home Template
--}}

@extends('layouts.app')

@section('content')
  @include('partials.intro')
  @include('partials.creator-full')
  @include('partials.creator')
  @include('partials.contact')
@endsection
