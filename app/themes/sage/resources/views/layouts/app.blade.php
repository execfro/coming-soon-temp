<!doctype html>
<html {!! get_language_attributes() !!}>
@include('partials.head')
<body @php body_class() @endphp id="anchor_top">
  @if($tag)
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-{!! $tag !!}"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    @endif
    @php do_action('get_header') @endphp
    @include('partials.header')
    @include('partials.hero')
    <main>
      @if (!is_front_page())
      <section class="section section--main">
        <div class="content">
          @yield('content')
          @if (App\display_sidebar())
          <aside class="sidebar">
            @include('partials.sidebar')
          </aside>
          @endif
        </div>
      </section>
      @else
      @yield('content')
      @endif
    </main>


    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>
  </html>
