
@php $facebook_url = get_field('facebook_url','option');

@$instagram_url = get_field('instagram_url','option');

@$google_url = get_field('google_url','option');

@$yelp_url = get_field('yelp_url','option');

@endphp

<ul class="social-media">

  @if ( $google_url )

  <li>

    <a href="{{ $google_url }}">

      <i class="fab fa-google"></i>

    </a>

  </li>

  @endif

  @if ( $facebook_url )

  <li>

    <a href="{{ $facebook_url }}">

      <i class="fab fa-facebook-f"></i>

    </a>

  </li>

  @endif

  @if ( $yelp_url )

  <li>

    <a href="{{ $yelp_url }}">

      <i class="fab fa-yelp"></i>

    </a>

  </li>

  @endif

  @if ( $instagram_url )

  <li>

    <a href="{{ $instagram_url }}">

      <i class="fab fa-instagram"></i>

    </a>

  </li>

  @endif

</ul>
