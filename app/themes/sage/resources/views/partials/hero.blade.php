<?php

$i = 1;

?>

@if ( is_front_page() && is_home() )


<div class="hero">

  <div class="hero__content">

    {!! do_shortcode('[hero_slider]') !!}

  </div>

</div>

@elseif ( is_front_page() )

<div class="hero">
  {!! do_shortcode('[hero_slider]') !!}
</div>

@elseif ( !is_front_page() && is_home() || is_404() || is_single() )

<div class="hero">
  <div class="hero__content">
    @include('partials.blog-hero')
  </div>
</div>

@else

  @if (is_page())
  <div class="hero">
    <div class="hero__content">
      {!! do_shortcode('[hero_slider]') !!}
    </div>
  </div>

  @endif

  @if (!is_page() || is_single() || is_archive() )

  <div class="hero">
    <div class="hero__content">
      @include('partials.blog-hero')
    </div>
  </div>

  @endif

@endif
