@php
@$logo = get_field('logo_field', 'option');
@endphp


<footer class="section section-footer">

  <div class="content">

    <p class="f-rights"> &copy; <?= date('Y'); ?> <?php bloginfo('name'); ?> | Website Design by  <a href="http://www.bigrigmedia.com">Big Rig Media LLC</a> &reg;</p>

  </div>

</footer>
