@php $loactions = get_field('phone_number', 'option');

@$call_form = get_field('call_form', 'option');

@endphp

<section class="section section--form">

  <div class="content">

    @if( $call_form )

    {{ $call_form }}

    @endif

  </div>

</section>
