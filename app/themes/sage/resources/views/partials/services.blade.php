@php

@$services_image = get_field('services_image');

@$service_content = get_field('service_content');

@$service_header_text = get_field('service_header_text');

@endphp

@if ( $service_content )

<section class="section section--services">

  @if ( $service_header_text )

  <div class="o-content">

    <h3>{{ $service_header_text }}</h3>

  </div>

  @endif

  <div class="fl-btw fl-column">

    <div class="column column-image" style="background-image: url( {!! $services_image !!} )" >
    </div>

    <div class="column">

      <div class="inner">

        {!! $service_content !!}

      </div>

    </div>

  </div>

</section>

@endif
