@php

@$identity_text = get_field('identity_text');

@endphp

@if( $identity_text )

<section class="section section--identity">

  <div class="content">

    {!! $identity_text !!}

  </div>

</section>

@endif
