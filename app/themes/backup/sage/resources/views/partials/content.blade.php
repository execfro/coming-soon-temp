<article <?php post_class('c-post'); ?>>
  <div class="flex-control">
    <div class="column column--1" >
      <a href="<?php echo get_permalink();?>">
        @if (has_post_thumbnail() )

        @the_post_thumbnail()

        @else

        <img src="@asset('images/logo_superior_air.svg')" title="{{ get_bloginfo('name', 'display') }}" alt="{{ get_bloginfo('name', 'display') }}" width="340" height="200">

        @endif

      </a>

    </div>

    <div class="column column--2" >

      <a href="<?php the_permalink(); ?>">
        <h2><?php the_title(); ?></h2></a>
        <?php the_excerpt(); ?>

      </div>

    </div>

  </article>
