@php
@$street = get_field('address_street_name', 'options');
@$city = get_field('address_city_name', 'options');
@$state = get_field('address_state', 'options');
@$zip = get_field('address_zipcode', 'options');
@$address = $street . '<br>' . $city . ', ' . $state .' '. $zip;
@$fax = get_field('fax_number', 'option');
@$hours = get_field('hours_open', 'option');
@$message = get_field('hours_message', 'option');
@$office = get_field('phone_number', 'option');
@$contact_image = get_field('contact_image', 'option');
@$contact_iframe = get_field('contact_iframe', 'option');
@$phone = get_field('phone_number', 'option');
@$email = get_field('email_field', 'option');
@endphp

<div class="section section--contact fl-btw fl-column">
  <div class="column">
    <div class="inner">

      <h4>{!! get_bloginfo() !!}</h4>

      <div itemscope itemtype="http://schema.org/RVPark" class="mbd">

        @if ($address)

        <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
          <span itemprop="streetAddress">{{ $street }}</span>
          <span itemprop="addressLocality">{{ $city }}</span>,
          <span itemprop="addressRegion">{{ $state }}</span>
          <span itemprop="postalCode">{{ $zip }}</span>

        </address>


        @endif

        @if( $phone )

        <a class="header__link header__link--phone" href="tel:{{ preg_replace('/[^0-9]/', '', $phone  ) }}" title="Call Toll Free {{ $phone }}"><i class="fas fa-phone"></i> <span class="p_num">{{ $phone }}</span></a>

        @endif

        @if( $email)

        <br>  <a class="email" href="mailto:{{ $email }}"><i class="fas fa-envelope"></i> <span class="e_num">{{ $email }}</span></a>

        @endif

        @include('partials.social-media')

      </div>
    </div>
  </div>

  <div class="column" style="background-image: url({!! $contact_image !!})">

    @if ($contact_iframe)

    {!! $contact_iframe !!}

    @endif

  </div>
</div>
