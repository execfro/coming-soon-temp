@if( have_rows('location_name', 'option') )

<ul class="locations fl-start">

  @while ( have_rows('location_name', 'option') ) @php the_row() @endphp

  <li class="city_name">

    {{ the_sub_field('city_name', 'option') }}

  </li>

  @endwhile

</ul>

@endif
