<article @php post_class() @endphp>
  <article <?php post_class('c-post c-post--single'); ?>>
    <h1 class="entry-title">{!! get_the_title() !!}</h1>
    @include('partials/entry-meta')
    @php the_content() @endphp

  </article>
