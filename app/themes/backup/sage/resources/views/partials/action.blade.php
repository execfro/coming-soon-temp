@if(get_field('call_to_action', 'options') )

<section class="section section--action">
  <div class="content">
      {!! get_field('call_to_action', 'options') !!}
  </div>
</section>

@endif
