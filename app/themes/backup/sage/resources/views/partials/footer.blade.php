@php
@$logo = get_field('logo_field', 'option');
@endphp


<footer class="section section-footer">

  <div class="content">

    <!---
    <a class="brand header__brand" href="https://doctorsfr.com/">
    <img src="{!! $logo !!}" title="{{ get_bloginfo('name', 'display') }}" alt="{{ get_bloginfo('name', 'display') }}" width="340" height="200">
  </a>
-->

<p class="f-rights"> &copy; <?= date('Y'); ?> <?php bloginfo('name'); ?> | Website Design by  <a href="http://www.bigrigmedia.com">Big Rig Media LLC</a> &reg;</p>

</div>

</footer>
