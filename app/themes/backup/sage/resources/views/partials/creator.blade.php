@php
$a = 1;
$b = 1;

@endphp

@if( have_rows('section_creator') )

<section class="section-creator">

  @while ( have_rows('section_creator') ) @php the_row() @endphp

  <div class="lazyload creator-{!! $a++ !!} bg_img {!! get_sub_field('dark_background') !!}"
  @if ( get_sub_field('background_field_contained') )
  style="background-image:url({!! get_sub_field('background_field_contained') !!})"
  @endif
  @if ( get_sub_field('background_color') )
  style="background-color: {!! get_sub_field('background_color') !!}"
  @endif
  >

  <div class="content">

    {!! the_sub_field('contained_editor') !!}

  </div>

</div>

@endwhile

</section>

@endif
