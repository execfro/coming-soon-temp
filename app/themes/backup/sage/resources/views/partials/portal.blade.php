<?php
$i =1;
$query = new WP_Query([
  'post_type' => 'page',
  'post__in'  => [31,33],
  'orderby'   => 'post__in'
]);

?>

<section class="section section--portal">

  <div class="content">

    @if ($query->have_posts())

    @while ($query->have_posts()) @php $query->the_post() @endphp

    @php $url = wp_get_attachment_url( get_post_thumbnail_id() ) @endphp

    <div class="column column-<?= $i++;?>">

      <div class="inner">

        <a class="image_link" href="{!! get_permalink() !!}">
          <div class="portal_image" style="background-image:url( {{ $url }} )">
            <div class="overlay">

              <p>{!! the_excerpt() !!}</p>

              <a href="{!! get_permalink() !!}" class="btn btn-r">Range of Services</a>

            </div>
          </div>
        </a>

        <a class="text_link" href="{!! get_permalink() !!}"><h4> {!! the_title() !!} </h4></a>

      </div>

    </div>

    @endwhile

    @php wp_reset_postdata() @endphp

    @endif

  </div>

</section>
