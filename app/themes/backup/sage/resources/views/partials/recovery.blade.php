@php
$a = 1;
$b = 1;
$services_recovery = get_field('recovery_services_top_content');

@endphp


@if( have_rows('recovery_services') )

<section class="section section--recovery">
  <div class="content">

    {!! $services_recovery !!}

    <div class="fl-center fl-column section creator">

      @while ( have_rows('recovery_services') ) @php the_row() @endphp

      <div class="card card-<?= $a++;?>"   @if (get_sub_field('background_field'))
      style="background-color:{!! the_sub_field('background_field') !!}"
      @endif
      >

      {{ the_sub_field('default_editor') }}

    </div>

    @endwhile

  </div>

</div>

<div class="o-content">

  <a href="#form_anchor" class="btn btn-r">get protected</a>

</div>

</section>

@endif
