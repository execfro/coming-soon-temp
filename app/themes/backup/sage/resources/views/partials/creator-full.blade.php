@php
$a = 1;
$b = 1;

@endphp

@if( have_rows('section_f_creator') )

<section class="section-creator-full">

  @while ( have_rows('section_f_creator') ) @php the_row() @endphp

  <div class="creator-f creator-f-{!! $a++ !!} flex fl-btw fl-column">

    <div class="column {!! get_sub_field('order_num') !!} bg_img lazyload" @if ( get_sub_field('background_field') ) style="background-image:url({!! get_sub_field('background_field') !!})" @endif></div>

    <div class="column {!! get_sub_field('order_num_two') !!} {!! get_sub_field('dark_back_color') !!}" style="background-color:{!! the_sub_field('bg_clr_f') !!}">

    <div class="inner">

      {!! the_sub_field('default_f_editor') !!}

    </div>

  </div>

</div>

@endwhile

</section>

@endif
