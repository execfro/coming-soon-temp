// import external dependencies
import 'jquery';

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,


  // for the mobile animation


  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());


//*Mobile Menu*//
$( '.c-header__toggle' ).click(function(e) {
  e.preventDefault();

  $('.p-nav').animate({height: 'toggle'});
});

// for the mobile animation

$('.c-header__toggle').click(function(){
  $(this).toggleClass('is-active');
});

// Mobile Drop Menu Primary Nav

$(function () {
  var children = $('.m-nav > li > a').filter(function () { return $(this).nextAll().length > 0 ;});
  $('<span class="drop-menu"></span>').insertAfter(children);
  $('.m-nav .drop-menu').click(function () {
    $(this).next().slideToggle(300);
    return false;
  });
});

//// Target Mobile Devices for inputing a Carrot Drop Button.

if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
  // tasks to do if it is a Mobile Device
  $(function () {
    var children = $('.p-nav > li > a').filter(function () { return $(this).nextAll().length > 0 ;});
    $('<span class="drop-menu"><i class="fas fa-chevron-down"></i></span>').insertAfter(children);
    $('.p-nav .drop-menu').click(function () {
      $(this).next().slideToggle(300);
      return false;
    });
  });

}


// Class on scroll

var viewportWidth = $(window).width();

var scrolls = $('.anchor-top'),
functionscroll   = 10000;
if (viewportWidth >= 300) {
  $(window).scroll(function() {
    var scrollX = $(this).scrollTop();

    if (scrollX >= 100) {
      $(scrolls).delay(functionscroll).addClass('appear');
    } else {
      $(scrolls).removeClass('appear');
    }
  });
}

///////// Smooth scroll to div

$('a[href*="#"]').on('click', function (e) {
  e.preventDefault();

  $('html, body').animate({
    scrollTop: $($(this).attr('href')).offset().top,
  }, 500, 'linear');
});

/// Form action
$('.form-action').click(function(){
  $('.reveal-me').toggleClass('reveal');
});

$('.close').click(function(){
  $('.reveal-me').toggleClass('reveal');
});


var scrolled = $('.form-action'),
functionscrolled   = 10000;
if (viewportWidth >= 300) {
  $(window).scroll(function() {
    var scrollX = $(this).scrollTop();

    if (scrollX >= 100) {
      $(scrolled).delay(functionscrolled).addClass('appear');
    } else {
      $(scrolled).removeClass('appear');
    }
  });
}

// Remove emtpy p tags

$('p:empty').remove();
