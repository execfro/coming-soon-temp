<?php

namespace App;

/**
 * Theme customizer
 */
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
    // Add postMessage support
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial('blogname', [
        'selector' => '.brand',
        'render_callback' => function () {
            bloginfo('name');
        }
    ]);

    // Set custom analytics section
    $wp_customize->add_section('analytics', [
        'title'         => 'Google Analytics',
        'description'   => '',
        'priority'      => 120
    ]);

    $wp_customize->add_setting('google_tag_manager', [
        'type'      => 'option',
        'default'   => '',
        'transport' => 'postMessage'
    ]);

    $wp_customize->add_control('google_tag_manager', [
        'label'         => 'Google Tag Manager',
        'section'       => 'analytics',
        'settings'      => 'google_tag_manager',
        'description'   => 'Exclude the GTM portion'
    ]);

});

/**
 * Customizer JS
 */
add_action('customize_preview_init', function () {
    wp_enqueue_script('sage/customizer.js', asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
});
