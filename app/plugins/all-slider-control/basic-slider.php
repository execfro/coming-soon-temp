<?php
/**
* Plugin Name:     All Slider Control (ASC)
* Plugin URI:      https://github.com/ernstoarllano/simple-specials
* Description:     Allows a developer to easily create Carousel Sliders
* Version:        9.9.9.
* Author:         Andrew F. Munoz
* Author URI:
* License:         MIT
* License URI:     http://opensource.org/licenses/MIT
*/

// Exit if accessed directly
if (!defined('ABSPATH')) {
  exit;
}

/**
* @since 1.0.0
*
* @package specialrotator
* @author  Ernesto Arellano Jr
*/
class specialrotator
{
  /**
  * Constructor
  *
  * @since 1.0.0
  */
  public function __construct()
  {
    // Variables
    $this->settings = [
      'dir'     => plugin_dir_url(__FILE__),
      'version' => '1.0.0'
    ];
    
    // Actions
    add_action('init', [$this, 'init']);
    add_filter('manage_edit-testimonials_columns', [$this, 'admin_columns']);
    add_action('manage_testimonials_posts_custom_column', [$this, 'admin_columns_content'], 10, 2);
    add_filter('manage_edit-testimonials_sortable_columns', [$this, 'admin_sortable_columns']);
    //add_action('contextual_help', [$this, 'help'], 10, 3);
    add_action('wp_enqueue_scripts', [$this, 'styles']);
    add_action('wp_enqueue_scripts', [$this, 'scripts']);
    
    // Shortcode
    add_shortcode('hero_slider', [$this, 'shortcode']);
  }
  
  /**
  *  Create custom post type and taxonomies
  *
  * @link https://codex.wordpress.org/Function_Reference/register_post_type
  * @link https://codex.wordpress.org/Function_Reference/register_taxonomy
  * @since 1.0.0
  */
  public function init()
  {
    // Create custom post type
  }
  
  /**
  * Modify admin columns
  *
  * @link https://codex.wordpress.org/Plugin_API/Filter_Reference/manage_$post_type_posts_columns
  * @param array $columns
  * @since 1.0.0
  */
  public function admin_columns($columns)
  {
    global $post;
    
    switch($column_name) {
      case 'source':
      $terms = get_the_terms($post_id, 'specials_source');
      
      if (!empty($terms)) {
        $output = [];
        
        foreach ($terms as $term) {
          $output[] = sprintf(
            '<a href="%s">%s</a>',
            esc_url(add_query_arg([
              'post_type' => $post->post_type,
              'source'    => $term->slug
            ], 'edit.php')),
            esc_html(sanitize_term_field('name', $term->name, $term->term_id, 'source', 'display'))
          );
        }
        
        echo join(', ', $output);
      }
      break;
      default:
      break;
    }
  }
  
  
  
  /**
  * Load carousel styles
  *
  * @link https://developer.wordpress.org/reference/functions/wp_enqueue_style/
  * @since 1.0.0
  */
  public function styles()
  {
    // Configure styles
    $styles = [
      'owl-carousel' => $this->settings['dir'] . 'assets/css/owl.carousel.min.css'
    ];
    
    // Enqueue styles
    foreach ($styles as $key => $style) {
      wp_enqueue_style($key, $style, false, $this->settings['version']);
    }
  }
  
  /**
  * Load carousel scripts
  *
  * @link https://developer.wordpress.org/reference/functions/wp_enqueue_script/
  * @since 1.0.0
  */
  public function scripts()
  {
    // Configure scripts
    $scripts = [
      'owl-carousel'          => $this->settings['dir'] . 'assets/js/owl.carousel.min.js',
      'simple-specials'   => $this->settings['dir'] . 'assets/js/scripts.js'
    ];
    
    // Enqueue scripts
    foreach ($scripts as $key => $script) {
      wp_enqueue_script($key, $script, ['jquery'], $this->settings['version'], true);
    }
  }
  
  
  /**
  * Create specials shortcode
  *
  * @link https://codex.wordpress.org/Shortcode_API
  * @param array $atts
  * @since 1.0.0
  */
  public function shortcode($atts)
  {
    // Set shortcode attributes
    $atts = shortcode_atts([
      'total' => 5
    ], $atts);
    
    // Get testimonial records
    
    ?>
    
    <div class="owl-carousel c-carousel c-carousel--hero">
      <?php
      // check if the repeater field has rows of data
      if( have_rows('repeater_heroslider') ):
        
        // loop through the rows of data
        while ( have_rows('repeater_heroslider') ) : the_row(); {?>
          
          <div class="slider">
            
            <div class="column hero-back" style="background-image: url(<?= get_sub_field('sub_field_image');?>)">
          
            
            </div>
            
          </div>
          
        <?php }
      endwhile;
      else :
        // no rows found
      endif;  ?>
    </div>
    
  <?php   }
}

// Initialize main class
new specialrotator();
