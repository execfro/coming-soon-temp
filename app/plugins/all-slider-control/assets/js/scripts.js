
//////////////////////// Hero Slider

(function ($) {

  if ($('.c-carousel--hero').length) {
    $('.c-carousel--hero').owlCarousel({

      items: 1,
      loop: false,
      nav: true,
      navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
      dots: false,
      autoplay: true,
      autoplayTimeout: 5000,
      autoplayHoverPause: true,
      autoplaySpeed: 800,
      autoHeight: false,
      touchDrag: false,
      mouseDrag: false,
      animateOut: 'fadeOut',
      animateIn: 'fadeIn',

    });
  }
})(jQuery);

(function ($) {

  if ($('.c-carousel--test').length) {
    $('.c-carousel--test').owlCarousel({

      items: 1,
      loop: true,
      nav: true,
      navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
      dots: false,
      autoplay: true,
      autoplayTimeout: 5000,
      autoplayHoverPause: false,
      autoplaySpeed: 800,
      autoHeight: true,
      touchDrag: true,
      mouseDrag: true,
      animateOut: 'fadeOut',
      animateIn: 'fadeIn',

    });
  }
})(jQuery);



if (window.matchMedia("(max-width: 768px)").matches) {

  (function ($) {
  
    if ($('.c-carousel--partnership').length) {
      $('.c-carousel--partnership').owlCarousel({
  
        items: 2,
        loop: true,
        nav: false,
        navText: ['', ''],
        dots: false,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: false,
        autoplaySpeed: 800,
        autoHeight: true,
        touchDrag: true,
        mouseDrag: true,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
  
      });
    }
  })(jQuery);

}

(function ($) {

  if ($('.c-carousel--partnership').length) {
    $('.c-carousel--partnership').owlCarousel({

      items: 5,
      loop: true,
      nav: false,
      navText: ['', ''],
      dots: false,
      autoplay: true,
      autoplayTimeout: 5000,
      autoplayHoverPause: false,
      autoplaySpeed: 800,
      autoHeight: false,
      touchDrag: true,
      mouseDrag: true,
      animateOut: 'fadeOut',
      animateIn: 'fadeIn',

    });
  }
})(jQuery);
