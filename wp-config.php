<?php
/**
* The base configurations of the WordPress.
*
* This file has the following configurations: MySQL settings, Table Prefix,
* Secret Keys, WordPress Language, and ABSPATH. You can find more information
* by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
* wp-config.php} Codex page. You can get the MySQL settings from your web host.
*
* This file is used by the wp-config.php creation script during the
* installation. You don't have to use the web site, you can just copy this file
* to "wp-config.php" and fill in the values.
*
* @package WordPress
*/

/**
* Disable automatic update
*
* @link http://codex.wordpress.org/Configuring_Automatic_Background_Updates
*/
define('AUTOMATIC_UPDATER_DISABLED', true);

/**
* Limit number of revisions saved
*
* @link https://codex.wordpress.org/Revisions
*/

define('WP_POST_REVISIONS', 10);

/**
* Update default mem limit
*
* @link http://codex.wordpress.org/Editing_wp-config.php#Increasing_memory_allocated_to_PHP
*/
define('WP_MEMORY_LIMIT', '64M');

/**
* Disable dashboard file editing
*
* @link http://codex.wordpress.org/Hardening_WordPress#Disable_File_Editing
*/
define('DISALLOW_FILE_EDIT', true);

/**
* WordPress Environment
*
* The default usage is:
* 	development - For local development
*	production - For live site
*/
define('WP_ENV', 'production');

// Switch MySQL settings based on environment
switch(WP_ENV){
  // Development
  case 'development':
  /** The name of the local database for WordPress */
  define('DB_NAME', 'comingsoon_db');
  
  /** MySQL local database username */
  define('DB_USER', 'root');
  
  /** MySQL local database password */
  define('DB_PASSWORD', 'root');
  break;
  // Production
  case 'production':
  /** The name of the live database for WordPress */
  define('DB_NAME', '');
  
  /** MySQL live database username */
  define('DB_USER', '');
  
  /** MySQL live database password */
  define('DB_PASSWORD', '');
  break;
}

// ** MySQL settings - You can get this info from your web host ** //
/** MySQL hostname */

define('DB_HOST', 'localhost');
//define('DB_HOST', '157.230.143.138');
//define('DB_HOST', '10.138.42.250');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/** Custom Content Directory **/
define('WP_CONTENT_DIR', dirname( __FILE__ ) . '/app');
define('WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/app');

/**
* Authentication Unique Keys and Salts.
*
* Change these to different unique phrases!
* You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
* You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
*
* @since 2.6.0
*/
define('AUTH_KEY',         'DYkql=T5g6DEyj_Qp&xC@}:ywRZ:@d680f]R@s5bibw_E~A=%c;VvY<Z7yuLdnZV');
define('SECURE_AUTH_KEY',  'IB)fu{|cJWqVy1=B9[IyOnW^+8FggV6Gx^y1!kJ=c D#DuRlh3?&/ h?azq>^*x0');
  define('LOGGED_IN_KEY',    '=F0/L+Z=N3ehXZK&$<%-I!+?eL|2@>B@4EYs+9Q2n!8|[6~VU/T~o/]1%>^ZzP@V');
  define('NONCE_KEY',        '*$Ap<*y dx}$m{x!o@E1qyS)s#SiQm}<sf2v%K5nidCB6c(toYdmv+]U`2Bpdz+r');
  define('AUTH_SALT',        'L,|/#BrOb*J+e7PbZ?>Kk`gpnEF{=@@x.[1qkOipg[dWP+?E+H[F<IJLi)G$>(_t');
    define('SECURE_AUTH_SALT', 'CsxN1[vI1R$lMX2/xgxUKa+x$Z_y|fgI3R{A-u,MWGXhS>`kg XnU.1pglV-hMHo');
      define('LOGGED_IN_SALT',   '#L^SRCOt|73FRdH0M3#~%?yx_~6y)v@=D}nZIJz+pQ2^H+-|%+uEb9T{YcEKtGE^');
        define('NONCE_SALT',       'l[TiU,HxLH_|~Ew+o@a7R|jJRQ)A3g*;O)hW#ptIMO$2NVpr Wwt(dp]rB,L|c^{');
          
          /**
          * WordPress Database Table prefix.
          *
          * You can have multiple installations in one database if you give each a unique
          * prefix. Only numbers, letters, and underscores please!
          */
          $table_prefix  = 'cs_';
          
          /**
          * WordPress Localized Language, defaults to English.
          *
          * Change this to localize WordPress. A corresponding MO file for the chosen
          * language must be installed to wp-content/languages. For example, install
          * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
          * language support.
          */
          define('WPLANG', '');
          
          /**
          * For developers: WordPress debugging mode.
          *
          * Change this to true to enable the display of notices during development.
          * It is strongly recommended that plugin and theme developers use WP_DEBUG
          * in their development environments.
          */
          define('WP_DEBUG', false);
          
          /** Enable theme debug mode **/
          define('THEME_DEBUG', false);
          
          /* That's all, stop editing! Happy blogging. */
          
          /** Absolute path to the WordPress directory. */
          if ( !defined('ABSPATH') )
          define('ABSPATH', dirname(__FILE__) . '/wp/');
          
          /** Sets up WordPress vars and included files. */
          require_once(ABSPATH . 'wp-settings.php');
